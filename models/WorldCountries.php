<?php

namespace app\models;

use Yii;
use app\models\Language;

/**
 * This is the model class for table "world_countries".
 *
 * @property integer $id
 * @property integer $id_language
 * @property integer $id_main
 * @property string $name
 * @property string $code
 * @property string $alias
 * @property string $lat
 * @property string $lng
 */
class WorldCountries extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'world_countries';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'code'], 'required'],
            [['id_language', 'id_main'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['slug'], 'unique'],
            [['name', 'code', 'alias'], 'string', 'max' => 255],
            [['id_language', 'code'], 'unique', 'targetAttribute' => ['id_language', 'code'], 'message' => Yii::t('countries', 'The combination of Language and Code has already been taken.')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_language' => Yii::t('countries', 'Language'),
            'name' => Yii::t('countries', 'Name'),
            'code' => Yii::t('countries', 'Code'),
            'alias' => Yii::t('countries', 'Alias'),
            'lat' => Yii::t('countries', 'Lat'),
            'lng' => Yii::t('countries', 'Lng'),
            'slug' => Yii::t("material", 'Path'),
        ];
    }
    
    function behaviors() {
        return [
            'slug' => [
                    'class' => 'app\modules\material\components\behaviors\Slug',
                    'in_attribute' => 'name',
                    'out_attribute' => 'slug',
                    'translit' => true
            ]
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'id_language']);
    }
    
    public static function getList($language = "ru") {
        $items = false;
        $id_language = Language::getLanguageId($language);
        if ($countries = WorldCountries::find()->where(["id_language" => $id_language])->all()) {
            $items = [];
            foreach ($countries as $country) {
                $item = [
                    "id" => $country->id,
                    "name" => $country->name,
                    "items" => []
                ];
                $cities = WorldCities::find()->where([
                    "id_language" => $id_language, 
                    "country" => $country->id]
                )->all();
                foreach ($cities as $city) {
                    $item['items'][] = [
                        "id" => $city->id,
                        "name" => $city->name
                    ];
                }
                $items[] = $item;
            }
        }
        return $items;
    }

    public static function getName($id = false) {
        $country = WorldCountries::findOne(["id" => $id]);
        return ($country) ? $country->name : null;
    }
    
    public static function getLocalList($id = false) {
        return WorldCountries::find()->where(["id_main" => $id])->orderBy("id_language ASC")->asArray()->all();
    }

}

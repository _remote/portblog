<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

class UserSearch extends User {

    public $_section;
    public $_sub_section;
    public $username;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'first_name', 'last_name', 'middle_name', 'email'], 'safe'],
        ];
    }
    
//    /**
//    * @inheritdoc
//    */
//    public function scenarios() {
//        // bypass scenarios() implementation in the parent class
//        return Model::scenarios();
//    }

    public function search($params, $id = false) {
        $query = ($id) ? UserSearch::find()->where(["id_user" => $id]) : UserSearch::find();
        $query->select("user.*");
        $query->joinWith([
//            'section' => function($query) {
//                $query->from(['section' => 'section']);
//            },
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort = ['defaultOrder' => ['id'=>SORT_DESC]];

//        $dataProvider->sort->attributes['username'] = [
//            'asc' => ['user.email' => SORT_ASC],
//            'desc' => ['user.email' => SORT_DESC],
//        ];

        //print_r($query->createCommand()->getRawSql()); die();
        

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['like', 'first_name', $this->first_name]);
        $query->andFilterWhere(['like', 'last_name', $this->last_name]);
        $query->andFilterWhere(['like', 'middle_name', $this->middle_name]);
        $query->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }

}
        
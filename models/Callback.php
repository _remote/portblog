<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "callback".
 *
 * @property integer $id
 * @property string $session
 * @property string $time
 * @property string $autor_name
 * @property string $autor_email
 * @property string $autor_telephone
 * @property string $text
 * @property integer $status
 */
class Callback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'callback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['session', 'status', 'text'], 'required'],
            [['time'], 'safe'],
            [['status'], 'integer'],
            [['session'], 'string', 'max' => 32],
            [['autor_name', 'autor_telephone'], 'string', 'max' => 50],
            [['autor_email'], 'string', 'max' => 100],
            [['text'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'session' => Yii::t("callback", 'Session'),
            'time' => Yii::t("callback", 'Time'),
            'autor_name' => Yii::t("callback", 'Autor Name'),
            'autor_email' => Yii::t("callback", 'Autor Email'),
            'autor_telephone' => Yii::t("callback", 'Autor Telephone'),
            'text' => Yii::t("callback", 'Text'),
            'status' => Yii::t("callback", 'Status'),
        ];
    }
    
    public function testPeriod() {
        $period = 0;
        if ($id = Yii::$app->session->id) {
            if ($session = Callback::find()->where(["and", ["session" => $id], ["<", "time", $this->time]])->orderBy('`time` DESC')->one()) {
                $period = $this->time - $session->time;
                if ($period > 2 * 60) {
                    $period = 0;
                }
                else $period = 2 * 60 - $period;
            }
        }
        return $period;
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\helpers\StringHelper;
use \yii\web\IdentityInterface;
use yii\data\ActiveDataProvider;
use app\modules\permit\interfaces\UserRbacInterface;
use app\components\UploadImageBehavior;
use app\components\Settings;
use app\models\UserSocials;
/**
 * This is the model class for table "{{%rest_user}}".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $email
 * @property string $avatar
 * @property string $password
 * @property string $salt
 * @property string $role
 * @property integer $send_delivery_sms
 * @property integer $id_city
 * @property integer $balance
 * @property integer $active
 * @property string $activate_key
 * @property string $temp_recover_password
 * @property string $recover_password_key
 * @property integer $recover_valid_time
 * @property string $date_created
 * @property string $date_updated
 * @property integer $is_from_social
 * @property integer $delivery_material
 * @property integer $delivery_comment
 * @property integer $delivery_events
 * @property integer $publish_twitter
 * @property integer $publish_facebook
 * @property integer $publish_vkontakte
 */

class User extends ActiveRecord implements IdentityInterface, UserRbacInterface {
    
    const ROLE_USER = 'user';
    const ROLE_CLIENT = 'client';
    const ROLE_MODERATOR = 'moderator';
    const ROLE_ADMINISTRATOR = 'administrator';
    const ROLE_OPERATOR = 'operator';
    
    const STATUS_NEW                    = 0;
    const STATUS_ACTIVE                 = 1;
    const STATUS_NOT_EMAIL              = 2;
    const STATUS_BANNED                 = 10;
    const STATUS_BLOCK_FROM_SOCIAL      = 11;
    
    
    public $username;
    public $password2;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%user}}';
    }
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['email'], 'required', 'on' => ['signup', 'update', 'insert']],
            ['email', 'unique', 'message' => Yii::t('users', 'This email address has already been taken.'), 'on' => ['signup', 'update', 'insert']],
            
            
            [['password', 'salt', 'date_created'], 'required'],
            [['role'], 'string'],
            [['email'], 'string', 'max' => 255],
            ['role', 'in', 'range' => [self::ROLE_USER, self::ROLE_CLIENT, self::ROLE_MODERATOR, self::ROLE_ADMINISTRATOR, self::ROLE_OPERATOR], 'message' => 'Invalide role'],
            ['role', 'default', 'value' => self::ROLE_USER],
            [['send_delivery_sms', 'id_city', 'balance', 'active', 'recover_valid_time', 'delivery_material', 'delivery_comment', 'delivery_events', 'publish_twitter', 'publish_facebook', 'publish_vkontakte'], 'integer'],
            [['send_delivery_sms', 'balance', 'active', 'is_from_social', 'delivery_material', 'delivery_comment', 'delivery_events', 'publish_twitter', 'publish_facebook', 'publish_vkontakte'], 'default', 'value' => 0],
            [['active'], 'default', 'value' => self::STATUS_NEW],
            [['date_created', 'date_updated'], 'safe'],
            [['first_name', 'last_name', 'middle_name'], 'string', 'max' => 64],
            [['recover_password_key'], 'unique'],
            [['password', 'salt'], 'string', 'max' => 32],
            [['password', 'password2'], 'string', 'min' => 8],
            [['activate_key', 'temp_recover_password', 'recover_password_key'], 'string', 'max' => 100],
            
            //[['id', 'recover_password_key'], 'unique', 'targetAttribute' => ['recover_password_key'], 'message' => Yii::t('users', 'The combination of user and recover_password_key value has already been taken.')],
            ['avatar', 'file', 'extensions' => 'jpg, jpeg, gif, png', 'on' => ['insert', 'update']],
        ];
    }

    function behaviors() {
        return [
            [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'avatar',
                'scenarios' => ['insert', 'update'],
                'placeholder' => '@webroot/upload/default_avatar.jpg',
                'path' => '@webroot/upload/user/{id}',
                'url' => '@web/upload/user/{id}',
                'thumbPath' => '@webroot/upload/user/{id}/thumb',
                'thumbUrl' => '@web/upload/user/{id}/thumb',
                'thumbs' => [
                    'preview' => ['width' => 200, 'height' => 200],
                ],
            ],
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        $this->avatar =  ($this->avatar)?Yii::getAlias('@web').$this->getThumbUploadUrl('avatar', 'preview'):"null";
    }
    
    public function afterDelete() {
        parent::afterDelete();
        UserSocials::deleteAll(["user_id" => $this->id]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'first_name' => Yii::t('users', 'First Name'),
            'last_name' => Yii::t('users', 'Last Name'),
            'middle_Name' => Yii::t('users', 'Middle Name'),
            'email' => Yii::t('users', 'Email'),
            'avatar' => Yii::t('users', 'Avatar'),
            'password' => Yii::t('users', 'Password'),
            'password2' => Yii::t('users', 'New password'),
            'salt' => Yii::t('users', 'Salt'),
            'role' => Yii::t('users', 'Role'),
            'send_delivery_sms' => Yii::t('users', 'Send Delivery Sms'),
            'id_city' => Yii::t('users', 'City ID'),
            'balance' => Yii::t('users', 'Balance'),
            'active' => Yii::t('users', 'Active'),
            'activate_key' => Yii::t('users', 'Activate Key'),
            'temp_recover_password' => Yii::t('users', 'Temp Recover Password'),
            'recover_password_key' => Yii::t('users', 'Recover Password Key'),
            'date_created' => Yii::t('users', 'Date Created'),
            'date_updated' => Yii::t('users', 'Date Updated'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        $user = static::findOne(['id' => $id]);
        if ($user)
            $user->username = $user->email;
        else $user = new User;
        return $user;
    }
    
    public function generateRecoverKey($id = false, $period = 3600) {
        $temp_password = md5(time().rand(2323, 77584));
        $this->temp_recover_password = $this->generatePasswordHash($temp_password, false);
        $result = false;
        while (!$result) {
            $key = md5($temp_password.$this->id.rand(934, 23444));
            if (!static::findOne(["recover_password_key" => $key])) {
                $result = true;
                $this->recover_password_key = $key;
                $this->recover_valid_time = time() + $period;
            }
        }
        if ($this->save()) {
            return true;
        }
        else return false;
    }
    
    public function changePasswordFromKey($password = false, $key = null) {
        if ($this->testRecoverKey($key)) {
            $this->password = $password;
            if ($this->validate()) {
                $this->setPassword($password);
                $this->recover_valid_time = time() - 3600;
                if ($this->save())
                    return true;
            }
        }
        return false;
    }
    
    public function testRecoverKey($key = null) {
        return (strcmp($this->recover_password_key, $key) == 0 && $this->recover_valid_time > time())?true:false;
    }
    
    public static function findUserFromRecoverKey($key = null) {
        return static::findOne(["recover_password_key" => $key]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        $user = static::findOne(['email' => $username]);
        if ($user) 
            $user->username = $user->email;
        return $user;
    }
    

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        $hash = $this->password;
        if (!is_string($password) || empty($password)) {
            $this->addError('password', Yii::t('user', 'Password must be a string and cannot be empty.'));
            return false;
        }
        
        $test = md5($password.$this->salt);
        $n = strlen($test);
        if ($n !== 32) {
            return false;
        }
        
        return $result = $this->compareString($test, $hash);
        //Yii::$app->debug->show($test." ".$hash);
    }

    public function compareString($expected, $actual) {
        $expected .= "\0";
        $actual .= "\0";
        $expectedLength = StringHelper::byteLength($expected);
        $actualLength = StringHelper::byteLength($actual);
        $diff = $expectedLength - $actualLength;
        for ($i = 0; $i < $actualLength; $i++) {
            $diff |= (ord($actual[$i]) ^ ord($expected[$i % $expectedLength]));
        }
        return $diff === 0;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password = $this->generatePasswordHash($password);
    }

    public function generatePasswordHash($password, $change_salt = true) {
        if ($change_salt)
            $this->salt = md5(time() . rand(1, 999));
        $hash = md5($password.$this->salt);
        if (!is_string($hash) || strlen($hash) !== 32) {
            throw new Exception(Yii::t('users', 'Unknown error occurred while generating hash.'));
        }
        return $hash;
    }

    public function getAuthKey() {
        
    }

    public function validateAuthKey($authKey) {
        
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserActivity() {
        return $this->hasOne(UserActivity::className(), ['id' => 'id_user']);
    }
    
    public static function getRoleList() {
        $list = array();
        $user = new User;
        foreach ($user->getValidators('role') as $validator) {
            if ($validator instanceof \yii\validators\RangeValidator && $validator->range !== null) {
                foreach($validator->range as $key => $value) 
                    $list[$value] = ucfirst($value);
                break;
            }
        }
        return $list;
    }
    
    public static function isAdmin($id = false) {
        if (!$id && !Yii::$app->user->isGuest) 
            $id = Yii::$app->user->id;
        $user = User::findOne(["id" => $id]);
        return ($user && in_array($user->role, [self::ROLE_ADMINISTRATOR]))?true:false;
    }
    
    public function search($params) {
        $query = User::find();
        //$query->orderBy("id DESC");
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        return $dataProvider;
    }
    
    public static function sendMail($from, $to, $text, $subject = null, $reply = null) {
        $result = array();
        
        if (!preg_match("/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/",$from)) {
            return Yii::t('users', 'Wrong format mail from');
        } elseif (!preg_match("/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/",$to)) {
            return Yii::t('users', 'Wrong format mail to');
        }
        
        $subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
		$headers = "From: $from\r\n";
        
        if ($reply) {
            if (!preg_match("/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/",$reply)) {
                return Yii::t('users', 'Wrong format mail reply');
            } else {
                $headers .= "Reply-To: $reply\r\n";
            }
        }
        
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=utf-8\r\n";
        $headers .= "Content-Transfer-Encoding: base64\r\n\r\n";
        $text = base64_encode($text);
        
        return mail($to, $subject, $text, $headers);
    }
    
    public static function getNameOfUser($id = false) {
        $user = User::findOne(['id' => $id]);
        return ($user)?$user->getAttribute('first_name'):false;
    }
    
    public function getUserName() {
        return $this->first_name." ({$this->email})";
    }
    
    public function getFIO() {
        return $this->first_name." ".$this->last_name;
    }
    
    public static function getUserAsArray($id = false) {
        $user = User::findIdentity($id);
        return $result = [
            "id" => $user->id,
            "image" => $user->avatar,
            "username" => $user->username, 
            "role" => $user->role,
            "first_name" => $user->first_name,
            "last_name" => $user->last_name,
            "middle_name" => $user->middle_name

        ];
    }
    
    public static function getRolesForUser($id = false) {
        $roles = Yii::$app->authManager->getRolesByUser($id);
        $_roles = [];
        foreach ($roles as $role) 
            $_roles[] = $role->description;
        return !empty($_roles)?$_roles:false;
    }
    
    public static function countComments($id = false) {
        $count = 0;
        $materials = ["articles", "blogs", "events", "news"];
        foreach ($materials as $material) {
            $class = Settings::getInstance()->getClassName($material, Settings::COMMENT);
            $count += $class::find()->where(["id_user" => $id])->count();
        }
        return $count;
    }
    
    public static function countPosts($id = false) {
        $class = Settings::getInstance()->getClassName("blogs", Settings::MATERIAL);
        return $class::find()->where(["id_user" => $id, "is_show" => $class::STATUS_PUBLISHED])->count();
    }
    
    public function getSocials() {
        $result = [];
        if ($items = UserSocials::find()->where(['user_id' => $this->id])->all()) {
            foreach ($items as $item) 
                $result[$item['source']] = $item['source_id'];
        }
        return $result;
    }
    
    public function getPosts() {
        $result = [];
        $class = Settings::getInstance()->getClassName("blogs", Settings::MATERIAL);
        if ($items = $class::find()->where(['id_user' => $this->id, "is_show" => $class::STATUS_PUBLISHED])->orderBy("`date_created` DESC")->all()) {
            foreach ($items as $item) 
                $result[$item->id] = $item;// ["path" => $item->slug, "title" => $item->title, "text" => $item->short_text];
        }
        return $result;
    }
    
    public function deliveryStatus($key) {
        return (isset($this->{"delivery_".$key}) && $this->{"delivery_".$key})?true:false;
    }
    
    public function publishStatus($key) {
        return (isset($this->{"publish_".$key}) && $this->{"publish_".$key})?true:false;
    }
    
    public static function isChecked($field = false) {
        return in_array($field, ['delivery_material', 'delivery_comment', 'delivery_events', 'publish_twitter', 'publish_facebook', 'publish_vkontakte']);
    }
    
    public static function isMe($id = false) {
        return (!Yii::$app->user->isGuest && Yii::$app->user->id == $id)?true:false;
    }
}

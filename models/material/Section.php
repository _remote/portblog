<?php

namespace app\models\material;

use Yii;
use app\models\Language;
use app\models\ActiveRecord;

/**
 * This is the model class for table "section".
 *
 * @property integer $id
 * @property integer $id_language
 * @property integer $id_main
 * @property string $name
 * @property string $comment
 */
class Section extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_language', 'name'], 'required'],
            [['id_language', 'id_main'], 'integer'],
            [['name',], 'string', 'max' => 100],
            [['comment'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_language' => Yii::t('material', 'Language'),
            'name' => Yii::t('material', 'Name'),
            'comment' => Yii::t('material', 'Comment'),
        ];
    }
    
    public function getMaterials() {
        return $this->hasMany(Material::className(), ['id_section' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'id_language']);
    }

    public static function getItemsList($language = "ru") {
        $list[] = ["id" => 0, "name" => Yii::t('common', "Все"),];
        $items = Section::find()->where(['id_language' => Language::getLanguageId($language)])->all();
        foreach ($items as $item) {
            $_item = $item->fieldsValue($language, ["id", "name"]);
            $sublist = [];
            $subitems = SubSection::find()->where(["id_section" => $item->id])->all();
            foreach ($subitems as $sub)
                $sublist[] = $sub->fieldsValue($language, ["id", "name"]);
            $_item['items'] = $sublist;
            $list[] = $_item;
        }
        return $list;
    }

    public static function getLocalList($id = false) {
        return Section::find()->where(["id_main" => $id])->orderBy("id_language ASC")->asArray()->all();
    }

}

<?php

namespace app\models\material;

use Yii;
use app\models\ActiveRecord;

/**
 * This is the model class for table "news_subscriptions".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_city
 * @property integer $id_section
 * @property integer $id_subsection
 * @property string $date_create
 */
class Subscription extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_user'], 'required'],
            [['id_user', 'id_city', 'id_section', 'id_subsection'], 'integer'],
            [['date_create'], 'safe'],
            [['id_user', 'id_city'], 'unique', 'targetAttribute' => ['id_user', 'id_city'], 'message' => Yii::t('material', 'The combination of Id User and Id City has already been taken.')],
            [['id_user', 'id_section'], 'unique', 'targetAttribute' => ['id_user', 'id_section'], 'message' => Yii::t('material', 'The combination of Id User and Id Section has already been taken.')],
            [['id_user', 'id_subsection'], 'unique', 'targetAttribute' => ['id_user', 'id_subsection'], 'message' => Yii::t('material', 'The combination of Id User and Id Sub Section has already been taken.')]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_user' => Yii::t('material', 'User'),
            'id_city' => Yii::t('material', 'City'),
            'id_section' => Yii::t('material', 'Section'),
            'id_subsection' => Yii::t('material', 'Sub Section'),
            'date_create' => Yii::t('material', 'Date Create'),
        ];
    }

}

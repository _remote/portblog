<?php

namespace app\models\material;

use Yii;
use yii\data\ActiveDataProvider;

use app\models\User;
use app\models\ActiveRecord;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property integer $id_material
 * @property integer $id_comment
 * @property integer $id_user
 * @property string $text
 * @property string $date_created
 * @property integer $is_show
 */
class Comment extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['is_show', 'default', 'value' => 1],
            [['id_material', 'id_user', 'text', 'date_created', 'is_show'], 'required'],
            [['id_material', 'id_comment', 'id_user', 'is_show'], 'integer'],
            [['text'], 'string'],
            [['id_material', 'id_user', 'text', 'date_created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_material' => Yii::t(MODULE_ID, 'Material'),
            'id_comment' => Yii::t('material', 'Answer'),
            'id_user' => Yii::t('material', 'User'),
            'text' => Yii::t('material', 'Text'),
            'date_created' => Yii::t('material', 'Date'),
            'is_show' => Yii::t('material', 'Is Show'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
    
    public function afterValidate() {
        parent::afterValidate();
        $this->testObject();
    }
    
    public function testObject() {
        return true;
    }

    public function search($params, $id_object = false) {
        $query = static::find();
        if ($id_object)
            $query->andWhere(["id_material" => $id_object]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 50],
        ]);
        
        $dataProvider->sort = ['defaultOrder' => ['id'=>SORT_DESC]];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id_material' => $this->id_material]);
        $query->andFilterWhere(['id_comment' => $this->id_comment]);
        $query->andFilterWhere(['id_user' => $this->id_user]);
        $query->andFilterWhere(['like', 'text', $this->text]);
        $query->andFilterWhere(['like', 'date_created', $this->date_created]);
        $query->andFilterWhere(['is_show' => $this->is_show]);

        return $dataProvider;
    }
    
    //Сортировка комментариев в виде дерева
    private static function sortComments($list = []) {
        //Сортируем список на дочерние и родительские
        $chields = [];
        $comments = [];
        foreach ($list as $id => &$item) {
            if (empty($item['id_comment'])) {
                $item['level'] = 1;
                $comments[] = $item;
            } else
                $chields[] = $item;
        }
        //Раскидываем дочерние элементы по родительским
        foreach ($chields as $item) {
            static::putChieldItem($comments, $item);
        }
        return $comments;
    }

    //Рекурсия. Ходит по дереву, исипользуется для сортировки
    private static function putChieldItem(&$list, &$subitem, $level = 1) {
        foreach ($list as $key => &$item) {
            if ($item['id'] == $subitem['id_comment']) {
                $subitem['level'] = $level + 1;
                $item['items'][] = $subitem;
                return true;
            } else if (isset($item['items']) && is_array($item['items'])) {
                if (static::putChieldItem($item['items'], $subitem, $level + 1))
                    return true;
            } else
                continue;
        }
        return false;
    }

    //Выдает комментарии для новости в виде сортированного дерева
    public static function getSortedCommentsList($id = false) {
        $comments = static::findAll(["id_material" => $id]);
        $list = [];
        foreach ($comments as $comment)
            $list[$comment['id']] = $comment->fieldsValue();
        $list = static::sortComments($list);
        return $list;
    }


    //Из дерева комментариев вытаскивает уровни каждого и формирует одномерный
    //массив в виде айдишника комментария и его уровень
    public static function getCommentsLevel($list = [], &$levels = []) {
        foreach ($list as $item) {
            $levels[$item['id']] = $item['level'];
            if (isset($item['items']) && is_array($item['items']))
                static::getCommentsLevel($item['items'], $levels);
        }
        return $levels;
    }

    //Использует метод getSortedCommentsList;
    //Формирует в массив первого уровня из вложенных элементов
    public static function getCommentsList($list = [], &$list1 = []) {
        foreach ($list as &$item) {
            $list1[$item['id']] = $item;
            if (isset($item['items']) && is_array($item['items'])) {
                static::getCommentsList($item['items'], $list1);
                unset($list1[$item['id']]['items']);
            }
        }
        return $list1;
    }

    public static function getChieldComments($id = false, $id_comment = false, $list = false, &$ids = []) {
        if (!$list)
            $list = static::getSortedCommentsList($id);
        foreach ($list as $item) {
            if ($item['id_comment'] == $id_comment || $item['id'] == $id_comment)
                $ids[] = $item['id'];
            if (isset($item['items']) && is_array($item['items'])) {
                static::getChieldComments($id, $item['id'], $list, $ids);
            }
        }
        return $ids;
    }

    public function formatDate($timestamp = false) {
        if (!$timestamp)
            $timestamp = strtotime($this->date_created);
        if (!$timestamp)
            return "";
        $datetime1 = new \DateTime('@' . time());
        $datetime2 = new \DateTime('@' . $timestamp);
        $interval = $datetime1->diff($datetime2);

        $years = $interval->format('%y');
        $months = $interval->format('%m');
        $days = $interval->format('%a');
        $hours = $interval->format('%h');
        $minutes = $interval->format('%i');
        $seconds = $interval->format('%S');

        $elapsed = "Now";
        if ($seconds && $seconds != "00"):
            $elapsed = $seconds == 1 ? $seconds . ' Second ' : $seconds . ' Seconds ';
        endif;
        if ($minutes):
            $elapsed = $minutes == 1 ? $minutes . ' Minute ' : $minutes . ' Minutes ';
        endif;
        if ($hours):
            $elapsed = $hours == 1 ? $hours . ' Hour ' : $hours . ' Hours ';
        endif;
        if ($days):
            $elapsed = $days == 1 ? $days . ' Day ' : $days . ' Days ';
        endif;
        if ($months):
            $elapsed = $months == 1 ? $months . ' Month ' : $months . ' Months ';
        endif;
        if ($years):
            $elapsed = $years == 1 ? $years . ' Year ' : $years . ' Years ';
        endif;

        return $elapsed;
    }

}

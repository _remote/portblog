<?php

namespace app\models\material\search;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property integer $id_area
 * @property integer $id_city
 * @property integer $id_user
 * @property string $title
 * @property string $short_text
 * @property string $text
 * @property string $image
 * @property string $video
 * @property string $date_created
 * @property string $date_update
 * @property string $site
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property integer $view_count
 * @property integer $is_show
 * @property integer $is_show_in_top
 * @property string $comment
 */
class Material extends \app\models\material\Material {

    public $_section;
    public $_sub_section;
    public $username;
    
    //public static abstract function materialName();

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_user', 'id_language', '_section', '_sub_section', 'username', 'date_created', 'date_update', 'title', 'text', 'is_show'], 'safe'],
        ];
    }
    
//    /**
//    * @inheritdoc
//    */
//    public function scenarios() {
//        // bypass scenarios() implementation in the parent class
//        return Model::scenarios();
//    }

    public function search($params, $id = false) {
        $query = ($id) ? Material::find()->where(["id_user" => $id]) : Material::find();
        $query->select("section.name, subsection.name, ".static::tableName().".*");
        $query->andWhere(static::tableName().'.`id_main` = '.static::tableName().'.`id`'); //Выборка только основных новостей
        $query->joinWith([
            'section' => function($query) {
                $query->from(['section' => 'section']);
            },
            'subsection' => function($query) {
                $query->from(['subsection' => 'subsection']);
            },
            'user']
        );
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort = ['defaultOrder' => ['id'=>SORT_DESC]];
        $dataProvider->sort->attributes['_section'] = [
            'asc' => ['section.name' => SORT_ASC],
            'desc' => ['section.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['_sub_section'] = [
            'asc' => ['subsection.name' => SORT_ASC],
            'desc' => ['subsection.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['username'] = [
            'asc' => ['user.email' => SORT_ASC],
            'desc' => ['user.email' => SORT_DESC],
        ];

        //print_r($query->createCommand()->getRawSql()); die();
        

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id_user' => $this->id_user]);
        $query->andFilterWhere(['news.id_language' => $this->id_language]);
        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'section.name', $this->_section]);
        $query->andFilterWhere(['like', 'subsection.name', $this->_sub_section]);
        $query->andFilterWhere(['like', 'news.date_created', $this->date_created]);
        $query->andFilterWhere(['is_show' => $this->is_show]);

        return $dataProvider;
    }

}
        
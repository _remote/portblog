<?php

namespace app\models\search;

use Yii;
use yii\data\ActiveDataProvider;
use app\models\News;
use app\models\User;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property integer $id_new
 * @property integer $id_comment
 * @property integer $id_user
 * @property string $text
 * @property string $date_created
 * @property integer $is_show
 */
class Comment extends \app\models\Comment {
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_new', 'id_comment', 'id_user', 'text', 'date_created', 'is_show'], 'safe'],
        ];
    }
    
    public function testObjects() {
        
    }

    public function search($params, $id_object = false) {
        $query = Comment::find();
        if ($id_object)
            $query->andWhere(["id_new" => $id_object]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 50],
        ]);
        
        $dataProvider->sort = ['defaultOrder' => ['id'=>SORT_DESC]];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id_new' => $this->id_new]);
        $query->andFilterWhere(['id_comment' => $this->id_comment]);
        $query->andFilterWhere(['id_user' => $this->id_user]);
        $query->andFilterWhere(['like', 'text', $this->text]);
        $query->andFilterWhere(['like', 'date_created', $this->date_created]);
        $query->andFilterWhere(['is_show' => $this->is_show]);

        return $dataProvider;
    }

}

<?php

namespace app\models\material;

use Yii;
use yii\data\ActiveDataProvider;
use app\components\UploadBehavior;
use app\components\UploadImageBehavior;
use yii\helpers\Url;

use app\models\User;
use app\models\Language;
use app\models\ActiveRecord;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property integer $id_main
 * @property integer $id_area
 * @property integer $id_city
 * @property integer $id_user
 * @property integer $id_language
 * @property string $title
 * @property string $short_text
 * @property string $text
 * @property string $image_file
 * @property string $image
 * @property string $video
 * @property string $date_created
 * @property string $date_update
 * @property string $site
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property integer $view_count
 * @property integer $is_show
 * @property integer $is_show_in_top
 * @property integer $is_selected
 * @property integer $gallery
 * @property string $comment
 */
class Material extends ActiveRecord {

    public $_section;
    public $_sub_section;
    public $username;
    
    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;
    const STATUS_ARCHIVED = 2;
    const STATUS_BANNED = 3;
    
    
    

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_area', 'id_main', 'id_city', 'id_user', 'id_language', 'id_section', 'id_subsection', 'view_count', 'is_show', 'is_show_in_top', 'is_selected', 'gallery'], 'integer'],
            [['id_section', 'id_subsection', 'id_user', 'id_language', 'title', 'text', 'date_created'], 'required'],
            [['short_text', 'text'], 'string'],
            ['image_file', 'file', 'extensions' => 'jpg, jpeg, gif, png', 'on' => ['insert', 'update']/* , 'maxFiles' => 5 */],
            [['_section', '_sub_section', 'username', 'date_created', 'date_update'], 'safe'],
            [['image', 'video', 'site'], 'string', 'max' => 255],
            [['title'], 'string', 'max' => 1000],
            ['image', 'url'],
            [['seo_title', 'seo_description', 'seo_keywords', 'comment'], 'string', 'max' => 512],
            [['view_count', 'is_show', 'is_show_in_top', 'is_selected', 'gallery'], 'default', 'value' => 0],
            ['is_show', 'in', 'range' => [self::STATUS_DRAFT, self::STATUS_PUBLISHED, self::STATUS_ARCHIVED, self::STATUS_BANNED], 'message' => 'Invalide status'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_area' => Yii::t("material", 'Country'),
            'id_city' => Yii::t("material", 'City'),
            'id_user' => Yii::t("material", 'User'),
            'id_language' => Yii::t("material", 'Language'),
            'title' => Yii::t("material", 'Title'),
            'id_section' => Yii::t("material", 'Type'),
            'id_subsection' => Yii::t("material", 'Sub Type'),
            'short_text' => Yii::t("material", 'Short Text'),
            'text' => Yii::t("material", 'Text'),
            'image_file' => Yii::t("material", 'Image'),
            'image' => Yii::t("material", 'Image link'),
            'video' => Yii::t("material", 'Video'),
            'date_created' => Yii::t("material", 'Date Created'),
            'date_update' => Yii::t("material", 'Date Update'),
            'site' => Yii::t("material", 'Site'),
            'seo_title' => Yii::t("material", 'Seo Title'),
            'seo_description' => Yii::t("material", 'Seo Description'),
            'seo_keywords' => Yii::t("material", 'Seo Keywords'),
            'view_count' => Yii::t("material", 'View Count'),
            'is_show' => Yii::t("material", 'Status'),
            'is_show_in_top' => Yii::t("material", 'Show in top'),
            'is_selected' => Yii::t("material", 'Selected'),
            'gallery' => Yii::t("material", 'Gallery'),
            'comment' => Yii::t("material", 'Comment'),
            '_section' => Yii::t("material", 'Section'),
            '_sub_section' => Yii::t("material", 'Sub-Section'),
        ];
    }
    
    public static function getStatusLabels() {
        return [
            self::STATUS_DRAFT => "Пишется",
            self::STATUS_PUBLISHED => "Опубликована",
            self::STATUS_ARCHIVED => "В архиве",
            self::STATUS_BANNED => "Заблокирована",
        ];
    }

    function behaviors() {
        return [
            [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'image_file',
                'scenarios' => ['insert', 'update'],
                'placeholder' => '@webroot/upload/default.jpg',
                'path' => '@webroot/upload/'.MODULE_ID.'/{id}',
                'url' => '@web/upload/'.MODULE_ID.'/{id}',
                'thumbPath' => '@webroot/upload/'.MODULE_ID.'/{id}/thumb',
                'thumbUrl' => '@web/upload/'.MODULE_ID.'/{id}/thumb',
                'thumbs' => [
                    'thumb' => ['width' => 400, 'quality' => 90],
                    'preview' => ['width' => 200, 'height' => 200],
                ],
            ],
        ];
    }

    public function afterFind() {
        parent::afterFind();
        if (!empty($this->image_file)) {
            $this->image = Yii::getAlias('@web') . $this->getUploadUrl('image_file');
            $this->image_file = Yii::getAlias('@web') . $this->getThumbUploadUrl('image_file', 'preview');
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'id_language']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
    
    
    public static function updateGalleryStatus($id = false, $status = false) {
        static::updateAll(["gallery" => ($status?1:0)], ["id_main" => $id]);
    }

    public static function getLanguagesList($id = false) {
        $languages = [];
        $news = static::find()->where(["id_main" => $id])->all();
        foreach ($news as $new)
            $languages[$new->id_language] = Language::getTypeOfLanguage($new->id_language);
        return $languages;
    }

    public static function getMaterialsList($id = false) {
        return static::find()->where(["id_main" => $id])->orderBy("id_language ASC")->asArray()->all();
    }
    
    public static function getStatusItems() {
        $list = array();
        $class = 'app\modules\\'.MODULE_ID.'\\models\\'.static::getShortName();
        $object = new $class;
        foreach ($object->getValidators('is_show') as $validator) {
            if ($validator instanceof \yii\validators\RangeValidator && $validator->range !== null) {
                foreach($validator->range as $key => $value) 
                    $list[$value] = static::getStatusName($value);
                break;
            }
        }
        return $list;
    }
    
    public static function getStatusName($status = false) {
        $list = static::getStatusLabels();
        return (isset($list[$status]))?$list[$status]:"";
    }

    public function search($params, $id = false) {
        $query = ($id) ? static::find()->where(["id_user" => $id]) : static::find();
        $query->from(["material" => MODULE_ID]);
        $query->select("section.name, subsection.name, material.*");
        $query->andWhere('material.`id_main` = material.`id`'); //Выборка только основных новостей
        $query->joinWith([
            'section' => function($query) {
                $query->from(['section' => MODULE_ID.'_section']);
            },
            'subsection' => function($query) {
                $query->from(['subsection' => MODULE_ID.'_subsection']);
            },
            'user']
        );
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort = ['defaultOrder' => ['id'=>SORT_DESC]];
        $dataProvider->sort->attributes['_section'] = [
            'asc' => ['section.name' => SORT_ASC],
            'desc' => ['section.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['_sub_section'] = [
            'asc' => ['subsection.name' => SORT_ASC],
            'desc' => ['subsection.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['username'] = [
            'asc' => ['user.email' => SORT_ASC],
            'desc' => ['user.email' => SORT_DESC],
        ];

        //print_r($query->createCommand()->getRawSql()); die();
        

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id_user' => $this->id_user]);
        $query->andFilterWhere(['material.id_language' => $this->id_language]);
        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'section.name', $this->_section]);
        $query->andFilterWhere(['like', 'subsection.name', $this->_sub_section]);
        $query->andFilterWhere(['like', 'material.date_created', $this->date_created]);
        $query->andFilterWhere(['is_show' => $this->is_show]);

        return $dataProvider;
    }

    //Выдает новость в виде массива со списком комментариев к ней
    public static function getMaterialWithComments($id = false, $language = "ru", $select = "*") {
        $material = static::findOne(["id" => $id]);
        if ($material) {
            $class = 'app\modules\\'.MODULE_ID.'\\models\\Comment';
            $result = $material->fieldsValue($language, $select);
            $count = $class::find()->where(["id_material" => $id])->count();
            $result['comments'] = ["comment_ru" => "Комментарии", "total_count" => $count];
            $result['comments']['items'] = $class::getSortedCommentsList($id);
            $result['type'] = MODULE_ID;
            return $result;
        } else
            return false;
    }
}

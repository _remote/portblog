<?php

namespace app\models\material;

use Yii;
use app\models\Language;
use app\models\ActiveRecord;

/**
 * This is the model class for table "subsection".
 *
 * @property integer $id
 * @property integer $id_section
 * @property integer $id_language
 * @property integer $id_main
 * @property string $name
 * @property string $comment
 */
class SubSection extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_section', 'id_language', 'name'], 'required'],
            [['id_section', 'id_language', 'id_main'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['comment'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_section' => Yii::t('material', 'Owner Type'),
            'id_language' => Yii::t('material', 'Language'),
            'name' => Yii::t('material', 'Name'),
            'comment' => Yii::t('material', 'Comment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwnertype() {
        return $this->hasOne(Section::className(), ['id' => 'id_section']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'id_language']);
    }

    public static function getLocalList($id = false) {
        return static::find()->where(["id_main" => $id])->orderBy("id_language ASC")->asArray()->all();
    }

}

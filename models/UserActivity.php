<?php

namespace app\models;

use Yii;
use app\modules\blog\models\BlogPosts;
use app\modules\blog\models\BlogComments;

use app\models\ActiveRecord;
use app\components\Settings;

/**
 * This is the model class for table "user_activity".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $post
 * @property integer $comment
 * @property integer $like
 * @property integer $total_count
 */
class UserActivity extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user_activity';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['post', 'comment', 'like', 'total_count'], 'default', 'value' => 0],
            [['id_user', 'post', 'comment', 'like', 'total_count'], 'integer'],
            [['id_user'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_user' => Yii::t('users', 'Id User'),
            'post' => Yii::t('users', 'Post'),
            'comment' => Yii::t('users', 'Comment'),
            'like' => Yii::t('users', 'Like'),
            'total_count' => Yii::t('users', 'Total Count'),
        ];
    }
    
    public static function getUserActivity($id_user = false) {
        $model = UserActivity::findOne(["id_user" => $id_user]);
        if (!$model && $id_user) {
            $model = new UserActivity;
            $model->id_user = $id_user;
            $model->post = BlogPosts::find()->joinWith(["blog"])->where(["blog.id_user" => $id_user])->count();
            $blog_comments = BlogComments::find()->where(["id_user" => $id_user])->count();
            Settings::getInstance()->setModuleId("news");
            $class = Settings::getInstance()->getModulePath() . '\models\search\Comment';
            $news_comments = $class::find()->where(["id_user" => $id_user])->count();
            $model->comment = $blog_comments + $news_comments;
            $model->like = 0;
            $model->total_count = $model->post + $model->comment + $model->like;
            $model->save();
        }
        return $model;
    }

}

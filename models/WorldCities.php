<?php

namespace app\models;

use Yii;
use app\models\Language;

/**
 * This is the model class for table "world_cities".
 *
 * @property integer $id
 * @property integer $id_language
 * @property integer $id_main
 * @property string $name
 * @property integer $country
 * @property string $latitude
 * @property string $longitude
 */
class WorldCities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'world_cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'country'], 'required'],
            [['country', 'id_language', 'id_main'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['name'], 'string', 'max' => 128],
            [['slug'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_language' => Yii::t('cities', 'Language'),
            'name' => Yii::t('cities', 'Name'),
            'country' => Yii::t('cities', 'Country'),
            'region' => Yii::t('cities', 'Region'),
            'latitude' => Yii::t('cities', 'Latitude'),
            'longitude' => Yii::t('cities', 'Longitude'),
            'slug' => Yii::t("material", 'Path'),
        ];
    }
    
    function behaviors() {
        return [
            'slug' => [
                    'class' => 'app\modules\material\components\behaviors\Slug',
                    'in_attribute' => 'name',
                    'out_attribute' => 'slug',
                    'translit' => true
            ]
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'id_language']);
    }
    
    public static function getName($id = false) {
        $country = WorldCities::findOne(["id" => $id]);
        return ($country)?$country->name:null;
    }
    
    public static function getLocalList($id = false) {
        return WorldCities::find()->where(["id_main" => $id])->orderBy("id_language ASC")->asArray()->all();
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_messages".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_comment
 * @property string $text
 * @property string $date_created
 * @property integer $is_show
 */
class UserMessages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'text', 'date_created', 'is_show'], 'required'],
            [['id_user', 'id_comment', 'is_show'], 'integer'],
            [['text'], 'string'],
            [['date_created'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_comment' => 'Id Comment',
            'text' => 'Text',
            'date_created' => 'Date Created',
            'is_show' => 'Is Show',
        ];
    }
}

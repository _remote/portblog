<?php

namespace app\models;

use Yii;
use \app\modules\settings\models\Settings;
use app\modules\settings\models\SettingsMail;

/**
 * 'mailer' => [
 *           'class' => 'yii\swiftmailer\Mailer',
 *           'viewPath' => '@backend/mail',
 *           'useFileTransport' => false,//set this property to false to send mails to real email addresses
 *           //comment the following array to send mail using php's mail function
 *           'transport' => [
 *               'class' => 'Swift_SmtpTransport',
 *               'host' => 'smtp.gmail.com',
 *               'username' => 'username@gmail.com',
 *               'password' => 'password',
 *               'port' => '587',
 *               'encryption' => 'tls',
 *           ],
 *       ],
 * */
class Mail extends Model {

    public $email;
    
    public static $exception = null;
    
    //Данные значения беруться из класса app\modules\settings\models\Settings;
    const MAIL_DELIVERY = 'mail_delivery';
    const MAIL_FOR_USER = 'mail_for_user';
    const MAIL_INFO = 'mail_info';
    const MAIL_ADMIN = 'mail_admin';
    

    public function rules() {
        return [
        ];
    }
    
    public static function getMailBoxId($type = false) {
        $id = false;
        if ($settings = Settings::getAll()) {
            $field = "id_".$type;
            $id = (!empty($settings[$field]))?$settings[$field]:false;
        }
        return $id;
    }

    public static function send($type = false, $to, $subject, $view = "test/html", $params = [], $from = "info.portblog@yandex.ru") {
        //Вытаскиваем настройки из базы, если не пустые, то задаем их для отправки
        $id_mail_box = self::getMailBoxId($type);
        $base = "";
        if ($mail = SettingsMail::findOne(["id" => $id_mail_box])) {
            $from = $mail->email;
            $transport = [
                'class' => 'Swift_SmtpTransport',
                'host' => $mail->host,
                'username' => $mail->username,
                'password' => $mail->password,
                'port' => $mail->port,
                'encryption' => $mail->encryption,
            ];
            Yii::$app->mailer->setTransport($transport);
        }
        
        //Yii::$app->mailer->htmlLayout(["layouts/html", "test" => 1]);
        
        
        
        try {
            return \Yii::$app->mailer->compose($view, ['params' => $params])
                ->setFrom($from)
                ->setTo($to)
                ->setSubject($subject)
                //->setHtmlBody($html)
                ->send();
        } catch (\Swift_SwiftException $exception) {
            self::$exception = $exception;
            if (YII_ENV_DEV) {
                Yii::$app->debug->show($exception);
            }
            return false;
        }
    }

}

<?php

namespace app\models;

class Model extends \yii\base\Model {  
    
    public static function getShortName() {
        return  (new \ReflectionClass(self::className()))->getShortName(); 
    }
    
    public function prepare($data) {
        $class = $this::getShortName();
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $data[$class][$key] = $value;
            }
        }
        return $this->load($data);
    }
    
}


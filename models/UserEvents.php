<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "user_events".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $material
 * @property integer $id_material
 * @property string $message
 * @property string $object
 * @property string $date_created
 */
class UserEvents extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user_events';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_user', 'id_material'], 'integer'],
            [['date_created'], 'safe'],
            [['material', 'message', 'object'], 'string', 'max' => 255],
            [['object'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'material' => 'Material',
            'id_material' => 'Id Material',
            'message' => 'Message',
            'date_created' => 'Date Created',
        ];
    }
    
    private static function event() {
        if (Yii::$app->user->isGuest)
            return false;
        $event = new UserEvents;
        $event->id_user = Yii::$app->user->id;
        $event->date_created = date("Y-m-d G:i:s", time());
        return $event;
    }
    
    public static function message($text = "") {
        if ($event = self::event()) {
            $event->message = $text;
            if ($event->save())
                return true;
        }
    }
    
    public static function subscription($item = "object", $type = false, $object = false) {
        if ($event = self::event()) {
            $event->message = "You are subscribed to {object}";
            $event->object = json_encode(["object" => $item]);
            if ($event->save())
                return true;
        }
    }
    
    public static function unsubscription($item = "object", $type = false, $object = false) {
        if ($event = self::event()) {
            $event->message = "You are unsubscribed from {object}";
            $event->object = json_encode(["object" => $item]);
            if ($event->save())
                return true;
        }
    }
    
    public function search($params, $id = false) {
        $query = ($id)?UserEvents::find()->where(['id_user' => $id]):UserEvents::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $dataProvider->sort = ['defaultOrder' => ['date_created'=>SORT_DESC]];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        return $dataProvider;
    }

}

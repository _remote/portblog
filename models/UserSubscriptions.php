<?php

namespace app\models;

use Yii;
use app\models\ActiveRecord;
use yii\data\ActiveDataProvider;

use app\models\User;
use app\models\UserEvents;

/**
 * This is the model class for table "user_subscriptions".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_autor
 * @property integer $status
 * @property string $date_created
 * @property string $date_updated
 */
class UserSubscriptions extends ActiveRecord {
    
    const STATUS_SIGNED = 1;
    const STATUS_UNSIGNED = 0;
    

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user_subscriptions';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_user', 'id_autor', 'status'], 'integer'],
            ['status', 'in', 'range' => [self::STATUS_SIGNED, self::STATUS_UNSIGNED], 'message' => Yii::t('user', 'Invalide role')],
            [['date_created', 'date_updated'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_autor' => 'Id Autor',
            'status' => 'Status',
            'date_created' => 'Date Created',
        ];
    }
    
    public function getAutor() {
        return $this->hasOne(User::className(), ['id' => 'id_autor']);
    }
    
    public function inList($id = false) {
        if (Yii::$app->user->isGuest)
            return false;
        $user = UserSubscriptions::find()->where(["id_user" => Yii::$app->user->id, "id_autor" => $id])->one();
        return ($user)?$user:false;
    }
    
    public static function isSigned($id = false) {
        if (Yii::$app->user->isGuest)
            return false;
        $user = UserSubscriptions::find()->where([
            "id_user" => Yii::$app->user->id, 
            "id_autor" => $id, 
            "status" => self::STATUS_SIGNED
        ])->one();
        return ($user)?$user:false;
    }
    
    public static function sign($id = false) {
        if ($id && !Yii::$app->user->isGuest) {
            if ($subscription = self::isSigned($id)) {
                return true;
            } else {
                if ($user = User::findIdentity($id)) {
                    $subscription = new UserSubscriptions;
                    $subscription->id_user = Yii::$app->user->id;
                    $subscription->id_autor = intval($id);
                    $subscription->status = self::STATUS_SIGNED;
                    $subscription->date_created = date("Y-m-d G:i:s", time());
                    if ($subscription->save()) {
                        UserEvents::subscription($user->getFIO());
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public static function unsign($id = false) {
        if ($id && !Yii::$app->user->isGuest) {
            if ($subscription = self::isSigned($id)) {
                $subscription->status = self::STATUS_UNSIGNED;
                if ($subscription->save()) {
                    if ($user = User::findIdentity($id))
                        UserEvents::unsubscription($user->getFIO());
                    return true;
                }
            }
        }
        return false;
    }
    
    public function search($params, $id = false) {
        $query = ($id)?  UserSubscriptions::find()->where(['id_user' => $id]):UserSubscriptions::find();
        $query->andWhere(["status" => self::STATUS_SIGNED]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $dataProvider->sort = ['defaultOrder' => ['date_created'=>SORT_DESC]];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        return $dataProvider;
    }

}

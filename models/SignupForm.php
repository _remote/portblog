<?php

namespace app\models;

use app\models\User;
use app\models\Mail;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model {

    public $username;
    public $password;
    public $first_name;
    public $last_name;
    public $middle_name;
    public $role;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            [['username', 'first_name'], 'required', 'on' => 'signup'],
            ['username', 'email', 'on' => 'signup'],
            //['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            ['password', 'required', 'on' => 'signup'],
            ['password', 'string', 'min' => 6, 'on' => 'signup'],
            [['first_name', 'last_name', 'middle_name'], 'string'],
            ['role', 'default', 'value' => User::ROLE_USER],
        ];
    }
    
//    public function scenarios() {
//        $scenarios = parent::scenarios();
//        $scenarios['signup'] = ['username', 'first_name', 'password'];
//        return $scenarios;
//    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup($scenario = "signup") {
        if ($this->validate()) {
            $user = new User();
            $user->scenario = $scenario;
            $user->email = $this->username;
            $user->first_name = $this->first_name;
            $user->last_name = $this->last_name;
            $user->middle_name = $this->middle_name;
            $user->setPassword($this->password);
            $user->date_created = date("Y-m-d G:i:s", time());
            $user->role = $this->role;
            $user->active = User::STATUS_ACTIVE;
            if ($user->validate()) {
                if ($user->save()) {
                    if ($userRole = Yii::$app->authManager->getRole('user'))
                        Yii::$app->authManager->assign($userRole, $user->getId());
                    try {
                        //Mail::send(Mail::MAIL_FOR_USER, $user->email, "Подтверждение о регистрации", "registration/html", ["login" => $user->email, "first_name" => $user->first_name]);
                    }
                    catch (\Swift_TransportException $e) {
                        $this->addError("username", $e->getMessage());
                    }
                    return $user;
                }
            }
            else $this->addErrors ($user->getErrors ());
        }
    }

}

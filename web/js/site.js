window.onresize = function() {
    resize();
};

function  getPageHeight(){
    if (self.innerHeight) { // all except Explorer
        return self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
        return document.documentElement.clientHeight;
    } else if (document.body) { // other Explorers
        return document.body.clientHeight;
    }
}

function closeModal(selector) {
    $(selector).removeAttr('data-toggle');
    $(selector).removeAttr('data-target');
    $(selector).attr('type', 'submit');
    $("a.send-button-action").attr("data-target", "#commentForm");
    
    //$(".view-user-button-action").removeAttr('data-toggle');
    //$(".view-user-button-action").removeAttr('data-target');
    $(".create-post-button-action").removeAttr('data-toggle');
    $(".create-post-button-action").removeAttr('data-target');
    $('.modal').modal('hide');
    $.pjax.reload({container:"#_user_information"}).done(function() { 
        $.pjax.reload({container:"#_user_subscription"});
    });
    
}

function resize() {
    var w = $("div.content").width();
    $("#top").width(w);
    size = 4;
    $(".wrap").addClass("pc");
    $("#blog-create").addClass("bar");
    var h = $(".main_content img").height();
    $(".main_content .desc").addClass("small");
    if ($("#small_status").css("display") == "block") {
        size = 12; // 6 - если нужно в два столбика
        $(".wrap").removeClass("pc");
        $("#blog-create").removeClass("bar");
        $(".main_content .desc").removeClass("small");
        $(".main_content .desc").css("height", "auto");
    }
    $(".main_content .desc.small").css("height", h + "px");
    $(".material").each(function (index, item) {
        if (!$(item).hasClass("main") && !$(item).hasClass("blog")) {
            $(item).removeClass("col-xs-6");
            $(item).removeClass("col-xs-4");
            $(item).addClass("col-xs-"+size);
        }
    });
    
    //Для шаблона подгоняем боди по вертикали
    var h = getPageHeight() - $(".footer").height();
    $("body.personal .content.height .body").css("min-height", h + "px");
}

function setActiveSlide(element, type, id_material, id_image) {
    $.get("set-active-slide", {type:type, id_material:id_material, id_image:id_image}, function(data) {
        if (data.result) {
            var slider = $(element).parent().parent().parent();
            $(slider).find("img").each(function(indx, image){
                if ($(image).attr("rel") == id_image)
                    $(image).addClass("active");
                else $(image).removeClass("active");
            });
        } else bootbox.alert("Сервер вернул ошибку");
    }, "json")
    
}

function subscription(button, type, id) {
    $.get("/user/subscription", {type:type, id:id}, function(data) {
        if (data.success) {
            var block = $(button).parent();
            $(button).before(data.success);
            $(block).find("button:first").width($(button).width());
            $(button).remove();
        }
    }, "json");
}

function unsubscription(button, type, id) {
    $.get("/user/unsubscription", {type:type, id:id}, function(data) {
        if (data.success) {
            var block = $(button).parent();
            $(button).before(data.success);
            $(block).find("button:first").width($(button).width());
            $(button).remove();
        }
    }, "json");
}

function incrementViews(type, id) {
    $.get("/material/views-increment", {type:type, id:id}, function(data) {
        
    }, "json");
}

function viewComments(element) {
    var link = $(element).closest("a");
    var href = $(link).attr("href");
    $(link).attr("href", href + "#comments");
}

function rating(type, id, element) {
    var value = $(element).val();
    $.get("/material/change-rating", {type:type, id:id, value:value}, function(data, status, xhr) {
        if (data.result) {
            
        }
    }, "json");
}

function redirect(path) {
    if (typeof(path) !== "undefined")
        location = path;
    else window.location.reload();
}

function reloadPage(path, pause) {
    if (typeof(pause) === "undefined")
        pause = 1000;
    setTimeout(redirect, pause, path);
}

function closeAlert(message) {
    message.hide(800, function() {
        message.removeClass("alert").removeClass('alert-danger').removeClass('alert-success').removeClass('alert-dismissible')
    });
}

function showMessage(message, text, error) {
    var close = '<button type="button" class="close" data-dismiss="alert" aria-label="Закрыть"><span aria-hidden="true">&times;</span></button>';
    message.addClass("alert")
    if (error)
        message.addClass('alert-danger')
    else message.addClass('alert-success')
    message.addClass('alert-dismissible').html("\n"+text);
    message.show("fast");
    setTimeout(closeAlert, 10000, message);
}

function deleteSlide(element, id_image, message) {
    if (typeof(message) == "undefined")
        message = 'Are you sure you want to delete this items?';

    bootbox.confirm(message, function(status) {
        if (status) {
            $.get("delete-slide", {id:id_image}, function(data) {
                if (data.result) {
                    var slider = $(element).parent().parent();
                    $(slider).remove();
                } else bootbox.alert("Сервер вернул ошибку");
            }, "json");
        }
    });
}

function loadMore(button, id_section, full) {
    var material = $(button).attr("role");
    var page = $(button).attr("page");
    $.get("/material/load-more", {material:material, page:page, id_section:id_section, full:full}, function(data) {
        if (data) {
            $(button).parent().before(data);
            $(button).attr("page", parseInt(page) + 1);
            onImgLoad('.'+material+'-page-'+page+' .preload img', onImageLoadAction);
        }
        else $(button).animate({opacity: "hide"}, 500, function() {
            $(button).parent().remove();
        });
    }, "html");
}

var actions = {
    changePassword: function (form, data) {
        var message = $('div[role="alert"]');
        if (typeof(data.error) !== "undefined") 
            showMessage(message, data.error, true);
        else if (typeof(data.success) !== "undefined") 
            showMessage(message, data.success, false);
        else {
            $.each(data, function(index, value) {
                var mask = "input[id$='" + index + "']";
                var input = form.find(mask);
                input.closest('.form-group').removeClass('has-success').addClass('has-error')
                input.next('.help-block').html(value);
            });
        }
    },
    changeAvatar: function (form, data) {
        var message = $('div[role="alert"]');
        if (typeof(data.error) !== "undefined") 
            showMessage(message, data.error, true);
        else if (typeof(data.avatar) !== "undefined") {
            $('#user_avatar').attr("src", data.avatar);
            $('.avatar').each(function(index, item) {
                if(!($(item).parent().hasClass('list')))
                    $(item).find("img").attr("src", data.avatar);
            });
            
        }
    },
    callback: function (form, data) {
        if (typeof(data.error) !== "undefined") 
            bootbox.alert(data.error);
        if (typeof(data.success) !== "undefined") 
            bootbox.alert(data.success);
    }
};

$(function () {
    /* выбор города */
    $('.languages_select').click(function(){
        $(".languages_list").slideToggle('fast');
    });
    $('ul.languages_list li').click(function(){
        var tx = $(this).find("img").attr("src");
        var language = $(this).attr('rel');
        $(".languages_list").slideUp('fast');
        $.get("/site/change-language", {language:language}, function() {
            window.location = "/"+language;
            //$(".languages_select span").find("img").attr("src", tx);
        }, "json");
    });
    
    resize();
    var is_avaible = true;
    var banner = $(".banner.top");
    var bh = $(banner).height();
    var offset = bh / 2;
    var inProgress = true;
    var p = $("#top").offset();
    //$(".body").css("margin-top", offset + "px");
    $(window).scroll(function() {
        if (banner) {
            var scroll = $(window).scrollTop();
            var top = (scroll < bh / 2)?bh - scroll:scroll; //Смещение менюшки относительно скролла
            var bp = (scroll < offset)?offset - scroll:0; //Смещение боди относительно скролла
            var pt = $("#top").offset();
            $("#lenta.body").css("margin-top", bp + "px");
            $("#top").offset({top:top});
        }
        
        //console.log(scroll + " " + top + " " + pt.top);
        /* Если высота окна + высота прокрутки больше или равны высоте всего документа и ajax-запрос в настоящий момент не выполняется, то запускаем ajax-запрос */
        if($(window).scrollTop() + $(window).height() >= $(document).height() - 200 && !inProgress) {
            
        }
    });
    
    var hash = $(location).attr('hash');
    if (hash) {
        is_avaible = false;
        $(document.body).animate({
            'scrollTop':   $(hash).offset().top - bh - 30
        }, 2000, function() {
            is_avaible = true;
        });
    }
    
    $("div.sections").on('mousemove', '.section_item', function(e) {
        var id = $(this).attr('rel');
        $('div.sub_sections_list .sub_sections_items').each(function(index, item) {
            $(item).removeClass("active");
        });
        $("#sub-section-list-" + id).addClass("active");
    });
       
    $('body').on('click', '.ajax-action', function(e){
        var href = $(this).attr('href');
        var self = this;
        $.get(href, function(){
            var pjax_id = $(self).closest('.pjax-wraper').attr('id');
            $.pjax.reload('#' + pjax_id);
        });
        return false;
    });
    
    $('body').on('click', '.checkbox-action', function(e){
        var rel = $(this).attr('rel');
        var value = ($(this).is(":checked"))?1:0;
        $.get("/user/change-option", {option:rel, value:value}, function(){

        }, "json");
    })
    
    $("body form.form-action").submit(function () {
        var form = $(this);
        // return false if form still have some validation errors
        if (form.find('.has-error').length) {
            return false;
        }
        if (!(method = form.attr('method'))) 
            method = "post";
        
        var formdata = new FormData();
        var other_data = form.serializeArray();
        //console.log(other_data)
        $.each(other_data, function (key, input) {
            //console.log(input);
            formdata.append(input.name, input.value);
        });
        
        $.each(form.find('input[type="file"]'), function(i, input) {
            //console.log(input)
            if (input.files && input.files != null && input.files.length > 0) {
                if (input.files.length == 1) {
                    formdata.append(input.name, input.files[0]); // or add the name
                } else {
                    $.each(input.files, function(i, file) {
                        formdata.append(input.name + "[" + i + "]", file); // or add the name
                    });
                }
            }
        });
        //return false;
        
        //console.log(formdata.getAll())
        //return false;
        // submit form
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
            url: form.attr('action'),
            type: method,
            dataType: 'json',
            processData: false,
            contentType: false,
            data: formdata, //form.serialize(), //_csrf : csrfToken
            cache: false,
            success: function (data) {
               var func = form.attr('role');
               actions[func](form, data);
               return false;
            }
        });
        return false;
   });
   
    $(".form_forgot").slideUp();
    
    $(".form_forgot_a").click(function () {
        $(".form_forgot").slideDown();
        $(".form_auth").slideUp();
    });
    
    $(".form_login_a").click(function () {
        $(".form_auth").slideDown();
        $(".form_forgot").slideUp();
    });
   
    $("body form#loginform").submit(function () {
        var form = $(this);
        // return false if form still have some validation errors
        if (form.find('.has-error').length) {
            return false;
        }
        // submit form
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
             url: form.attr('action'),
             type: 'post',
             dataType: 'json',
             data: form.serialize(), //_csrf : csrfToken
             //cache: false,
             success: function (data) {
                if (data.result) {
                    if (data.selector)
                        closeModal(data.selector);
                    else window.location.reload();
                }
                else if (data.error) {
                    $(".auth_error").html(data.error);
                }
             }
        });
        return false;
   });
   
   $("body form#signupform").submit(function () {
        var form = $(this);
        // return false if form still have some validation errors
        if (form.find('.has-error').length) {
            return false;
        }
        // submit form
        $.ajax({
             url: form.attr('action'),
             type: 'post',
             dataType: 'json',
             data: form.serialize(),
             success: function (data) {
                if (data.result)
                    window.location.reload();
                else if (data.error) {
                    $(".reg_error").html(data.error);
                }
             }
        });
        return false;
   });
   
//   $("body form#email-form").submit(function () {
//        var form = $(this);
//        // return false if form still have some validation errors
//        if (form.find('.has-error').length) {
//            return false;
//        }
//        // submit form
//        var csrfToken = $('meta[name="csrf-token"]').attr("content");
//        $.ajax({
//             url: form.attr('action'),
//             type: 'post',
//             dataType: 'json',
//             data: form.serialize(), //_csrf : csrfToken
//             //cache: false,
//             success: function (data) {
//                if (data.result)
//                    window.location.reload();
//                else if (data.error) {
//                    $(".auth_error").html(data.error);
//                }
//             }
//        });
//        return false;
//   });

    //search
    $('.search .openclose').click(function () {
        var $p = $(this).closest('.search');
        var $speed = 500;
        if ($p.is('.open')) {
            $p.removeClass('open');
            $p.animate({
                width: 50
            }, $speed, function () {

            });
        } else {
            $p.addClass('open');
            $p.animate({
                width: '100%'
            }, $speed, function () {
                $('input[type=text]', $p).focus();

            });
        }
        return false;
    });


    $('.searchbox .opcl').click(function () {
        var $p = $(this).closest('.searchbox');
        if ($p.is('.searchbox_open')) {
            $p.animate({width: '50px'}).removeClass('searchbox_open');
        } else {
            $p.animate({width: '100%'}).addClass('searchbox_open');
            $('input[type=text]', $p).focus();
        }
        return false;
    });
    $('.searchbox .cl').click(function () {
        var $p = $(this).closest('.searchbox');
        if ($p.is('.searchbox_open')) {
            $p.animate({width: '50px'}).removeClass('searchbox_open');
        } else {
            $p.animate({width: '100%'}).addClass('searchbox_open');
            $('input[type=text]', $p).focus();
        }
        return false;
    });
    //


    $('.loginbox .opcl').click(function () {
        var $p = $(this).closest('li');
        if (!$p.is('.open')) {
            $('.loginbox .open').removeClass('open');
            $p.addClass('open');
        } else {
            $p.removeClass('open');
        }

    });

});

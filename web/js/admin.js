/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        console.log(numFiles);
        console.log(label);
        
        if (numFiles > 1) label = numFiles + " file(s) selected";
        $(".file-input .form-control").val(label);
    });
});

$("document").ready(function(){ 
    $(".combobox" ).click(function() {
       $(this).autocomplete( "search", "" );
    });
});

$(function(){
    $('body').on('click', '.grid-action', function(e){
        var href = $(this).attr('href');
        var self = this;
        $.get(href, function(){
            var pjax_id = $(self).closest('.pjax-wraper').attr('id');
            $.pjax.reload('#' + pjax_id);
        });
        return false;
    })
});

function deleteSelectedInGrid(id, container, messages) {
    var keys = $(id).yiiGridView('getSelectedRows');
    if (typeof(messages.confirm) == "undefined")
        messages.confirm = 'Are you sure you want to delete this items?';
    if (typeof(messages.cancel) == "undefined")
        messages.cancel = 'Please select at least one element!';
    if (keys.length > 0) {
        bootbox.confirm(messages.confirm, function(status) {
            if (status) {
                $.post("deleteselected", {items:keys}, function(result) {
                    if (typeof(result.error) !== "undefined") {
                        bootbox.alert(result.error);
                    }
                    else $.pjax.reload({container:container});
                }, "json");
            }
        });
//                
    }
    else bootbox.alert(messages.cancel);
}

function setActiveSlide(element, type, id_material, id_image) {
    $.get("set-active-slide", {type:type, id_material:id_material, id_image:id_image}, function(data) {
        if (data.result) {
            var slider = $(element).parent().parent().parent();
            $(slider).find("img").each(function(indx, image){
                if ($(image).attr("rel") == id_image)
                    $(image).addClass("active");
                else $(image).removeClass("active");
            });
        } else bootbox.alert("Сервер вернул ошибку");
    }, "json")
    
}

function deleteSlide(element, id_image, message) {
    if (typeof(message) == "undefined")
        message = 'Are you sure you want to delete this items?';

    bootbox.confirm(message, function(status) {
        if (status) {
            $.get("delete-slide", {id:id_image}, function(data) {
                if (data.result) {
                    var slider = $(element).parent().parent();
                    $(slider).remove();
                } else bootbox.alert("Сервер вернул ошибку");
            }, "json");
        }
    });
}


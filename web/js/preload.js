var onImgLoad = function(selector, callback){
    $(selector).each(function(){
        $(this).attr('src', $(this).attr('src')); // Для корректной работы закешированых изображений
        if (this.complete) {
            callback.apply(this);
        }
        else {
            $(this).load(function(){
                callback.apply(this);
            }).error(imgNotFound);
        }
    });
};

var imgNotFound = function() {
    $(this).unbind("error").attr("src", "/css/images/without-photo.jpg");
};

function onImageLoadAction() {
    $(this).parent().find(".loading").hide();
    $(this).parent().find(".icon_photo").removeClass('hide');
    $(this).hide().fadeIn(700);
}

$(function() {
    onImgLoad('.preload img', onImageLoadAction);
});


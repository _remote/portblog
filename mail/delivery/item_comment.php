<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Mail;
use app\components\Settings;

$this->params['body'] = Settings::getInstance()->getMailTemplate(Mail::MAIL_DELIVERY);

$text = Settings::getInstance()->getMailOfType("material_delivery_item");
$url = Url::to(["admin/".$material['type']."/comments/view", "id" => $material['id']]);
$text = str_replace("{LINK}", $url, $text);
$text = str_replace("{INDEX}", ($index+1), $text);
$text = str_replace("{COMMENT}", $material['text'], $text);

echo $text;
?>


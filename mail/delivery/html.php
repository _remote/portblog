<?php
if ($params['materials']) {
    foreach ($params['materials'] as $material) {
        echo $this->render("item", [
            "material" => $material,
        ]);
    }
}


<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Mail;
use app\components\Settings;

$this->params['body'] = Settings::getInstance()->getMailTemplate(Mail::MAIL_DELIVERY);

$text = Settings::getInstance()->getMailOfType("material_delivery_item");
$url = Url::to(["/material/{$material['type']}/{$material['slug']}"]);
$text = str_replace("{LINK}", $url, $text);
$text = str_replace("{TITLE}", $material['title'], $text);
$text = str_replace("{IMAGE}", $material['image'], $text);
$text = str_replace("{TEXT}", $material['text'], $text);

echo $text;
?>



<?php
if ($params['materials']) {
    $count = count($params['materials']);
?>
<h2>Всего новых комментариев за период <?= " (".date("G:i", (time() -$params['period']))." - ".date("G:i", time()).")" ?>: <?= $count ?></h2>
<?php
    foreach ($params['materials'] as $index => $material) {
        echo $this->render("item_comment", [
            "index" => $index,
            "material" => $material,
            "count" => $count,
        ]);
    }
}


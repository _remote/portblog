<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Mail;
use app\components\Settings;

$this->params['body'] = Settings::getInstance()->getMailTemplate(Mail::MAIL_FOR_USER);
//Загрузка шаблона из базы
$text = Settings::getInstance()->getMailOfType("user_registration");
//Подстановка параметров в шаблон
$text = str_replace("{USERNAME}", $params['first_name'], $text);
$text = str_replace("{LOGIN}", $params['login'], $text);

echo $text;
?>

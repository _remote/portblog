<?php
use app\models\Mail;
use app\components\Settings;

$this->params['body'] = Settings::getInstance()->getMailTemplate(Mail::MAIL_INFO);

$text = Settings::getInstance()->getMailOfType("test");
$text = str_replace("{FILE}", __FILE__, $text);

echo $text;
?>
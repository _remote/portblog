<?php

namespace app\widgets\googlemap;

use yii\web\AssetBundle;

class GoogleMapAssets extends AssetBundle {

    public $css = [];
    public $js = [
        'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&sensor=true',
    ];
    public $depends = [];

}

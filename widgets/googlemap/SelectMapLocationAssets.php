<?php

namespace app\widgets\googlemap;

use yii\web\AssetBundle;

class SelectMapLocationAssets extends AssetBundle {

    public $sourcePath = '@app/widgets/googlemap';
    public $css = [];
    
    public $js = [
        'js/select-google-map-location.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'app\widgets\googlemap\GoogleMapAssets',
    ];

}

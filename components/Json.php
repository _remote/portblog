<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\Application;
use \yii\helpers\ArrayHelper;
use app\models\User;
use yii\helpers\HtmlPurifier;

class Json extends Component {

    public $value = '{}';
    public $dir = "cache";
    public $simple = false; //Простой ответ
    
    protected $path = false; //Папка, в которую будет сохраняться кеш
    protected $filename = false;
    protected $page = 1;
    protected $limit = 10;
    protected $sort = "id";
    protected $sort_direction = "ASC";
    protected $type = "none";
    protected $language = "ru";
    public $field_id = false; //По какому полю будет считаться поиск по умолчанию
    
    public function setLanguage($language = "ru") {
        $this->language = $language;
    }
    
    protected function clearString($array = []) {
        foreach ($array as $key => $item) {
            if (is_array($item)) 
                $array[$key] = $this->clearString($item);
            else if (is_string($item)) {
                //$value = str_replace("<p>", '', $item);
                //$value = str_replace("</p>", "\n", $value);
                
                $value = str_replace('"', "'", $item);
                $value = str_replace('&quot;', "'", $value);
                //$value = str_replace('"', "&quot;", $item);
                $array[$key] = htmlspecialchars($item);
            }
            //else $array[$key] = (string)$item; 
        }
        return $array;
    }

    public function build($array, $errors = false, $only_items = false) {
        if (is_array($errors)) {
            $result = [
                "success" => "0",
                "errors" => $errors
            ];
        }
        else if (is_array($array)) {
            $result = [];
            //if (!$only_items)
            //    $result["success"] = "1";
            $array = $this->clearString($array);
            foreach ($array as $key => $value)
                $result[$key] = $value;
        }
        else return "";
        return (json_encode($result));
    }
    
    public function saveToFile($array, $path = false, $filename = false, $ext = "json") {
        if ($path) 
            $this->path = $path;
        if ($filename)
            $this->filename = $filename.".".$ext;
        $json = $this->build($array, false, true);
        $this->save($json);
    }
    
    public function permission() {
        return $this->build([], ["status" => "You don`t have permission!"]);
    }
    
    public function success($model = false, $params = false) {
        $result = [];
        if (isset($model->id)) {
            $result['id'] = $model->id;
        }
        if (is_array($params)) {
            foreach ($params as $key => $value)
                $result[$key] = $value;
        }
        return $this->build($result);
    }
    
    protected function save($data = "") {
        $path = $this->getPath($this->path);
        
        $this->checkPath($path);
        $filename = $this->getPathToFile();
        if ($fp = fopen($filename, "a")) { // Открываем файл в режиме записи 
            if ($test = fwrite($fp, /*htmlspecialchars_decode*/($data))) { // Запись в файл
                fclose($fp); 
                //@chmod($filename, 0664);
                return true;
            }
        }
    }
    
    public function getFile($path = false, $filename = false, $ext = "json") {
        if ($path) $this->path = $path;
        if ($filename) $this->filename = $filename.".".$ext;
        
        $filename = $this->getPathToFile();
        if (file_exists($filename)) {
            //echo "Берем из файла";
            $json = file_get_contents($filename);
            return ($json)?($json):"";
        }
        else return false;
    }
    
    protected function getPath($id = false) {
        return Yii::$app->basePath."/".$this->dir."/".HtmlPurifier::process($id);
    }
    
    protected function getPathToFile() {
        $filename = (!$this->filename)?($this->limit?$this->language."_".$this->limit."_".$this->page:$this->language).".json":$this->filename;
        return $this->getPath($this->path)."/".$filename;
    }
    
    protected function checkFile() {
        $file = $this->getPathToFile();
        return (file_exists($file))?true:false;
    }
    
    protected function checkPath($path) {
        if (!file_exists($path)) {
            if (@mkdir ($path, 0777, true)) {
                if ($fp = fopen($path."/.htaccess", "a")) { // Открываем файл в режиме записи 
                    $test = fwrite($fp, "deny from all"); // Запись в файл
                    fclose($fp); //Закрытие файла
                    return true;
                }
            }
        }
    }
    
    public function deleteFolder($folder) {
        if (is_dir($folder)) {
            $handle = opendir($folder);
            while ($subfile = readdir($handle)) {
                if ($subfile == '.' or $subfile == '..') continue;
                if (is_file($subfile)) unlink("{$folder}/{$subfile}");
                else $this->deleteFolder("{$folder}/{$subfile}");
            }
            closedir($handle);
            rmdir($folder);
        } else
            unlink($folder);
    }
    
    public function search($model = false, $condition = [], $p = []) {
        if (!$model) return "";
        if (isset($p['path'])) $this->path = $p['path'];
        if (isset($p['type'])) $this->type = $p['type'];
        $this->limit = isset($_GET['limit'])?intval($_GET['limit']):false;
        $this->page = isset($_GET['page'])?intval($_GET['page']):1;
        if ($sort = (isset($_GET['sort']))?$_GET['sort']:false) {
            if (preg_match("/^([a-z_]+)-(asc|desc)$/i", $sort, $out)) {
                $this->sort = $out[1];
                $this->sort_direction = $out[2];
                //Yii::$app->debug->show($out);
            }
        }
        
        if (isset($p['language'])) $this->language = $p['language'];
        $conformity = isset($p['conformity'])?$p['conformity']:[];
        
        $params = $model->prepareForSearch(Yii::$app->request->get(), $conformity);
        $isSimple = $model->checkSimpleQuery($params, $this->field_id);
        //Если запрос не простой, то нужно быть авторизированным
        //if (!$isSimple && Yii::$app->user->isGuest) 
        //    return $this->permission();
        $default_sort = ($this->sort == "id" && strtoupper($this->sort_direction) == "ASC")?true:false;
        //Если это простой запрос, и есть такой файл на диске, то берем данные с него
        if ($this->path && $isSimple && $default_sort && $this->checkFile()) {
            return $this->getFile();
        }
        else { //Создаем файл, при его отсутсвии
            $select = isset($p['select'])?$p['select']:"*";
            $fields = $model->selectedFields($select);
            $search = $model->find()->select($fields)->andWhere($condition)->andWhere($params); 
            $items = $search->offset(($this->page-1)*$this->limit)->limit($this->limit)->orderBy("`{$this->sort}` {$this->sort_direction}")->all();
            $result = [];
            if (!count($items)) return; //Если по критериям поиска не найдено ничего, то ничего делать не надо.
            foreach ($items as $item) {
                $result[] = $item->fieldsValue($this->language, $select); //Вытаскиваем данные, относительно языка
            }
            if (!$this->simple) {
                $totalCount = $search->count();
                $json = $this->build([
                    "totalCount" => $totalCount,
                    "currentPage" => $this->page,
                    "countOnPage" => ($this->limit)?$this->limit:$totalCount,
                    "pageCount" => ($this->limit)?ceil($totalCount / $this->limit):1,
                    "language" => $this->language,
                    "dateUpdate" => date("Ymd", time()),
                    "timeUpdate" => date("G:i", time()),
                    "params" => $params,
                    "items" => $result,
                    "type" => $this->type,
                    "sort" => "{$this->sort} {$this->sort_direction}",
                ]);
            }
            else $json = $this->build($result, false, true);
            if ($isSimple && $default_sort) { //Если простой запрос, то сохраняем его в кеш
                //echo "сохраняем";
                $this->save($json);
            }
            return $json;
        }
    }
    
    protected function getIdValue($model = false) {
        $field = $this->field_id;
        return isset($model->$field)?$model->$field:false;
    }
    
    public function clearCache($path = false) {
        $path_to_cache = $this->getPath($path);
        if (file_exists($path_to_cache)) {
            $this->deleteFolder ($path_to_cache);
            return true;
        }
        return false;
    }
    
    public function add($model = false) {
        if (Yii::$app->user->isGuest) 
            return $this->permission();
        
        if ($model->prepare(Yii::$app->request->get()) && $model->validate()) {
            if (!User::isAdmin() && $model->ownerId != Yii::$app->user->id) 
                return $this->build([], ["status" => "You can add places only to themselves"]);
            if ($model->save()) {
                $status = $this->clearCache($this->getIdValue($model));
                return $this->success($model, ["clear_cache" => $status]);
            }
            else return $this->build([], ["status" => "Database error"]);
        }
        else return $this->build([], $model->getErrors());
    }
    
    public function update($model = false, $id = false) {
        if (Yii::$app->user->isGuest) 
            return $this->permission();
        if (!$id)
            return $this->build([], ["status" => "You must specify id"]);
        if (!$model)
           return $this->build([], ["status" => "Object not found!"]);
        else $model = $model->findOne(["id" => $id]);
        
        if ($model->prepare(Yii::$app->request->get()) && $model->validate()) {
            if (!User::isAdmin() && $model->ownerId != Yii::$app->user->id) 
                return $this->build([], ["status" => "You can update places only to themselves"]);
            if ($model->update()) {
                $status = $this->clearCache($this->getIdValue($model));
                return $this->success($model, ["clear_cache" => $status]);
            }
            else return $this->success($model, ["status" => "It was not affected any record"]);
        }
        else return $this->build([], $model->getErrors());
    }
    
    public function delete($model = false, $id = false) {
        if (Yii::$app->user->isGuest) 
            return $this->permission();       
        if (!$id)
            return $this->build([], ["status" => "You must specify id"]);
        $record = $model->findOne(["id" => $id]);
        if ($model->deleteAll(["id" => $id])) {
            $status = $this->clearCache($this->getIdValue($record));
            return $this->success($model, ["clear_cache" => $status]);
        }
        else return $this->build([], ["status" => "Object not found!"]);
    }
    
}
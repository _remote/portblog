<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\components\Settings;

class RecountController extends Controller {
    
    public function actionComments() {
        ini_set("memory_limit","512M");
        $materials = ["articles", "blogs", "events", "company", "news", "persons", "places"];
        foreach ($materials as $material) {
            Settings::getInstance()->setModuleId($material);
            $class = Settings::getInstance()->getModulePath($material)."\models\Material";
            $class_comment = Settings::getInstance()->getModulePath($material)."\models\Comment";
            if ($items = $class::find()->where("id IS NOT NULL")->all()) {
                foreach ($items as $index => $item) {
                    $count = $class_comment::find()->where(["id_material" => $item->id])->count();
                    $item->scenario = "update";
                    $item->comments_count = $count;
                    //echo $item->rating = (rand(1, 50) / 10.0);
                    //echo $item->rating_voting_count = rand(1, 100);
                    echo "{$material} {$item->id}";
                    if ($item->validate()) {
                        $item->save();
                    }
                    else echo "{$material} {$item->id} ({$count}) fail \n\r";
                }
            }
            
        }
    }
    
    public function actionSection() {
        ini_set("memory_limit","512M");
        $materials = ["articles", "blogs", "events", "company", "news", "persons", "places"];
        foreach ($materials as $material) {
            Settings::getInstance()->setModuleId($material);
            $class1 = Settings::getInstance()->getModulePath($material)."\models\Section";
            $class2= Settings::getInstance()->getModulePath($material)."\models\SubSection";
            if ($items = $class1::find()->where("id IS NOT NULL")->all()) {
                foreach ($items as $index => $item) {
                    //$item->scenario = "update";
                    if ($item->validate()) {
                        $item->save();
                    }
                }
            }
            
            if ($items = $class2::find()->where("id IS NOT NULL")->all()) {
                foreach ($items as $index => $item) {
                    //$item->scenario = "update";
                    if ($item->validate()) {
                        $item->save();
                    }
                }
            }
            
        }
    }

}

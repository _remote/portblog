<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\components\Settings;
use app\models\User;
use app\models\Mail;

class CronController extends Controller {

    private function loadMaterial(&$materials, $type) {
        Settings::getInstance()->setModuleId($type);
        $today = date("Y-m-d", time());
        $class = '\\app\\modules\\' . Settings::getInstance()->getModuleId() . '\\models\\Material';
        if ($material = $class::find()->where(["delivery_date" => $today, "is_show" => $class::STATUS_PUBLISHED])->all()) {
            foreach ($material as $item)
                $materials[] = [
                    "id" => $item->id,
                    "title" => strip_tags($item->title),
                    "text" => $item->short_text,
                    "image" => $item->getThumbUploadUrl('image_file', 'preview'),
                    "type" => $type,
                ];
        }
    }

    public function actionDelivery() {
        //Выбираем всех пользователей, которые есть в системе
        $today = date("Y-m-d", time());
        //if (time() - strtotime($today) >= 3600)
        $materials = [];
        $this->loadMaterialComments($materials, "articles", $period);
        $this->loadMaterialComments($materials, "blogs", $period);
        $this->loadMaterialComments($materials, "company", $period);
        $this->loadMaterialComments($materials, "events", $period);
        $this->loadMaterialComments($materials, "news", $period);
        $this->loadMaterialComments($materials, "persons", $period);
        $this->loadMaterialComments($materials, "places", $period);
        //return Mail::send(Mail::MAIL_DELIVERY, "mindnighte@gmail.com", "Рассылка", "delivery/html", ["materials" => $materials]);

        if (!empty($materials)) {
            if ($users = User::find()->where([/* "active" => 1 */])->all()) {
                foreach ($users as $user) {
                    $result = Mail::send(Mail::MAIL_DELIVERY, $user->email, "Рассылка", "delivery/html", ["materials" => $materials]);
                    echo $user->email . ": " . $result . "\r\n";
                }
            }
        }
    }

    private function loadMaterialComments(&$materials, $type, $period = 3600) {
        Settings::getInstance()->setModuleId($type);
        $_date = date("Y-m-d G:i:s", time());
        $_date_last = date("Y-m-d G:i:s", (time() - $period));
        $class = '\\app\\modules\\' . Settings::getInstance()->getModuleId() . '\\models\\Comment';
        if ($material = $class::find()->where(["and", ["<", "date_created", $_date], [">=", "date_created", $_date_last]])->all()) {
            foreach ($material as $item)
                $materials[] = [
                    "id" => $item->id,
                    "id_material" => $item->id_material,
                    "text" => $item->text,
                    "date" => $item->date_created,
                    "type" => $type,
                ];
        }
    }

    public function actionDeliveryComments() {
        $materials = [];
        $period = 3600;
        $this->loadMaterialComments($materials, "articles", $period);
        $this->loadMaterialComments($materials, "blogs", $period);
        //$this->loadMaterialComments($materials, "company", $period);
        $this->loadMaterialComments($materials, "events", $period);
        $this->loadMaterialComments($materials, "news", $period);
        //$this->loadMaterialComments($materials, "persons", $period);
        //$this->loadMaterialComments($materials, "places", $period);
        
        //Yii::$app->debug->show($materials);
        //return Mail::send(Mail::MAIL_DELIVERY, "mindnighte@gmail.com", "Новые сообщения", "delivery/comment", ["materials" => $materials, "period" => $period]);
        //Выбираем всех пользователей, которые есть в системе
        if (!empty($materials)) {
            if ($users_id = \app\modules\permit\models\AuthAssignment::getUsersByRoles([User::ROLE_ADMINISTRATOR, User::ROLE_MODERATOR])) {
                foreach ($users_id as $id) {
                    $user = User::findIdentity($id);
                    $result = Mail::send(Mail::MAIL_INFO, $user->email, "Новые сообщения", "delivery/comment", ["materials" => $materials, "period" => $period]);
                    echo $user->email . ": " . $result . "\n\r";
                }
            }
        }
    }

}

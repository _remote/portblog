<?php
Yii::setAlias('@app', dirname(__DIR__));
Yii::setAlias('@tests', dirname(__DIR__) . '/tests');
Yii::setAlias('@webroot', dirname(__DIR__).'/web');
Yii::setAlias('@web', 'http://api.portrussia.ru/');

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'app\commands',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'authManager' => [
          'class' => 'yii\rbac\DbManager',
        ],
        'urlManager' => [
            'baseUrl' => 'http://api.portrussia.ru/',
            'hostInfo' => 'http://api.portrussia.ru/',
            'scriptUrl' => '',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'sphinx' => [
            'class' => 'yii\sphinx\Connection',
            'dsn' => 'mysql:host=127.0.0.1;port=9306;',
            'username' => '',
            'password' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'debug' => [
            'class' => 'app\components\Debug',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    //'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    'class' => 'app\components\DbMessageSource',
                    'sourceLanguage' => 'en',
                    'on missingTranslation' => ['app\components\DbMessageSource', 'handleMissingTranslation'],
                    
                ],
            ]
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
];

<?php
return [
    'admin/panel' => [
        [
            'actions' => ['index', 'error', 'captcha', 'login', 'logout', 'signup'],
            'allow' => true,
        ],
    ],
    'files/default' => [
        [
            'actions' => ['form', 'upload'],
            'allow' => true,
        ],
    ],
    'site' => [
        [
            'actions' => ['callback', 'index', 'view', 'auth', 'signup', 'logout', 'search', 'blogs', 'auth-social', 'views-increment', 'change-rating', 'load-more', 'rating', 'user', 'user-view', 'change-language'],
            'allow' => true,
        ],
    ],
    'info' => [
        [
            'actions' => ['index', ],
            'allow' => true,
        ],
    ],
    'instagram/default' => [
        [
            'actions' => ['index', 'success', 'get-token', 'search', 'auth'],
            'allow' => true,
        ],
    ],
    'admin/cities' => [
        [
            'actions' => ['countrylist'],
            'allow' => true,
        ],
    ],
    'user' => [
        [
            //'actions' => ['index', '*'],
            'allow' => true,
        ],
    ],
    
    'localisation' => [
        [
            'actions' => ['index', 'search'],
            'allow' => true,
        ],
    ],
    'countries' => [
        [
            'actions' => ['index', 'search'],
            'allow' => true,
        ],
    ],
    
    'admin/news/default' => [
        [
            'actions' => ['sublist', 'sectionlist', 'citieslist', 'upload', 'comments/list', 'delivery'],
            'allow' => true,
        ],
    ],
    
    'news/default' => [
        [
            'actions' => ['search', 'today', 'view', 'delete', 'rss', 'menu'],
            'allow' => true,
        ],
    ],
    
    'news/comments' => [
        [
            'actions' => ['index', 'set'],
            'allow' => true,
        ],
    ],
    'blogs/default' => [
        [
            'actions' => ['search', 'view', 'delete', 'tags'],
            'allow' => true,
        ],
    ],
    
    'events/admin/default' => [
        [
            'actions' => ['sublist', 'sectionlist', 'citieslist', 'commentlist', 'delivery'],
            'allow' => true,
        ],
    ],
    'articles/admin/default' => [
        [
            'actions' => ['sublist', 'sectionlist', 'citieslist', 'commentlist', 'delivery'],
            'allow' => true,
        ],
    ],
    'blog/admin/posts' => [
        [
            'actions' => ['blogslist'],
            'allow' => true,
        ],
    ],
    
    'blogs/posts' => [
        [
            'actions' => ['search', 'view', 'delete'],
            'allow' => true,
        ],
    ],
    'cabinet' => [
        [
            'actions' => ['index', 'search'],
            'allow' => true,
        ],
    ],
    
    'events/default' => [
        [
            'actions' => ['search', 'today', 'view', 'menu', 'rss'],
            'allow' => true,
        ],
    ],
    'events/comments' => [
        [
            'actions' => ['set'],
            'allow' => true,
        ],
    ],
    'articles/default' => [
        [
            'actions' => ['search', 'today', 'view', 'menu', 'rss'],
            'allow' => true,
        ],
    ],
    'articles/comments' => [
        [
            'actions' => ['set'],
            'allow' => true,
        ],
    ],
    
    
    'admin/company/default' => [
        [
            'actions' => ['sublist', 'sectionlist', 'citieslist', 'upload', 'comments/list', 'delivery'],
            'allow' => true,
        ],
    ],
    'company/default' => [
        [
            'actions' => ['search', 'today', 'view', 'delete', 'rss', 'menu'],
            'allow' => true,
        ],
    ],
    'company/comments' => [
        [
            'actions' => ['index', 'set'],
            'allow' => true,
        ],
    ],
    
    'admin/places/default' => [
        [
            'actions' => ['sublist', 'sectionlist', 'citieslist', 'upload', 'comments/list'],
            'allow' => true,
        ],
    ],
    'places/default' => [
        [
            'actions' => ['search', 'today', 'view', 'delete', 'rss', 'menu'],
            'allow' => true,
        ],
    ],
    'places/comments' => [
        [
            'actions' => ['index', 'set'],
            'allow' => true,
        ],
    ],
    
    'admin/persons/default' => [
        [
            'actions' => ['sublist', 'sectionlist', 'citieslist', 'upload', 'comments/list'],
            'allow' => true,
        ],
    ],
    'persons/default' => [
        [
            'actions' => ['search', 'today', 'view', 'delete', 'rss', 'menu'],
            'allow' => true,
        ],
    ],
    'persons/comments' => [
        [
            'actions' => ['index', 'set'],
            'allow' => true,
        ],
    ],
    
    'admin/test' => [
        [
            'actions' => ['search'],
            'allow' => true,
        ],
    ],
];


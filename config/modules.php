<?php
return [
    'gii' => 'yii\gii\Module',
    'permit' => [
        'class' => 'app\modules\permit\Permit',
        'params' => [
            'userClass' => 'app\models\User',
            'rolesFromUser' => true,
        ],
    ],    
    'blogs' => [
        'class' => 'app\modules\blogs\Blogs',
    ],
    'files' => [
        'class' => 'app\modules\files\Files',
    ],
    'settings' => [
        'class' => 'app\modules\settings\Settings',
    ],
    'news' => [
        'class' => 'app\modules\news\News',
    ],
    'events' => [
        'class' => 'app\modules\events\Events',
    ],
    'articles' => [
        'class' => 'app\modules\articles\Articles',
    ],
    'persons' => [
        'class' => 'app\modules\persons\Persons',
    ],
    'company' => [
        'class' => 'app\modules\company\Company',
    ],
    'places' => [
        'class' => 'app\modules\places\Places',
    ],
    'instagram' => [
        'class' => 'app\modules\instagram\Instagram',
    ],
];


<?php
return [
    /**
     * Клиенткая часть 
     */
    '/' => 'site/index',
    'material/views-increment' => 'site/views-increment', //Увеличение колличества просмотров материала
    'material/change-rating' => 'site/change-rating', //Изменение данных по рейтингу
    'material/load-more' => 'site/load-more', //Подгрузить пакет по тематике материала
    
    /* Старый синтаксис путей */
    'material/<type:[\w-]+>' => 'site/index',
    'material/<type:[\w-]+>/<path:[\w-]+>' => 'site/index',
    'material/<type:[\w-]+>/section/<section:[\w-]+>' => 'site/index',
    'material/<type:[\w-]+>/section/<section:[\w-]+>/<subsection:[\w-]+>' => 'site/index',
    /* Конец старого синтаксиса */
    
    /* Статические страницы */
    'info/<path:[\w-]+>' => 'info/index',
    '<language:(ru|en|de)>/info/<path:[\w-]+>' => 'info/index',
    
    /* Навигация по материалу */
    '<language:(ru|en|de)>' => 'site/index',
    '<language:(ru|en|de)>/<type:[\w-]*>' => 'site/index',
    '<language:(ru|en|de)>/<type:[\w-]*>/<city:[\w-]*>' => 'site/index',
    '<language:(ru|en|de)>/<type:[\w-]*>/<city:[\w-]*>/<section:[\w-]*>' => 'site/index',
    '<language:(ru|en|de)>/<type:[\w-]*>/<city:[\w-]*>/<section:[\w-]*>/<subsection:[\w-]*>' => 'site/index',
    '<language:(ru|en|de)>/<type:[\w-]*>/<city:[\w-]*>/<section:[\w-]*>/<subsection:[\w-]*>/<path:[\w-]+>' => 'site/index',
    
    
    
    'search/<material:[\w\s]+>/<tag:[\w-]+>' => 'site/search',
    
    /* Личный кабинет */
    'user' => 'user/index',
    'user/<action:[\w-]+>' => 'user/<action>',

    /**
     * Админская часть
     */
    'admin/'=>'admin/panel/index',
    'admin/<action:\w+>'=>'admin/panel/<action>',
    'admin/settings/<action:[\w-]+>'=>'settings/admin/default/<action>',
    'admin/settings/<controller:[\w-]+>/<action:[\w-]+>'=>'settings/admin/<controller>/<action>',
    'settings/<action:[\w-]+>'=>'settings/default/<action>',
    
    'admin/icons/<action:[\w-]+>'=>'files/admin/icons/<action>',
    //'admin/files/<controller:[\w-]+>/<action:[\w-]+>'=>'files/admin/<controller>/<action>',

    'admin/<module:(news|events|articles|company|persons|places|blogs)>/<action:[\w-]+>'=>'<module>/admin/default/<action>',
    'admin/<module:(news|events|articles|company|persons|places|blogs)>/<controller:\w+>/<action:[\w-]+>'=>'<module>/admin/<controller>/<action>',
    '<module:(news|events|articles|company|persons|places|instagram|blogs)>/<action:[\w-]+>'=>'<module>/default/<action>',

    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
    '<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',
];


<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=@host@;dbname=@db@',
    'username' => '@user@',
    'password' => '@password@',
    'charset' => 'utf8',
    'tablePrefix' => ''
];

<?php
Yii::setAlias('@views', dirname(__DIR__) . '/views');

$params = require(__DIR__ . '/params.php');
$access = require(__DIR__ . '/access.php');
$modules = require(__DIR__ . '/modules.php');
$socials = require(__DIR__ . '/socials_loc.php');
$url_rules = require(__DIR__ . '/url_rules.php');

$config = [
    'id' => 'portblog',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'language' => "ru",
    'modules' => $modules,
    'as AccessBehavior' => [
        'class' => "app\modules\permit\behaviors\AccessBehavior",
        'rules' => $access
    ],
    'components' => [
        'authManager' => [
          'class' => 'yii\rbac\DbManager',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => $socials,
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            //'enableCookieValidation' => false,
            'cookieValidationKey' => 'OY9BzQTNqXUBiJuZ2LG_3onHLkaknkUk',
            //'baseUrl' => '/portblog',
//            'csrfParam' => '_backendCSRF',
//            'csrfCookie' => [
//                'httpOnly' => true,
//                'path' => '/admin',
//            ],
        ],
//        'session' => [
//            'cookieParams' => [
//                'path' => '/',
//                'httponly' => false,
//            ],
//        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['admin/panel/login'],
//            'identityCookie' => [
//                'name' => '_backendIdentity',
//                'path' => '/admin',
//                'httpOnly' => true,
//            ],
        ],
        'urlManager' => [
            //'baseUrl' => 'http://portrussia.ru/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => $url_rules,
        ],
        'assetManager' => [
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'bundles' => [
                'dosamigos\google\maps\MapAsset' => [
//                    'options' => [
//                        'key' => '132142067559',
//                        'language' => 'id',
//                        'version' => '3.1.18'
//                    ]
                ]
            ]
        ],
        'json' => [
            'class' => 'app\components\Json',
        ],
        'debug' => [
            'class' => 'app\components\Debug',
        ],
        'upload' => [
            'class' => 'app\modules\files\components\Prepare',
        ],
//        'instagram' => [
//            'class' => 'app\components\instagram\Instagram',
//            'apiKey' => '5764bc4b02c741c3a8dab930b078a78c',
//            'apiSecret' => '9d4dd6a409ba429f81ab3d0ff3c00643',
//            'apiCallback' => 'http://portrussia.ru',
//        ],
        
        'errorHandler' => [
            //'class' => '\yii\web\ErrorHandler',
            'errorAction' => 'site/error',
        ],
        'sphinx' => [
            'class' => 'yii\sphinx\Connection',
            'dsn' => 'mysql:host=@sphinx_host@;port=@sphinx_port@;',
            'username' => '',
            'password' => '',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'viewPath' => '@app/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
//                'host' => 'smtp.yandex.ru',
//                'username' => 'info.portblog',
//                'password' => 'GhbdtnDctv',
//                'port' => '587',
//                'encryption' => 'tls',
            ],
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    //'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    'class' => 'app\components\DbMessageSource',
                    'sourceLanguage' => 'en',
                    'on missingTranslation' => ['app\components\DbMessageSource', 'handleMissingTranslation'],
                    
                ],
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;

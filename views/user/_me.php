<?php
use Yii;
use kartik\tabs\TabsX;

$this->params['breadcrumbs'][] = Yii::t("client", "Cabinet");

$items = [
    [
        'id' => 'settings',
        'label' => Yii::t("client", "Settings"),
        'content' => $this->render("panels/settings", [
            "user" => $user,
            "socials" => $socials,
        ]),
        
    ],
    [
        'id' => 'posts',
        'label' => Yii::t("client", "My posts"),
        'content' => $this->render("panels/posts", [
            "user" => $user,
            "posts" => $posts,
        ]),
        'active' => ($tab == "posts")
    ],
    [
        'id' => 'subscription',
        'label' => Yii::t("client", "My subscriptions"),
        'content' => $this->render("panels/subscriptions", [
            "user" => $user,
        ]),
    ],
    [
        'id' => 'events',
        'label' => Yii::t("client", "Events"),
        'content' => $this->render("panels/events", [
            "user" => $user,
        ]),
    ],

    [
        'id' => 'blog-create',
        'label' => Yii::t("client", "Create post"),
        'headerOptions' => [
            'id' => 'blog-create',
            'class' => "bar"
        ],
        'content' => $this->render("panels/post_form", [
            "user" => $user,
            "model" => $model,
        ]),
        'active' => ($tab == "create_post")
    ],

];

echo TabsX::widget([
    'items' => $items,
    'position'=>TabsX::POS_ABOVE,
    'encodeLabels'=>false,
]);
?>


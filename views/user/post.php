<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;

use app\components\Site;

$this->params['breadcrumbs'][] = ['label' => Yii::t("client", "Cabinet"), 'url' => ["/user"]];
$this->params['breadcrumbs'][] = Yii::t("client", "Update post");
?>  
<div class="row">
    <div class="material_content col-md-9">
        <h2><?= Yii::t("client", "Update post"); ?></h2>
        <?php
        echo $this->render("panels/post_form", [
            //"user" => $user,
            "model" => $model,
            "edit" => true,
        ]);
        ?>
    </div>
    <div class="col-md-3">
        <?php //$this->render("@views/site/templ/rblock", ["user" => $user]) ?>
    </div>
</div>



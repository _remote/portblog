<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
$this->params['breadcrumbs'][] = Yii::t("client", "Password reset");

if ($success) {
?>
<script type="text/javascript">
reloadPage("/user", 3000);
</script>
<div class="alert alert-success" role="alert">
    <strong>Успех!</strong> Ваш пароль восстановлен.
</div>
<?php
return true;
}
?>

<div class="user-form">

    <h1>Восстановление пароля</h1>
    <?php 
    $form = ActiveForm::begin();
    ?>

    <?= $form->field($model, 'password')->passwordInput(["value" => false]) ?>


    <div class="form-group">
        <?= Html::submitButton("Изменить", ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


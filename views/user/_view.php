<?php
use app\models\User;

$this->params['breadcrumbs'][] = ['label' => Yii::t("client", "Cabinet"), 'url' => ["/user"]];
$this->params['breadcrumbs'][] = Yii::t("client", "View user: {user}", ["user" => User::getNameOfUser($user->id)]);
?>

<h2><?= Yii::t("client", "User posts") ?></h2>
<?php
echo $this->render("panels/posts", [
    "user" => $user,
    "posts" => $posts,
]);
?>


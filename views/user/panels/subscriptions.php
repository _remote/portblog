<?php
use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\bootstrap\Html;

use app\models\UserSubscriptions;

$subscriptions = new UserSubscriptions;


//Список сообщений
Pjax::begin(['id' => '_list_subscription', 'options' => ['class' => 'pjax-wraper']]);
echo ListView::widget([
    'dataProvider' => $subscriptions->search(null, Yii::$app->user->id),
    'itemView' => '_item_subscription',
    'viewParams' => [
       
    ]
]);
Pjax::end();


<?php
use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\bootstrap\Html;

use app\models\UserEvents;

$events = new UserEvents;


//Список сообщений
Pjax::begin(['id' => '_list_events', 'options' => ['class' => 'pjax-wraper']]);
echo ListView::widget([
    'dataProvider' => $events->search(null, Yii::$app->user->id),
    'itemOptions' => ['class' => false],
    'itemView' => function ($model, $key, $index, $widget) {
        return Html::encode(Yii::t('client', $model->message, @json_decode($model->object)));
    },
]);
Pjax::end();


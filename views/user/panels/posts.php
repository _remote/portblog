<?php
use yii\bootstrap\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

use app\models\User;
?>
<div class="posts">
    <?php
    if ($posts) {
        Pjax::begin(['id' => 'post-list', 'options' => ['class' => 'pjax-wraper']]);
        foreach ($posts as $key => $item) {
    ?>
            <div class="material blog col-xs-12 row">
                <h2><?= Html::a($item->title, ["/material/blogs/{$item->slug}"]) ?></h2>
                <?php
                if (User::isMe($item->id_user)) {
                    echo Html::tag("div", 
                        Html::a(Html::tag("span", null, ["class" => "glyphicon glyphicon-pencil"]), ["/user/update-post", "id" => $item->id], ["data-pjax" => 0]).
                        Html::a(Html::tag("span", null, ["class" => "glyphicon glyphicon-trash"]),  ["/user/delete-post", "id" => $item->id, "path" => "/user/index", "tab" => "posts"], [
                            'data' => [
                                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                                'pjax' => 0,
                            ],
                            'class' => 'ajax-action'
                        ]),

                        ["class" => "buttons"]
                    );
                }
                ?>
<!--                <div class="info blog">
                    <div class="user_name"><?= Html::a(User::getNameOfUser($item->id_user), null) ?></div>
                    <div class="date" title="<?= $item->date_created ?>" itemprop="dateCreated"><?= $item->formatDate() ?></div>
                    <div class="bar">
                        <div class="comments">
                            <a href="<?= "/material/blogs/{$item->slug}" ?>#comments">
                                <span class="glyphicon glyphicon-envelope"></span>
                                <div class="count" itemprop="commentCount"><?= $item->comments_count ?></div>
                            </a>
                        </div>
                        <div class="views"><span class="glyphicon glyphicon-eye-open"></span><div class="count"><?= $item->view_count ?></div></div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-xs-12" itemprop="description">
                        <?=
                        $item->short_text."...";
                        ?>
                    </div>
                </div>
            </div>
    <?php
        }
        Pjax::end();
    }
    ?>
</div>


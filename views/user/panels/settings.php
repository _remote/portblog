<?php
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

use app\models\User;
?>
<div class="settings">
    <h3>Аккаунт</h3>
    <div role="alert"></div>
    <table>
        <tr>
            <th><?= Yii::t("client", "User name") ?></th>
            <td><?= User::getNameOfUser($user->id) ?></td>
        </tr>
        <tr>
            <th><?= Yii::t("client", "Avatar") ?></th>
            <td>
            <?php 
            echo Html::img($user->getThumbUploadUrl('avatar', 'preview'), [
                "id" => "user_avatar",
                "class" => "img-circle", 
                "width" => "80", 
                "height" => "80", 
                "style" => "margin-bottom: 5px;", 
            ]); 
            $form = ActiveForm::begin([
                    'id' => 'avatar-form',
                    'action' => '/user/avatar',
                    'options' => [
                        'class' => "form-action",
                        'enctype' => 'multipart/form-data',
                        'role' => "changeAvatar",
                    ],
                    'enableClientValidation' => true,
                ]);
                echo $form->field($user, 'avatar')->fileInput(['maxlength' => true])->label(false);
                echo Html::submitButton(Yii::t("client", "Change"), ['class' => 'btn btn-primary']);
                ActiveForm::end();
            ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t("client", "e-Mail") ?></th>
            <td>
                <?php
                //echo $user->username . "<br>";
                $form = ActiveForm::begin([
                    'id' => 'email-form',
                    'action' => '/user/change-email',
                    'options' => [
                        'class' => "form-action",
                    ],
                    'enableClientValidation' => true,
                ]);
                echo $form->field($user, 'email')->textInput(['maxlength' => true, "disabled" => "disabled"])->label(false);
                echo Html::submitButton(Yii::t("client", "Change"), ['class' => 'btn btn-primary', "disabled" => "disabled"]);
                ActiveForm::end();
                ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t("client", "Password") ?></th>
            <td>
                <?php
                $form = ActiveForm::begin([
                    'id' => 'password-form',
                    'action' => '/user/change-password',
                    'options' => [
                        'class' => "form-action",
                        'role' => "changePassword",
                    ],
                    'enableClientValidation' => true,
                ]);
                echo $form->field($user, 'password')->passwordInput(['maxlength' => true, "value" => false])->label(false);
                echo $form->field($user, 'password2')->passwordInput(['maxlength' => true])->label(false);
                echo Html::submitButton(Yii::t("client", "Change"), ['class' => 'btn btn-primary']);
                ActiveForm::end();
                ?>
            </td>
        </tr>
    </table>
    <hr>
    <h3>Уведомления</h3>
    <table>
        <tr>
            <th><?= Yii::t("client", "PortRussia Subscription") ?></th>
            <td><?= Html::input("checkbox", null, false, ["class" => "checkbox-action", "rel" => "delivery_material", "checked" => $user->deliveryStatus("material")?"checked":false])." ".Html::tag("span", Yii::t("client", "e-Mail delivery")) ?></td>
        </tr>
        <tr>
            <th><?= Yii::t("client", "Comments & Answers") ?></th>
            <td><?= Html::input("checkbox", null, false, ["class" => "checkbox-action", "rel" => "delivery_comment", "checked" => $user->deliveryStatus("comment")?"checked":false])." ".Html::tag("span", Yii::t("client", "e-Mail delivery")) ?></td>
        </tr>
        <tr>
            <th><?= Yii::t("client", "New friends") ?></th>
            <td><?= Html::input("checkbox", null, false, ["class" => "checkbox-action", "rel" => "delivery_events", "checked" => $user->deliveryStatus("events")?"checked":false])." ".Html::tag("span", Yii::t("client", "e-Mail delivery")) ?></td>
        </tr>
    </table>
    <hr>
    <h3>Дополнительно</h3>
    <table>
        <tr>
            <th><?= Yii::t("client", "Twitter") ?></th>
            <td>
                <?php
                if (!isset($socials['twitter'])) {
                    echo Html::a(Yii::t("client", "Auth"), ["/site/auth-social", "authclient" => "twitter", "redirect" => "user", "tab" => "settings"], ["class" => "auth-link button twitter"]);
                }
                else echo Html::input("checkbox", null, false, ["class" => "checkbox-action", "rel" => "publish_twitter", "checked" => $user->publishStatus("twitter")?"checked":false])." ".Html::tag("span", Yii::t("client", "Default publish my posts")) 
                ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t("client", "Faceboock") ?></th>
            <td>
                <?php
                if (!isset($socials['facebook'])) {
                    echo Html::a(Yii::t("client", "Auth"), ["/site/auth-social", "authclient" => "facebook", "redirect" => "user", "tab" => "settings"], ["class" => "auth-link button facebook"]);
                }
                else echo Html::input("checkbox", null, false, ["class" => "checkbox-action", "rel" => "publish_facebook", "checked" => $user->publishStatus("facebook")?"checked":false])." ".Html::tag("span", Yii::t("client", "Default publish my posts")) 
                ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t("client", "VKontakte") ?></th>
            <td>
                <?php
                if (!isset($socials['vkontakte'])) {
                    echo Html::a(Yii::t("client", "Auth"), ["/site/auth-social", "authclient" => "vkontakte", "redirect" => "user", "tab" => "settings"], ["class" => "auth-link button vkontakte"]);
                }
                else echo Html::input("checkbox", null, false, ["class" => "checkbox-action", "rel" => "publish_vkontakte", "checked" => $user->publishStatus("vkontakte")?"checked":false])." ".Html::tag("span", Yii::t("client", "Default publish my posts")) 
                ?>
            </td>
        </tr>
    </table>
</div>


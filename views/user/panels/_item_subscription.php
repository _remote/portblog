<?php
use yii\bootstrap\Html;

use app\models\User;
use app\models\UserSubscriptions;

//echo $model->id_autor;

if ($user = User::findIdentity($model->id_autor)) {
?>
<div class="user detail list">
    <div class="avatar">
        <?= Html::img($user->getThumbUploadUrl('avatar', 'preview'), ["class" => "img-circle"]); ?>
    </div>
    <div class="name"><?= Html::a($user->getFio(), ["/user/view", "id" => $user->id]) ?></div>
    <!--<div class="email"><?= $user->username ?></div>-->
    <!--<div class="rating">Рейтинг<span>567</span></div>-->
    <div class="buttons">
        <?php if (!UserSubscriptions::isSigned($user->id)) { ?>
        <button class="subscription" onclick="subscription(this, 'user', <?= $user->id ?>);">Подписаться</button>
        <?php } else { ?>
        <button class="subscription user_unsubscribe" onclick="bootbox.confirm('<?= Yii::t('client', 'Are you sure you want to unsubscribe from {user}?', ["user" => $user->getFio()]) ?>', function(status) { if (status) {unsubscription($('button.subscription.user_unsubscribe'), 'user', <?= $user->id ?>);}});" >Отписаться</button>
        <?php } ?>
        <button class="message">Написать</button>
    </div>
</div>
<?php } ?>


<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use yii\web\JsExpression;
use app\components\Settings;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\file\FileInput;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;

$class_section = Settings::getInstance()->getClassName("blogs", Settings::SECTION);
$class_subsection = Settings::getInstance()->getClassName("blogs", Settings::SUB_SECTION);

$form = ActiveForm::begin([
    'id' => 'edit-post',
    'options' => [
        'class' => (empty($edit))?"form-action":false,
        'enctype' => 'multipart/form-data',
    ],
    'action' => (empty($edit))?'/user/create-post':false,
    'enableClientValidation' => true,
]);
?>

<?php $form->field($model, "id_user")->hiddenInput()->label(false); ?> 
<?php $form->field($model, "id_language")->hiddenInput()->label(false); ?> 
<?php $form->field($model, "date_created")->hiddenInput()->label(false); ?> 
<?php $form->field($model, "date_public")->hiddenInput()->label(false); ?> 

<?= $form->field($model, "title")->textInput(); ?> 
<div class="row">
    <div class="col-md-6">
    <?php
    echo $form->field($model, "id_section", [
            //'template' =>  "{label} ".Html::a('', ['/admin/'.Settings::getInstance()->getModuleId().'/section/create', "id_language" => $id_language], ['class' => 'glyphicon glyphicon-plus action-button', 'target' => "_blank"])." {input}" 
        ])->widget(Select2::classname(), [
        'options' => [
            'id'=>"section-id",
            'placeholder' => Yii::t('material', 'Select a section ..')
        ],
        'data' => ArrayHelper::map($class_section::find()->select(['name', 'id'])->where(["id_language" => $model->id_language])->orderBy("`name` DESC")->all(), 'id', 'name')

    ]);
    ?>
    </div>
    <div class="col-md-6">
    <?php
    $subsection = $class_subsection::find()->where(["id_section" => $model->id_section])->asArray()->all();
    echo $form->field($model, "id_subsection", [
            //'template' =>  "{label} ".Html::a('', ['/admin/'.Settings::getInstance()->getModuleId().'/subsection/create', "id_language" => $id_language], ['class' => 'glyphicon glyphicon-plus action-button', 'target' => "_blank"])." {input}" 
        ])->widget(DepDrop::classname(), [
        'data' => @ArrayHelper::map($subsection, 'id', 'name'),
        'options'=>['id'=>"subsection-id"],
        'type' => DepDrop::TYPE_SELECT2,
        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
        'pluginOptions'=>[
            'depends'=>["section-id"],
            'placeholder'=>Yii::t('material', 'Select a subsection..'),
            'url'=>Url::to(['sublist'])
        ]
    ]);
    ?>
    </div>
</div>
<?php
echo $form->field($model, "scenario")->hiddenInput()->label(false);
echo $form->field($model, "image_file")->widget(FileInput::classname(), [
    'options' => [
        'accept' => 'image/*',
        //'multiple'=>true
    ],
    'pluginOptions' => [
        'showCaption' => false,
        'showRemove' => false,
        'showUpload' => false,
        'browseClass' => 'btn btn-primary btn-block',
        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
        'browseLabel' =>  Yii::t('material', 'Select main photo'),
        'initialPreview'=> ($model->image_file)?Html::img($model->getThumbUploadUrl('image_file', 'preview'), ['class' => 'img-thumbnail']):($model->image?Html::img($model->image, ['class' => 'img-thumbnail']):false), 
        'overwriteInitial'=>true,
    ],
    'pluginEvents' => [
        "fileclear" => "function() { jQuery('#".$model->getShortName(true)."-scenario').val('default');}",
        "fileloaded" => "function() { jQuery('#".$model->getShortName(true)."-scenario').val('update')}",
    ],
]);
?>

<?= 
$form->field($model, "text")->widget(TinyMce::className(), [
    'options' => ['rows' => 15],
    'language' => Yii::$app->language,
    'clientOptions' => [
        'height' => 400,
        "menubar" => false,
        "statusbar" => false,
        "content_css" => "/css/tinymce_content.css?t=".time(),
        'plugins' => [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],
        'toolbar1' => "insertfile undo redo | link image media | code preview | bold italic",
        //'toolbar2' => " bold italic | alignleft aligncenter alignright alignjustify | forecolor backcolor",
        'image_advtab' => true,
        "relative_urls" => false,
        "remove_script_host"=> false,
        "file_browser_callback_types" => 'image',
        'file_browser_callback'=> new yii\web\JsExpression("function(field_name, url, type, win) {
            if(type=='image') {
                $('#upload_form_model input').val('{$model->getShortName()}');
                $('#upload_form_object input').val({$model->id});
                $('#upload_form input[type=file]').click();
            }
        }"),
    ],

]); 
//$form->field($model, "[{$id_language}]text")->textarea(['rows' => 10]);
?>

<?php    
echo $form->field($model, "_temp_gallery[]", [
        'template' =>  "{label} ".((!$model->isNewRecord && $model->gallery)?$model->buildSlider(7, true):false)."{input}" 
    ])->widget(FileInput::classname(), [
    'options' => [
        'accept' => 'image/*',
        'multiple'=>true
    ],
    'pluginOptions' => [
        'showRemove' => false,
        'showUpload' => false,
        'overwriteInitial'=>true,
        //'uploadUrl' => Url::to(['/site/file-upload']),
    ],
]);
?>

<?= $form->field($model, "tags")->textInput() ?>

<?= Html::hiddenInput("action", "preview", ["id" => 'post-submit-action']); ?>

<script type="text/javascript">
    function change_action(value) {
        $("#post-submit-action").val(value);
    }
</script>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', "onclick" => "change_action('save');"]) ?>
    <?= Html::submitButton(Yii::t('common', 'Preview'), ['class' => 'btn btn-success', "onclick" => "change_action('preview');"]) ?>
</div>

<?php
ActiveForm::end();
?>


<?php
if (!$model->isNewRecord && $model->is_show == $model::STATUS_DRAFT) {
?>
<div class="row col-md-12" id="preview">
    <hr>
    <h2><?= $model->title ?></h2>
    <?= $model->text ?>
</div>
<?php } ?>


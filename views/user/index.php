<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;

use app\components\Site;

?>  
<div class="row">
    <div class="material_content col-md-9">
        <?php
        echo $this->render($view?"_view":"_me", [
            "user" => $user,
            "socials" => $socials,
            "posts" => $posts,
            "model" => $model,
            "tab" => $tab,
        ]);
        ?>
    </div>
    <div class="col-md-3">
        <?= $this->render("@views/site/templ/rblock", ["user" => $user, "view" => $view]) ?>
    </div>
</div>



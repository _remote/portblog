<?php
use \yii\widgets\ListView;
use yii\bootstrap\Modal;
use \yii\web\JsExpression;
use yii\widgets\Pjax;
use yii\helpers\Html;

use app\components\Settings;
?>

<p>
<?php
//Блок, для отправки комментария к материалу
if (isset($model)) {
    //if (!Yii::$app->user->isGuest) { 
        $this->registerJs(
            '$("document").ready(function(){ 
                 $("#_send_message").on("pjax:end", function() {
                     $.pjax.reload({container:"#_list_message"});
                 });
             });'
        );
        echo "<div class=\"form\">";
        Pjax::begin(['id' => '_send_message']);
        echo $this->render("comment_form", ["model" => $model]);
        Pjax::end();
        echo "</div>";
//    } else { 
//        echo $this->render("comment_form", ["model" => $model]);
//    } 
}
?>
</p>
<p>
<?php
//Список сообщений
Pjax::begin(['id' => '_list_message', 'options' => ['class' => 'pjax-wraper']]);
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => 'comment_item',
    'layout' => "{items}\n{pager}",
    'viewParams' => [
        "levels" => $levels,
    ]
]);
Pjax::end();
?>
</p>

<?php
$_id_block = $model->getShortName(true);
//Отправка личного сообщения
Modal::begin([
    'id' => 'commentForm',
    'header' => '<h3 id="'.$_id_block.'-header"></h3>',
    'clientEvents' => [
        'show.bs.modal' => new JsExpression("function(event){
            var button = $(event.relatedTarget);
            var id_comment = button.data('id_comment');
            var title = button.data('title');
            var modal = $(this);
            //modal.find('#{$_id_block}-header').text('Ответ на \"' + title + '\"');
            modal.find('#{$_id_block}-id_comment').val(id_comment);
        }"),
    ]
]);


$this->registerJs(
   '$("document").ready(function(){ 
        $("#_send_comment").on("pjax:end", function() {
            $("#commentForm").modal("hide");
            $.pjax.reload({container:"#_list_message"});
        });
    });'
);

echo "<div class=\"form\">";
Pjax::begin(['id' => '_send_comment']);
echo $this->render("comment_form", ["model" => $model]);
Pjax::end();
echo "</div>";
Modal::end();

?>
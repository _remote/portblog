<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\components\Site;
use app\models\User;
use yii\widgets\Pjax;
?>

<div class="row">
    <div class="material_title">
        <span>Блоги</span>
    </div>
    <?php
    Pjax::begin(['id' => 'post-list', 'options' => ['class' => 'pjax-wraper']]);
    foreach ($items as $key => $items_for_type) {
        foreach ($items_for_type as $item) {
    ?>
        <div class="material blog col-xs-12" itemscope itemtype="http://schema.org/Article">
            <h2 itemprop="name"><?= Html::a($item->title, ["/material/{$selected}/{$item->slug}"], ["itemprop" => "url"]) ?></h2>
            <?php 
            if (!Yii::$app->user->isGuest && $item->id_user == Yii::$app->user->id) {
                echo Html::tag("div", 
                    Html::a(Html::tag("span", null, ["class" => "glyphicon glyphicon-pencil"]), ["/user/update-post", "id" => $item->id]).
                    Html::a(Html::tag("span", null, ["class" => "glyphicon glyphicon-trash"]),  ["/user/delete-post", "id" => $item->id], [
                        'data' => [
                            'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                            'pjax' => 0,
                        ],
                        'class' => 'ajax-action'
                    ]),

                    ["class" => "buttons"]
                );
            }
            ?>
            <div class="info blog">
                <div class="user_name" itemprop="author"><?= Html::a(User::getNameOfUser($item->id_user), User::isMe($item->id_user)?["/user"]:["/user/view", "id" => $item->id_user], [
                    'data' => [
                        'pjax' => 0,
                    ]
                ]) ?></div>
                <div class="date" title="<?= $item->date_created ?>" itemprop="dateCreated"><?= $item->formatDate() ?></div>
                <div class="bar">
                    <div class="comments" onclick="viewComments(this);"><span class="glyphicon glyphicon-comment"></span><div class="count" itemprop="commentCount"><?= $item->comments_count ?></div></div>
                    <div class="views"><span class="glyphicon glyphicon-eye-open"></span><div class="count"><?= $item->view_count ?></div></div>
                </div>
            </div>
            <div class="row">
                <?php if ($item->view_count >= 0 && !empty($item->image)) { ?>
                <div class="col-xs-12">
                    <div class="image preload blog">
                        <?php
                        if (!empty($item->image)/* && Site::image_exisits($item->image)*/) { 
                            //echo '<b class="loading"></b>';
                            echo Html::img($item->image, ["itemprop" => "image"]);
                        }
                        //else echo Html::tag("div", null, ['class' => "without_image"]);
                        ?>
                    </div>
                </div>
                <?php } ?>
                <div class="col-xs-12" itemprop="description">
                    <?=
                    $item->short_text."...";
                    ?>
                </div>
            </div>
        </div>
    <?php
        }
    }
    Pjax::end();
    ?>
</div>


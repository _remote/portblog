<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\widgets\ActiveForm;

use app\components\Site;
use app\components\Settings;
?>

<div class="menu row" style="min-height: 20px;">
<?php
$_items = [];
if ($footer_items = Site::getInstance()->getFooter()) {
    foreach ($footer_items as $item) {
        $_items[] = [
            "label" => $item->title,
            "url" => !empty($item->url)?$item->url:"/info/".$item->slug
        ];
    }
}
echo Nav::widget([
    'encodeLabels' => false,
    'items' => $_items,
    'options' => ['class' => 'navbar-nav'],
    ]); 
?>
</div>
<div class="row">
    <div class="col-md-9">
        <div class="social-groups">
            <ul class="social_list">
                <li><?= Html::a(Html::tag("span", null, ["class" => "social-icons facebook"]), ["#"], ["options" => ["class" => "social"]]) ?></li>
                <li><?= Html::a(Html::tag("span", null, ["class" => "social-icons tweeter"]), ["#"], ["options" => ["class" => "social"]]) ?></li>
                <li><?= Html::a(Html::tag("span", null, ["class" => "social-icons vk"]), ["#"], ["options" => ["class" => "social"]]) ?></li>
            </ul>
        </div>
        <div class="description">
            <?= Settings::getInstance()->getConfig("footer") ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="callback-form">
            <?php
            $callback = new \app\models\Callback;
            ?>
            <?php 
            $form = ActiveForm::begin([
                'id' => 'callback_form',
                'action' => '/site/callback',
                //'method' => 'get',
                'options' => [
                    'class' => "form-action",
                    'role' => 'callback'
                ],
                'enableClientValidation' => false,
            ]); 
            ?>

            <?= $form->field($callback, 'autor_name')->textInput(['maxlength' => true, "placeholder" => Yii::t("client", "Name")])->label(false) ?>

            <?= $form->field($callback, 'autor_email')->textInput(['maxlength' => true, "placeholder" => Yii::t("client", "e-Mail")])->label(false) ?>

            <?php $form->field($callback, 'autor_telephone')->textInput(['maxlength' => true])->label(false) ?>

            <?= $form->field($callback, 'text')->textarea(['maxlength' => true, /*"placeholder" => Yii::t("client", "Text"),*/ "style" => "height: 100px;"])->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t("client", 'Send'), ['class' => 'btn']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
    
</div>


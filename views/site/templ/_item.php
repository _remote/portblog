<?php
use yii\helpers\Html;
use yii\helpers\Url;

use app\components\Site;
use app\modules\files\models\Icons;
?>
<?php 
if (!$is_main) {
?>
<div class="mcontent"> 
    <div class="image preload">
        <b class="loading"></b>
    <?php
    if (!empty($item->image)/* && Site::image_exisits($item->image)*/) { 
        echo Html::img($item->image,["itemprop" => "image"]);
    }
    //else echo Html::tag("div", null, ['class' => "without_image"]);
    ?>
        <div class="icon_photo hide">
            <?= Icons::getImage($item->id_icon, false) ?>
        </div>
    </div>
    <div class="info">
        <div class="date" itemprop="dateCreated" title="<?= $item->date_public ?>"><?= $item->formatDate()  ?></div>
        <div class="bar">
            <div class="comments" onclick="viewComments(this);"><span class="glyphicon glyphicon-comment"></span><div class="count" itemprop="commentCount"><?= $item->comments_count ?></div></div>
            <div class="views"><span class="glyphicon glyphicon-eye-open"></span><div class="count"><?= $item->view_count ?></div></div>
        </div>
    </div>
    <div class="text">
        <span itemprop="name"><?= strip_tags(htmlspecialchars_decode($item->title)) ?></span>. <span itemprop="description"><?= htmlspecialchars_decode($item->short_text) ?></span>
    </div>
</div>
<?php
}
else { //Главная новость
?>
<div class="main_content">
    <div class="col-md-6 preload">
        <b class="loading"></b>
        <?php
        if (!empty($item->image) && Site::image_exisits($item->image)) { 
            echo Html::img($item->image, ["class" => "", "itemprop" => "image"]);
        }
        ?>
    </div>
    <div class="col-md-6 desc small">
        <h2><?= strip_tags(htmlspecialchars_decode($item->title)) ?></h2>
        <div class="section">
            <?php
            if ($name = $item->sectionName())
                echo Html::a($name, ["/material/{$type}/section/".@$item->section->slug]);
            if ($name = $item->subSectionName())
                echo " / ".Html::a($name, ["/material/{$type}/section/".@$item->section->slug."/".@$item->subsection->slug]);
            ?>
        </div>
        <div class="text" itemprop="description">
            <?= htmlspecialchars_decode($item->short_text) ?>
        </div>
        <div class="info main">
            <div class="date" itemprop="dateCreated" title="<?= $item->date_public ?>"><?= $item->formatDate()  ?></div>
            <div class="bar">
                <div class="comments" onclick="viewComments(this);"><span class="glyphicon glyphicon-comment"></span><div class="count" itemprop="commentCount"><?= $item->comments_count ?></div></div>
                <div class="views"><span class="glyphicon glyphicon-eye-open"></span><div class="count"><?= $item->view_count ?></div></div>
            </div>
        </div>
    </div>
</div>    
<?php
} 
?>

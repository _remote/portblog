<?php if (!empty($model->gallery)) { ?>
    <div class="gallery">
        <?php
        $items = [];
        foreach ($model->getGalleryImagesList() as $image) 
            $items[] = ["img" => $image];

        echo \metalguardian\fotorama\Fotorama::widget(
            [
                'items' => $items,
                'options' => [
                    'nav' => 'thumbs',
                    'loop' => true,
                    'hash' => true,
                    //'ratio' => 800/600,
                ],
//                            'spinner' => [
//                                'lines' => 20,
//                            ],
//                            'tagName' => 'span',
//                            'useHtmlData' => false,
                'htmlOptions' => [
                    'class' => isset($small)?"small_gallery_theme":false,
                    //'id' => 'custom-id',
                ],
            ]
        ); 
        ?>
    </div>
<?php } ?>


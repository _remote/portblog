<?php
use yii\helpers\Html;
use app\components\Site;

if ($items) {
    $show_main = false; 
    //Ищем главную новость
    foreach ($items as $i => $item) {
        if ($item['is_show_in_top']) {
            $show_main = $i;
            //Site::getInstance()->setMetaKey("image", $item->image);
            $this->title = strip_tags(htmlspecialchars_decode($item['title']));
            echo "<div class=\"material main col-md-12\" itemscope itemtype=\"http://schema.org/Article\">";
            if ($city_link = $item->getCityPath($key)) { 
?>
            <div class="city">
                <?= $city_link ?>
                <div class="_logo"></div>
            </div>
<?php       } 
            echo Html::a($this->render("_item", [
                "item" => $item,
                "type" => $key,
                "is_main" => true,
                //"city_link" => $item->getCityPath($key)
            ]), [$item->getPath($key)]);
            echo "</div>";
            break;
        }
    }
    
    foreach ($items as $i => $item) {
        if (is_int($show_main) && $i == $show_main)
            continue;
        //Если нет главного материала и заполнены две строки, то пропукаем элемент
//      if (!$show_main && 2*$count_in_row == $i)
//          continue;
        $style = ($item['is_selected'])?"active":false;

        echo "<div class=\"material col-sm-4 {$style}\" itemscope itemtype=\"http://schema.org/Article\">";
        if ($city_link = $item->getCityPath($key)) {
        ?>
            <div class="city">
                <?= $city_link ?>
                <div class="_logo"></div>
            </div>
        <?php  
        }
        echo Html::a($this->render("_item", [
            "item" => $item,
            "type" => $key,
            "is_main" => false,
            "city_link" => $item->getCityPath($key)
        ]), [$item->getPath($key)]);
        echo "</div>";
    }
}


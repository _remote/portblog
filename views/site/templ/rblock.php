<?php
use yii\widgets\Pjax;
use yii\helpers\Html;
use app\models\User;
use app\models\UserSubscriptions;
?>

<div class="row">
    <?php if (isset($user)) { ?>
    <div class="user details hidden-xs">
        <div class="avatar">
            <?= Html::img($user->getThumbUploadUrl('avatar', 'preview'), ["class" => "img-circle"]); ?>
        </div>
        <div class="name"><?= $user->first_name ?></div>
        <!--<div class="email"><?= $user->username ?></div>-->
        <div class="rating">Рейтинг<span>567</span></div>
        <div class="hr"></div>
        <ul class="info">
            <li class="left">Постов<span><?= User::countPosts($user->id) ?></span></li>
            <li>Комментариев<span><?= User::countComments($user->id) ?></span></li>
            <li class="right">Нравиться<span>0</span></li>
        </ul>
        <?php if (isset($view) && $view) { ?>
        <?php Pjax::begin(['id' => '_user_subscription']); ?>
        <div class="buttons">
            <?php if (!UserSubscriptions::isSigned($user->id)) { ?>
            <button class="subscription" <?php if (!Yii::$app->user->isGuest) {?>onclick="subscription(this, 'user', <?= $user->id ?>);"<?php } else { ?>data-toggle='modal' data-target="#form-auth-modal"<?php } ?>>Подписаться</button>
            <?php } else { ?>
            <button class="subscription user_unsubscribe" onclick="bootbox.confirm('<?= Yii::t('client', 'Are you sure you want to unsubscribe from {user}?', ["user" => $user->getFio()]) ?>', function(status) { if (status) {unsubscription($('button.subscription.user_unsubscribe'), 'user', <?= $user->id ?>);}});" >Отписаться</button>
            <?php } ?>
            <button class="message" <?php if (!Yii::$app->user->isGuest) {?>onclick="send_message(this, 'user', <?= $user->id ?>);"<?php } else { ?>data-toggle='modal' data-target="#form-auth-modal"<?php } ?>>Написать</button>
        </div>
        <?php Pjax::end(); ?>
        <?php } ?>
    </div>
    <?php } ?>
    <?= !empty($material)?$this->render("../modules/last_comments", ["material" => $material]):""; ?>
    <?= (empty($material) || $material != "news")?$this->render("../modules/last_material", ["material" => "news"]):"" ?>
    <div class="widget_block"> 
        <div class="title">
            <span>Реклама</span>
        </div>
        <div class="body">
            <div class="banner widget"></div>
        </div>
    </div>
    <?= (empty($material) || $material != "events")?$this->render("../modules/last_material", ["material" => "events"]):"" ?>
    <?= (empty($material) || $material != "articles")?$this->render("../modules/last_material", ["material" => "articles"]):"" ?>
    <div class="widget_block"> 
        <div class="title">
            <span>Instagram</span>
        </div>
        <div class="body">
            модуль отключен
        </div>
    </div>
    <?php 
    if (isset($material)) {
        echo $this->render("../modules/tags", ["material" => $material, "without_header" => true]);
    } 
    ?>
    
</div>


<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\components\Site;

$i = 1;
foreach ($items as $key => $items_for_type) {
    //Что не надо отображать на главной
    if (in_array($key, ['blogs']))
        continue;
?>
<?php if ($i++ % 3 == 0) { ?>
<div class="row empty">
    <div class="banner material"></div>
</div>
<?php } ?>
<div class="row">
    <div class="material_title"><span><?= $key ?></span></div>
    <?php
    echo $this->render("list", ["items" => $items_for_type, "key" => $key, "material" => $selected]);
    ?>
    <?php if ($counts[$key] > count($items_for_type)) { //Если элементов больше чем показаноssss ?>
    <div class="more col-md-12">
        <div class="button" role="<?= $key ?>" page="2" onclick="loadMore(this, <?= $id_section?$id_section:0 ?>, <?= $selected?1:0 ?>);">Показать еще</div>
    </div>
    <?php } ?>
</div>

<?php
}
?>

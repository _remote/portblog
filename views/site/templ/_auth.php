<?php
use yii\bootstrap\Html;
use app\models\LoginForm;
use yii\widgets\ActiveForm;

use app\models\User;
?>

<div class="form_auth">
    <!--<div class="h3">Вход через соцсети</div>-->
    
    <!--<div class="hr"></div>-->
    <?php 
    $model = new LoginForm;
    $form = ActiveForm::begin([
        'id' => 'loginform',
        'action' => '/site/auth',
        'enableClientValidation' => true,
    ]);
    echo Html::input("hidden", "selector", "#send_comment_button");
    echo Html::input("hidden", "action", null);
    ?>
    <div class="frow auth_error"></div>
    
    <div class="frow">
        <div class="grid">
            <span><?= $form->field($model, "username")->textInput(["placeholder" => "e-Mail"])->label("Логин") ?></span>
        </div>
    </div>
    <div class="frow">
        <div class="">
            <span><?= $form->field($model, "password")->passwordInput(["placeholder" => "Пароль"])->label("Пароль") ?></span>
        </div>
    </div>
    <div class="frow">
        <?= $form->field($model, "rememberMe")->checkbox()->label(false) ?>
    </div>
<!--    <div class="frow">
        Забыли пароль? <a class="form_forgot_a" href="javascript:void(0);">Восстановить</a>
    </div>-->
    <span class="btn user_auth">
        <input type="submit" value="">Войти
    </span>
    <?php
    ActiveForm::end();
    ?>
    <div class="black-social">
        <?php echo yii\authclient\widgets\AuthChoice::widget([
                'baseAuthUrl' => ['site/auth-social', "redirect" => preg_replace("/\?.*$/", "", $_SERVER['REQUEST_URI']), "anchor" => @$anchor]
           ]);
        ?>
    </div>
</div>						
<div class="form_forgot">
    <div class="h3">Восстановление пароля</div>
    <div role="alert"></div>
    <div class="frow">
        Попробовать еще раз <a class="form_login_a" href="javascript:void(0);">Войти</a>
    </div>
    <?php 
    $model = new User;
    $form = ActiveForm::begin([
        'id' => 'recover-form',
        'action' => '/user/recover',
        'method' => 'get',
        'options' => [
            'class' => "form-action",
            'role' => 'changePassword'
        ],
        'enableClientValidation' => true,
    ]);
    ?>
    <div class="mfs_inp full">
        <span><?= $form->field($model, "email")->textInput(["placeholder" => "e-Mail"])->label(false) ?></span>
    </div>
    <span class="user_forgot btn"><input type="submit" value="">восстановить пароль</span>
    <?php
    ActiveForm::end();
    ?>
</div>


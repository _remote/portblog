<?php
//use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\ActiveForm;

use app\models\User;
use app\components\Site;

use app\models\LoginForm;
use app\models\SignupForm;

use yii\widgets\Pjax;
use app\models\Language;

//Генерация меню для сайта
$menu_html = "";
$menu_items = Site::getInstance()->getMenu();
$menu_html .= "<ul>";
$menu_html .= Html::tag("li", 
    Html::a(null, ["/"], ['class' => "logo"])
);
$drop = "";
if (!empty($menu_items)) {
    foreach ($menu_items as $index => $menu) {
        $class = false;
        if (in_array($menu->url, ["/".@$this->params['selected'], "/material/".@$this->params['selected']])) {
            $class = "active";
        }
        ${($index < 5)?"menu_html":"drop"} .= Html::tag("li", 
            Html::a($menu->title, Url::toRoute($menu->url)),
            ["class" => $class]
        );

    }
}
if (!empty($drop)) {
    $menu_html .= "<li><a href=\"javascript:void(0);\"><span>Еще</span></a><div class=\"drop\"><ul>";
    $menu_html .= $drop;
    $menu_html .= "</div></ul></li>";
}
$menu_html .= "</ul>";

$items = [];
if (!empty($menu_items)) {
    foreach ($menu_items as $menu) {
        $class = false;
        if (in_array($menu->url, ["/".@$this->params['selected'], "/material/".@$this->params['selected']])) {
            $class = "active";
            //Site::getInstance()->setMeta($menu, __FILE__);
        }
        $item = [
            'label' => $menu->title, 
            'url' => Url::toRoute($menu->url),
            'options' =>  ['class' => $class],
        ];
        $items[] = $item;
    }
}
//Yii::$app->debug->show($items);
?>
<div style="height: 1px; width: 1px; background-color: transparent;" class="visible-xs-block visible-sm-block" id="small_status"></div>
<div class="visible-xs-block">
    <?php 
    NavBar::begin(['brandLabel' => ""]);
    echo Nav::widget([
        'items' => $items,
        'options' => ['class' => 'navbar-nav'],
    ]); 
    NavBar::end();
    ?>
</div>
<?php if ($banner) { ?>
<div class="banner_back"> 
    <div class="banner top"></div>
</div>
<?php } ?>
<div id="top" class="hidden-xs">
    <div class="back"></div>
    
    <div class="aligner">
        <div class="menu">
            <?php 
            echo $menu_html;
            ?>
        </div>
        <?php Pjax::begin(['id' => '_user_information']); ?>
        <ul class="loginbox">
            <li>
                <div class="languages">
                    <div class="languages_select">
                        <!--<div id="drop_btn"></div>-->
                        <span><?= Html::img(Language::getCurrentIcon()); ?></span>
                    </div>
                    <ul class="languages_list">
                        <?php 
                        $languages = Language::iconsList();
                        foreach ($languages as $type => $language) {
                            echo "<li rel=\"{$type}\">{$language}</li>";
                        }
                        ?>
                    </ul>
                </div>
            </li>
            <?php if (Yii::$app->user->isGuest) { ?>
            <li class="">
                <a href="javascript:void(0);" class="opcl link_auth"><?= Yii::t("client", "Login") ?></a>
                <div class="drop link_auth_drop">
                    <div class="form_auth">
                        <div class="h2"><?= Yii::t("client", "Login as user") ?></div>
                        <?php 
                        $model = new LoginForm;
                        $form = ActiveForm::begin([
                            'id' => 'loginform',
                            'action' => '/site/auth',
                            'enableClientValidation' => true,
                        ]);
                        ?>
                        <div class="frow auth_error"></div>
                        <div class="frow">
                            <?= Yii::t("client", "Forgot your password?") ?> <a class="form_forgot_a" href="javascript:void(0);"><?= Yii::t("client", "Remind password") ?></a>
                        </div>
                        <div class="frow">
                            <div class="mfs_inp grid">
                                <span><?= $form->field($model, "username")->textInput(["placeholder" => "e-Mail"])->label(false) ?></span>
                            </div>
                            <div class="mfs_inp">
                                <span><?= $form->field($model, "password")->passwordInput(["placeholder" => "Пароль"])->label(false) ?></span>
                            </div>
                        </div>
                        <div class="frow">
                            <label><?= $form->field($model, "rememberMe")->checkbox() ?></label>
                        </div>
                        <span class="btn user_auth">
                            <input type="submit" value=""><?= Yii::t("client", "Login") ?>
                        </span>
                        <?php
                        ActiveForm::end();
                        ?>
                        <div class="black-social">
                            <?php echo yii\authclient\widgets\AuthChoice::widget([
                                    'baseAuthUrl' => ['site/auth-social']
                               ]);
                            ?>
                        </div>
                    </div>						
                    <div class="form_forgot">
                        <div class="h2"><?= Yii::t("client", "Password recovery") ?></div>
                        <div role="alert"></div>
                        <div class="frow">
                            <?= Yii::t("client", "Try again") ?> <a class="form_login_a" href="javascript:void(0);"><?= Yii::t("client", "Login") ?></a>
                        </div>
                        <?php 
                        $model = new User;
                        $form = ActiveForm::begin([
                            'id' => 'recover-form',
                            'action' => '/user/recover',
                            'method' => 'get',
                            'options' => [
                                'class' => "form-action",
                                'role' => 'changePassword'
                            ],
                            'enableClientValidation' => true,
                        ]);
                        ?>
                        <div class="mfs_inp full">
                            <span><?= $form->field($model, "email")->textInput(["placeholder" => "e-Mail"])->label(false) ?></span>
                        </div>
                        <span class="user_forgot btn"><input type="submit" value=""><?= Yii::t("client", "password recovery") ?></span>
                        <?php
                        ActiveForm::end();
                        ?>
                    </div>
                </div>
            </li>
            <li class="">
                <a href="javascript:void(0);" class="opcl link_registration"><?= Yii::t("client", "Registration") ?></a>
                <div class="drop link_registration_drop">
                    <div class="h2"><?= Yii::t("client", "Registration") ?></div>
                    <?php 
                    $model = new SignupForm;
                    $form = ActiveForm::begin([
                        'id' => 'signupform',
                        'action' => '/site/signup',
                        'enableClientValidation' => true,
                    ]);
                    ?>
                    <div class="frow reg_error"></div>
                    <div class="form_reg">
                        <div class="mfs_inp full">
                            <span><?= $form->field($model, "first_name")->textInput(["placeholder" => Yii::t("client", "First name")])->label(false) ?></span>
                        </div>
                        <div class="mfs_inp full">
                            <span><?= $form->field($model, "last_name")->textInput(["placeholder" => Yii::t("client", "Last name")])->label(false) ?></span>
                        </div>
                        <div class="mfs_inp full">
                            <span><?= $form->field($model, "username")->textInput(["placeholder" => "e-Mail"])->label(false) ?></span>
                        </div>
                        <div class="mfs_inp full">
                            <span><?= $form->field($model, "password")->passwordInput(["placeholder" => Yii::t("client", "Password")])->label(false) ?></span>
                        </div>
                        <span class="btn user_reg"><input type="submit" value=""><?= Yii::t("client", "Registration") ?></span>
                    </div>
                    <?php
                    ActiveForm::end();
                    ?>
                </div>
            </li>
            <?php } else { ?>
                <li class="">
                    <a href="/user" class="opcl link_auth" data-pjax="0"><?= User::getNameOfUser(Yii::$app->user->id) ?></a>
                </li>
                <li class=""><?= Html::a(Yii::t("client", "Logout") , ["/site/logout", "redirect" => preg_replace("/\?.*$/", "", $_SERVER['REQUEST_URI'])], ["class" => "opcl link_auth", "data" => array("pjax" => 0)]) ?></li>
            <?php } ?>
        </ul>
        <?php Pjax::end(); ?>
        <div class="search" style="width: 50px;">
            <form method="get" action="/site/search">
                <a href="#" class="openclose"></a>
                <div class="input"><span><input type="text" name="text" value="" placeholder="<?= Yii::t("client", "serch") ?>"></span></div>
                <input type="hidden">
            </form>
        </div>
    </div>
</div>


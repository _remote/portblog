<?php
use yii\helpers\Html;
use app\components\Site;
?>

<?php if (isset($items['blogs'])) { //Предфутер ?>
<?php // Стили переопределяются для того, чтобы увеличить размер футера ?>
<style type="text/css">
    .wrap {
        padding-bottom: 715px;
    }
    .footer {
        margin-top: -715px;
        min-height: 715px;
    }
    .prefooter {
        height: 415px;
    }
    
</style>
<div class="talking row">
    <div class="col-md-9">
        <div class="header com-md-12">
            <div class="title"><h2><?= Yii::t('client', "What talking about?") ?></h2></div>
            <div class="create">
                <?php 
                if (Yii::$app->user->isGuest)
                    echo Html::a(Yii::t("client", "Create post"), ["/user", "tab" => "create_post"], ['class' => "btn create-post-button-action", "data" => ["toggle" => "modal", "target" => "#form-auth-modal"]]);
                else echo Html::a(Yii::t("client", "Create post"), ["/user", "tab" => "create_post"], ['class' => "btn create-post-button-action"]);
                ?>
            </div>
        </div>
        <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
            <?php
            foreach ($items['blogs'] as $item) {
            ?>
            <div class="post_item col-md-6">
                <div class="post_title"><?= Html::a($item->title, $item->getPath("blogs")) ?></div>
                <div class="info">
                    <div class="post_date"><?= $item->date_public ?></div>
                    <div class="post_section">
                        <?php
                        if ($name = $item->sectionName())
                            echo Html::a($name, ["/".Site::getInstance ()->getLanguage ()."//blogs/section/".@$item->section->slug]);
                        if ($name = $item->subSectionName())
                            echo " / ".Html::a($name, ["/".Site::getInstance ()->getLanguage ()."//blogs/section/".@$item->section->slug."/".@$item->subsection->slug]);
                        ?>
                    </div>
                </div>
            </div>  
            <?php
            }
            ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="header com-md-12">
            <div class="title"><h2><?= Yii::t('client', "Popular tags") ?></h2></div>
        </div>
        <div class="tags col-md-12" style="padding-left: 0px; padding-right: 0px;">
            <?= $this->render("../modules/tags", ["material" => "blogs"]) ?>
        </div>
    </div>
</div>
<?php } ?>


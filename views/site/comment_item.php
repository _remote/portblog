<?php
use yii\helpers\Html;
use app\models\User;
use yii\widgets\Pjax;

use app\components\Settings;

//use app\modules\news\models\Comment;
$class_comment = Settings::getInstance()->getModulePath()."\models\Comment";

$_model = $model;
$model = new $class_comment();
$model->prepare($_model);
$username = User::getNameOfUser($model->id_user);
$user = User::findIdentity($model->id_user);
?>
<div class="comment row" id="mes-id-<?= $_model['id'] ?>">
    <div class="user-box" style="margin-left: <?= isset($levels[$_model['id']])?(25 * ($levels[$_model['id']] - 1)):0 ?>px !important;">
        <?= Html::img($user->getThumbUploadUrl('avatar', 'preview'), ["width" => 47, "height" => 47, "class" => "avatar img-circle"]); ?>
    </div>
    <div class="col-md-8 _body">
        <div class="head">
            <div class="autor"><?= Html::a($username, User::isMe($model->id_user)?["/user"]:["/user/view", "id" => $model->id_user], ["class" => 'view-user-button-action']) ?></div>
            <div class="date" title="<?= $model->date_created ?>"><?= $model->formatDate() ?></div>
        </div>
        <div class="text"><?= $model->text ?></div>
    </div>
    <ul class="buttons">
        <?php
        //if (!Yii::$app->user->isGuest) {
            echo Html::a(Yii::t('client', 'send'), '#', [
                'id' => 'send-button-'.$_model['id'],
                'data-toggle' => 'modal',
                'data-target' => (Yii::$app->user->isGuest)?"#form-auth-modal":"#commentForm",
                'data-id_comment' => $_model['id'],
                'data-title' => (strlen($model->text) > 20)?substr($model->text, 0, 19)."..":$model->text,
                //'data-pjax' => '0',
                'class' => 'send-button-action'
            ]);         
        //}
        ?>
        <?php

        ?>
    </ul>
</div>


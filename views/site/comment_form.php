<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use app\models\User;
use app\components\Settings;

use yii\bootstrap\Modal;

//use app\modules\news\models\Comment;
$class_comment = Settings::getInstance()->getModulePath()."\models\Comment";
?>
<?php 
use yii\widgets\Pjax;
use yii\web\JsExpression;

$form = ActiveForm::begin([
    'options' => ['data-pjax' => 1]
]); 
?>
<?= $form->field($model, 'id_comment')->hiddenInput()->label(false) ?>
<?= $form->field($model, 'text')->textarea()->label(Yii::t('material', "Message")) ?>

<div class="form-group">
    <?php 
    if (Yii::$app->user->isGuest)
        echo Html::Button(Yii::t('client', "Add"), ["id" => "send_comment_button", 'class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => "#form-auth-modal",]); 
    else 
        echo Html::submitButton(Yii::t('client', "Add"), ['class' => 'btn btn-primary']); 
    ?>
</div>
<?php ActiveForm::end(); ?>


<?php
use yii\helpers\Html;

use app\components\Site;

use kartik\social\VKPlugin;
use kartik\social\FacebookPlugin;
use kartik\social\TwitterPlugin;

use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;

//$pos = preg_replace("/\?.*$/", "", $_SERVER['REQUEST_URI']);
//Yii::$app->debug->show($pos);

$this->title = strip_tags($model->title);

$this->params['selected'] = $material;

Site::getInstance()->setMeta($model, __FILE__);

$this->params['breadcrumbs'][] = ['label' => Yii::t("client", $material), 'url' => ["/material/{$material}"]];
$this->params['breadcrumbs'][] = $this->title;

//Устанавливаем шаблон для отображения
if (!is_null($model->template))
    Site::getInstance()->setTemplate($model->template);

//Устанавливаем шаблон для отображения
if (!empty($model->js))
    Site::getInstance()->setJavaScript($model->js);
?>  
<script type="text/javascript">
    incrementViews('<?= $material ?>', <?= $model->id ?>);
</script>
<div class="row">
    <div class="material_content col-md-<?= ($model->template != $model::TEMPLATE_WITHOUT_WIDGETS)?"9":"12 personal" ?>">
        <div class="row">
<!--            <div class="material_title">
                <span>Просмотр материала</span>
            </div>-->
            <?php
            $schema = [
                "blogs" => "Blog",
                "places" => "Place",
                "events" => "Event"
            ];
            ?>
            <div class="col-md-12" itemscope itemtype="http://schema.org/<?= isset($schema[$material])?$schema[$material]:"Article" ?>">
                <h2 itemprop="name"><?= $model->title ?></h2>
                
                <?php if (isset($model->adress)) { ?>
                <div class="map">
                    <?php
                    //echo $model->adress;
                    
                    $coord = new LatLng(['lat' => $model->latitude, 'lng' => $model->longitude]);
                    $map = new Map([
                        'scrollwheel' => false,
                        'center' => $coord,
                        'zoom' => 15,
                    ]);
                    $map->width = "100%";
                    $map->height = 250;
                    $marker = new Marker([
                        'icon' => '/css/images/tool.png',
                        'position' => $coord,
                        'title' => $model->adress,
                    ]);
                    $map->addOverlay($marker);
                    echo $map->display();
                    ?>
                </div>
                <?php if ($model->isShow(["events", "places"])) { ?>
                <div class="row">
                    <table class="event_info">
                        <?php if (!empty($model->work_time)) { ?>
                        <tr>
                            <td class="title"><?= Yii::t("client", ($material == "places")?"Work time":"Time") ?></td>
                            <td itemprop="doorTime"><?= $model->work_time ?></td>
                        </tr>
                        <?php } ?>
                        <?php if (!empty($model->adress)) { ?>
                        <tr>
                            <td class="title"><?= Yii::t("client", "Adress") ?></td>
                            <td itemprop="address"><?= $model->adress ?></td>
                        </tr>
                        <?php } ?>
                        <?php if (!empty($model->telephone)) { ?>
                        <tr>
                            <td class="title"><?= Yii::t("client", "Telephone") ?></td>
                            <td itemprop="telephone"><?= $model->telephone ?></td>
                        </tr>
                        <?php } ?>
                        <?php if (!empty($model->price)) { ?>
                        <tr itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
                            <td class="title"><?= Yii::t("client", "Price") ?></td>
                            <td itemprop="lowPrice"><?= $model->price ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
                <?php } ?>
                <div class="row">
                    <div class="col-md-12">
                        <div id="text" itemprop="text">
                            <div class="col-md-6" style="float: right;">
                                <?= $this->render("templ/_gallery", ["model" => $model]); ?>
                            </div>
                            <?= $model->text ?>
                        </div>

                    </div>
                
                </div>
                <?php } else { ?>
                
                <?php if (Site::image_exisits($model->image)) { ?>
        <!--        <div class="image main full">
                    <?= Html::img($model->image, ["itemprop" => "image"]) ?>
                </div>-->
                <?php }  ?>
        
                <?= $this->render("templ/_gallery", ["model" => $model, "small" => true]); ?>
                <div id="text" itemprop="text">
                    <?= $model->text ?>
                </div>
                <?php } ?>
        
                <div class="socials row">
                    <?= $this->render("templ/socials", ["url" => yii\helpers\Url::to([$model->getPath($material)]), "model" => $model]); ?>
                    <div class="item rating">
                        <?php
                        echo kartik\rating\StarRating::widget([
                            'name' => 'item_rating',
                            'value' => $model->rating,
                            'options' => [
                                "data-size" => "small"
                            ],
                            'pluginOptions' => [
                                'readonly' => false,
                                'showClear' => false,
                                'showCaption' => false,
                                'step' => 0.1,
                            ],
                            'pluginEvents' => [
                                "rating.change" => "function() { rating('{$material}', {$model->id}, this);  }",
//                                "rating.clear" => "function() { console.log('rating.clear'); }",
//                                "rating.reset" => "function() { console.log('rating.reset'); }",
                            ],
                        ]);
                        ?>
                    </div>
                </div>
                
                <?php if (!empty($model->tags)) { ?>
    <!--            <div class="material_title">
                    <span>Теги</span>
                </div>-->
                <div class="tags">
                    <h2>Теги</h2>
                    <?= implode(" ", $model->getTagLinks("/site/search")) ?>
                </div>
                <?php } ?>
    <!--            <div class="material_title">
                    <span>Комментарии</span>
                </div>-->
                
                <div id="comments" itemprop="comment">
                    <h2>Обсуждение</h2>
                    
                    <?= 
                    $this->render("comment_list", [
                        'dataProvider' => $commentProvider,
                        "model" => $commentModel,
                        "levels" => $levels
                    ]) 
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php if ($model->template != $model::TEMPLATE_WITHOUT_WIDGETS) { ?>
    <div class="col-md-3">
        <?= $this->render("templ/rblock", ["material" => $material]) ?>
    </div>
    <?php } ?>
</div>


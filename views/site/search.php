<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\components\Site;
$this->params['selected'] = "search";

//$this->params['breadcrumbs'][] = ['label' => Yii::t("client", $material), 'url' => ["/material/{$material}"]];
$this->params['breadcrumbs'][] = Yii::t("client", "Search");
?>

<div class="row">
    <div class="material_content col-md-9">
        <?php
        //Yii::$app->debug->show($items);
        foreach ($items as $key => $items_for_type) {
            if (!empty($items_for_type) && count($items_for_type)) {
        ?>
        <div class="row">
            <div class="material_title"><span><?= $key ?></span></div>
            <?php
                foreach ($items_for_type as $i => $item) {  
                    echo "<div class=\"material col-md-4\" itemscope itemtype=\"http://schema.org/".(($material != "blogs")?"Article":"Blog")."\">";
                    echo Html::a($this->render("templ/_item", [
                        "item" => $item,
                        "type" => $key,
                        "is_main" => false,
                        "city_link" => $item->getCityPath($key)
                    ]), ["/material/{$key}/{$item['slug']}"]);
                    echo "</div>";
                }
            ?>
        </div>
        <?php
            }
        }
        ?>
    </div>
    <div class="col-md-3">
        <?= $this->render("templ/rblock", ["material" => $material]) ?>
    </div>
</div>


<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\components\Site;

$this->params['selected'] = $selected;
$this->params['items'] = $items;

$view = ($selected == "blogs")?"_blogs":"_index";
?>
<?php 
//Проверям нужно ли выводить разделы или нет
if ($selected && !empty($sections)) { 
    $this->params['breadcrumbs'][] = Yii::t("client", $selected);
?>
<div class="sections">
    <span>Разделы:</span> &nbsp; 				
    <?php
    $s = [];
    $ss = [];
    foreach ($sections as $section) {
        $style = "section_item";
        //Если выбран раздел, то сео-данные будут браться из него
        if ($section['id'] && $section['id'] == $id_section) {
            Site::getInstance()->setMeta($section, __FILE__);
            $style .= " active";
        }
        $url = Yii::$app->language."/".$selected;
        //if (!empty($section['slug'])) $url .= "/section/{$section['slug']}";
        $s[] = Html::a($section['name'], Url::toRoute([$url."/".$section['slug']]), ["rel" => $section['id'], "class" => $style]);
        if (!empty($section['items'])) {
            foreach ($section['items'] as $id => $sub) {
                //Если выбран подраздел, то сео-данные будут браться из него
                if ($sub['id'] && $sub['id'] == $id_sub_section) {
                    Site::getInstance()->setMeta($sub, __FILE__);
                }
                $ss[$section['id']][] = Html::a($sub['name'], Url::toRoute([
                    $url."/".$section['slug']."/".$sub['slug']
                ]), ["rel" => $section['id'], "class" => ($id_sub_section == $sub['id'])?"active":false]);
            }
        }
        else $ss[$section['id']] = false;
    }
    echo implode("|", $s);
    echo "<div class='sub_sections_list'>";
    foreach ($ss as $id => $sub_list) {
        $style = false;
        if ($id_section && $id == $id_section)
            $style = "active";
        echo "<div id='sub-section-list-{$id}' class='sub_sections_items {$style}'>";
        if (!empty($sub_list))
            echo implode(" ", $sub_list);
        echo "</div>";
        
    }
    echo "</div>";
    ?>
</div>
<?php } ?>
<div class="row">
    <div class="material_content col-md-9">
        <?= $this->render("templ/{$view}", [
            "items" => $items,
            "selected" => $selected,
            //"sections" => $sections,
            "id_section" => $id_section,
            "counts" => $counts,
        ]); ?>
    </div>
    <div class="col-md-3">
        <?= $this->render("templ/rblock", ["material" => $selected]) ?>
    </div>
</div>


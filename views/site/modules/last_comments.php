<?php
use yii\helpers\Html;
use app\components\Site;
use app\components\Settings;

$type = (isset($material) && $material != "all")?$material:false;

$materials = ["news", "articles", "events", "companies" => "company", "persons", "places", "blogs"];
$items = [];
foreach ($materials as $path => $material) {
    if ($type && ($type != $material && $type != (string)$path))
        continue;
    
    $class = Settings::getInstance()->getClassName($material, Settings::COMMENT);
    if ($comments = $class::getMainComments(2)) {
        
        foreach ($comments as $comment) {
            $items[] = Html::a($comment->text, [$comment->material->getPath($material), "#" => "mes-id-{$comment->id}"]);
        }
    }
}

if (!empty($items)) {
?>
    <div class="widget_block"> 
        <div class="title">
            <span><?= Yii::t("client", "Comments") ?></span>
        </div>
        <div class="body scrollable">
            <table class="table table-striped">
        <?php
        if (!empty($items)) {
            foreach ($items as $item)
                 echo "<tr><td>{$item}</td></tr>";
        }
        else echo Yii::t("client", "No elements of this section");
        ?>
            </table>
        </div>
    </div>
<?php 
    
} 
?>
<?php
use yii\helpers\Html;
use app\modules\material\models\Tags;

$type = (isset($material) && $material != "all")?$material:false;

$model = new Tags;
if ($tags = $model->findTagWeights(20, $type)) {
    if (@$without_header) {
?>
<div class="widget_block"> 
    <div class="title">
        <span><?= Yii::t("client", "Tags") ?></span>
    </div>
    <div class="tags body">
    <?php
    }
    if (!empty($tags)) {
        foreach ($tags as $tag => &$frequency)
            $frequency = Html::a($tag, ["search/".(($type)?$type:"all")."/".$tag]);
        echo Tags::array2string($tags);
    }
    else echo Yii::t("client", "No elements of this section");
    if (@$without_header) {
    ?>
    </div>
</div>
<?php 
    }
} 
?>

<?php
use Yii;
use yii\bootstrap\Html;

use app\components\Settings;
use app\components\Site;

use app\modules\material\components\Loader;

$block_title = [
    "news" => "Последние новости",
    "events" => "Последние события",
    "articles" => "Последние статьи",
];

$loader = new Loader($material);
$loader->setLanguage(Yii::$app->language);
//$loader->fullList(false, 10);
//$loader->showSQL();
if ($items = $loader->get(false, 20)) {
?>
<div class="widget_block"> 
    <div class="title">
        <span><?= $block_title[$material] ?></span>
    </div>
    <div class="body scrollable">
<?php
if (!empty($items)) {
?>
<table class="table table-striped">
    <?php
    foreach ($items as $item) {
        $title = htmlspecialchars_decode(strip_tags($item->title));
        echo "<tr><td>".Html::a($title, ["/material/{$material}/{$item['slug']}"])."</td></tr>"; 
    } 
    ?>
</table>
<?php
} else echo "Нет элементов";
?>
    </div>
</div>
<?php } ?>


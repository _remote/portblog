<?php
use yii\helpers\Html;
use app\components\Site;

if ($items) {
    foreach ($items as $i => $item) {
        $style = ($item['is_selected'])?"active":false;
        echo "<div class=\"material col-sm-4 {$style} {$material}-page-{$page}\" itemscope itemtype=\"http://schema.org/Article\">";
        echo Html::a($this->render("templ/_item", [
            "item" => $item,
            "type" => $material,
            "is_main" => false,
            "city_link" => $item->getCityPath($material)
        ]), ["/material/{$material}/{$item['slug']}"]);
        echo "</div>";
    }
}


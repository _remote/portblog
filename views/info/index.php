<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\components\Site;

$this->title = strip_tags($model->title);

Site::getInstance()->setMeta($model, __FILE__);

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="material_content col-md-9">
        <?= $model->text ?>
    </div>
    <div class="col-md-3">
        <?= $this->render("@views/site//templ/rblock", []) ?>
    </div>
</div>


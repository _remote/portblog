<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\models\WorldCountries;
use app\models\Language;
use kartik\tabs\TabsX;

/* @var $this yii\web\View */
/* @var $model app\models\WorldCities */

$this->title = $main->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('cities', 'World Cities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="world-cities-view">

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $main->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $main->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php
    $items = [];
    foreach ($models as $model) {
        $items[] = [
            'options' => ['id' => 'lng-'.$model->id_language],
            'label' => Language::getTitleOfLanguage($model->id_language),
            'content' => "<h1 style=\"margin-top: 0px;\">{$model->name}</h1>".DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'id',
                    [
                        'label' => Yii::t('cities', 'Language'),
                        'format' => 'raw',
                        'value' => !empty($model->language->name)?Html::a($model->language->name, ["/admin/language/view", "id" => $model->language->id]):false,
                    ],
                    'name',
                    'country' => [
                        'label' => Yii::t('cities', 'Country'),
                        'format' => 'raw',
                        'value' => Html::a(WorldCountries::getName($model->country), ["/admin/countries/view", "id" => $model->country])
                    ],
                    //'region',
                    'latitude',
                    'longitude',
                ],
            ]),
            'active' => ($language == Language::getTypeOfLanguage($model->id_language))?true:false,
        ];
    }
    
    echo TabsX::widget([
        'items' => $items,
        'position'=>TabsX::POS_LEFT,
        'encodeLabels'=>false,
    ]);
    ?>

</div>

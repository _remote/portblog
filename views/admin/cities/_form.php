<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\WorldCountries;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Language;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\WorldCities */
/* @var $form yii\widgets\ActiveForm */
$id_language = isset($id_language)?$id_language:false;
?>

<?= $form->field($model, "[{$id_language}]id_language")->hiddenInput()->label(false) ?>  

<?php 
//echo $form->field($model, 'id_language', [
//        'template' =>  "{label} ".Html::a('', ['admin/language/create'], ['class' => 'glyphicon glyphicon-plus action-button'])." {input}" 
//    ])->widget(Select2::classname(), [
//    'options' => [
//        'id'=>'language-id',
//        'placeholder' => 'Select a language ...'
//    ],
//    'data' => ArrayHelper::map(Language::find()->select(['name', 'id'])->orderBy("`name` DESC")->all(), 'id', 'name')
//]);
//
//$countries = WorldCountries::find()->where(["id_language" => $model->id_language])->asArray()->all();
//echo $form->field($model, "country", [
//        'template' =>  "{label} ".Html::a('', ['admin/countries/create'], ['class' => 'glyphicon glyphicon-plus action-button', 'target' => "_blank"])." {input}" 
//    ])->widget(DepDrop::classname(), [
//    'data' => @ArrayHelper::map($countries, 'id', 'name'),
//    'options'=>['id'=>"country-id"],
//    'type' => DepDrop::TYPE_SELECT2,
//    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
//    'pluginOptions'=>[
//        'depends'=>["language-id"],
//        'placeholder'=>'Select a country...',
//        'url'=>Url::to(['countrylist'])
//    ]
//]);

echo $form->field($model, "[{$id_language}]country", [
        'template' =>  "{label} ".Html::a('', ['admin/countries/create', "language" => Language::getTypeOfLanguage($model->id_language)], ['class' => 'glyphicon glyphicon-plus action-button'])." {input}" 
    ])->widget(Select2::classname(), [
    'options' => [
        'id'=>"country-{$id_language}-id",
        'placeholder' => Yii::t('cities', 'Select a country ..')
    ],
    'data' => ArrayHelper::map(WorldCountries::find()->where(["id_language" => $model->id_language])->asArray()->all(), 'id', 'name')
]);
?>

<?= $form->field($model, "[{$id_language}]name")->textInput(['maxlength' => true]) ?>

<?= $form->field($model, "[{$id_language}]latitude")->textInput(['maxlength' => true]) ?>

<?= $form->field($model, "[{$id_language}]longitude")->textInput(['maxlength' => true]) ?>


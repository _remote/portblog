<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\models\WorldCountries;
use app\models\Language;
use app\models\WorldCities;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cities', 'Cities');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="world-cities-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('cities', 'Create Cities'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
//            [
//                'attribute' => 'id_language',
//                'label' => 'Language',
//                'format' => 'raw',
//                'value' => function($model) {
//                    return !empty($model->language)?Html::a($model->language->name, ["/admin/language/view", "id" => $model->language->id]):false;
//                }
//            ],
            'name',
            'slug',
            'country' => [
                'attribute' => 'country',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a(WorldCountries::getName($model->country), ["/admin/countries/view", "id" => $model->country]);
                }
            ],
            //'region',
            'latitude' => [
                'attribute' => 'latitude',
                'value' => function($model) {
                    return round($model->latitude, 3);
                }
            ],
            'longitude' => [
                'attribute' => 'longitude',
                'value' => function($model) {
                    return round($model->longitude, 3);
                }
            ],
            [
                'attribute' => 'id_language',
                'label' => Yii::t('cities', 'Language'),
                'format' => 'raw',
                'value' => function($model) {
                    $countries = WorldCities::getLocalList($model->id);
                    $icons = [];
                    foreach ($countries as $country) {
                        $icons[] = Html::a(Html::img(Language::getIcon($country['id_language']), ["width" => 15, "title" => Language::getNameOfLanguage($country['id_language'])]), ["/admin/language/view", "id" => $country['id_language']]);
                    }
                    return implode("  ", $icons);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

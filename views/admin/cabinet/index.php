<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\models\User;
use yii\helpers\ArrayHelper;
use app\modules\blog\models\Blog;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cabinet';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Blog', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Blog subscriptions', ['/admin/blog/subscription/index'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
//            'id_user' => [
//                'attribute' => 'id_user',
//                'format' => 'raw',
//                'value' => function($model) {
//                    return User::getNameOfUser($model->id_user);
//                },
//                'filter' => ArrayHelper::map(
//                    User::find()
//                        ->orderBy('id')
//                        ->asArray()->all(),
//                    'id', 'first_name'),
//                'options' => ['width' => '120'],
//                'label' => "User"
//            ],
            'title',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model) {
                    return Blog::getStatusName($model->status);
                },
            ],
            'date_create',
            [
                'label' => 'Count posts',
                'format' => 'raw',
                'value' => function($model) {
                    $count = Blog::countOfPosts($model->id);
                    return ($count)?Html::a($count, ["/admin/blog/posts/index", "id_blog" => $model->id]):0;
                },
            ],
            // 'date_update',

//            [
//                'class' => 'yii\grid\ActionColumn',
//                'contentOptions' => ['style' => 'width:30px;'],
//                'buttons'=>[
//                    'view' => function ($url, $model) {
//                        return Html::a('<span class="glyphicon glyphicon-book"></span>', ["admin/posts/create", "id_blog" => $model->id], [
//                            'title' => Yii::t('yii', 'Создать пост'),
//                            'data-pjax'=>'0',
//                            'target' => '_blank',
//                            //'class' => 'grid-action'
//                        ]);
//                    },
//                ],
//                'template' => '{view}'
//            ],
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'header'=>'V',
//                'contentOptions' => ['style' => 'width:30px;'],
//                'template' => '{view}'
//            ],
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'header'=>'U',
//                'contentOptions' => ['style' => 'width:30px;'],
//                'template' => '{update}'
//            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'D',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{delete}'
            ],
        ],
    ]); ?>

</div>


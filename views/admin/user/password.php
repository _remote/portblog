<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('users', 'Change password for') . ' ' . $model->first_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('users', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->first_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('users', 'Change password');
?>

<div class="user-form">

    <h1><?= $this->title ?></h1>
    <?php 
    $form = ActiveForm::begin();
    ?>

    <?= $form->field($model, 'password')->passwordInput(["value" => false]) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Change'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

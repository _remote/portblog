<?php

use yii\helpers\Html;
use yii\grid\GridView;

use yii\helpers\ArrayHelper;
use app\models\Language;
use yii\jui\DatePicker;
use app\models\TranslateMessage;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('translate', 'Translate Messages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="translate-message-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'id_language' => [
                'attribute' => 'id_language',
                'format' => 'raw',
                'value' => function($model) {
                    return Language::getNameOfLanguage($model->id_language);
                },
                'filter' => ArrayHelper::map(
                    Language::find()
                        ->orderBy('id')
                        ->asArray()->all(),
                    'id', 'name'),
                'options' => ['width' => '120'],
                'label' => Yii::t('translate', "Language"),
            ],
            //'language',
            'category' => [
                'attribute' => 'category',
                'format' => 'raw',
                'value' => function($model) {
                    return $model->category;
                },
                'filter' => TranslateMessage::getCategories(),
                'options' => ['width' => '120'],
            ],
            'message',
            'translation:ntext',
            'date_updated' => [
                'attribute' => 'date_updated',
                'filter' => DatePicker::widget([
                    'name' => "TranslateMessageSearch[date_updated]",
                    'language' => 'ru', 
                    'dateFormat' => 'yyyy-MM-dd',
                    'value' => $searchModel->date_updated
                ]),
                'format' => 'html',
                'options' => ['width' => '120'], 
            ],
                    
            'status' => [
                'attribute' => 'status',
                'filter' => TranslateMessage::getStatusArray(),
                'format' => 'raw',
                'value' => function($model) {
                    $statuses = TranslateMessage::getStatusArray();
                    return $statuses[$model->status];
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'V',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{view}'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'U',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{update}'
            ],
                [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'D',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{delete}'
            ],
        ],
    ]); ?>

</div>

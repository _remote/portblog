<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\models\WorldCountries;
use app\models\Language;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('countries', 'Countries');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="world-countries-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('countries', 'Create Countries'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            
            'name',
            'code',
            //'alias',
            'lat' => [
                'attribute' => 'lat',
                'value' => function($model) {
                    return round($model->lat, 3);
                }
            ],
            'lng' => [
                'attribute' => 'lng',
                'value' => function($model) {
                    return round($model->lng, 3);
                }
            ],
            [
                'attribute' => 'id_language',
                'label' => Yii::t('countries', 'Language'),
                'format' => 'raw',
                'value' => function($model) {
                    $countries = WorldCountries::getLocalList($model->id);
                    $icons = [];
                    foreach ($countries as $country) {
                        $icons[] = Html::a(Html::img(Language::getIcon($country['id_language']), ["width" => 15, "title" => Language::getNameOfLanguage($country['id_language'])]), ["/admin/language/view", "id" => $country['id_language']]);
                    }
                    return implode("  ", $icons);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'V',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{view}'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'U',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{update}'
            ],
                [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'D',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{delete}'
            ],
        ],
    ]); ?>

</div>

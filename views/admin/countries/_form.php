<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use app\models\Language;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\WorldCountries */
/* @var $form yii\widgets\ActiveForm */
$id_language = isset($id_language)?$id_language:false;
?>

<?= $form->field($model, "[{$id_language}]id_language")->hiddenInput()->label(false) ?>

<?= $form->field($model, "[{$id_language}]name")->textInput(['maxlength' => true]) ?>

<?= $form->field($model, "[{$id_language}]code")->textInput(['maxlength' => true]) ?>

<?= $form->field($model, "[{$id_language}]lat")->textInput(['maxlength' => true]) ?>

<?= $form->field($model, "[{$id_language}]lng")->textInput(['maxlength' => true]) ?>


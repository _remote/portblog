<?php

use yii\helpers\Html;

use app\models\Language;
use kartik\tabs\TabsX;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\WorldCountries */

$this->title = Yii::t('countries', 'Create World Countries');
$this->params['breadcrumbs'][] = ['label' => Yii::t('countries', 'World Countries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="world-countries-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="world-countries-form">
   
        <?php 
        $form = ActiveForm::begin([
            'enableClientValidation' => false,
        ]);
        ?>

        <?php
        $items = [];
        foreach ($languages as $id => $type) {
            $items[] = [
                'options' => ['id' => 'lng-'.$id],
                'label' => Language::getTitleOfLanguage($id),
                'content' => $this->render('_form', [
                    'model' => $models[$id],
                    'id_language' => $id,
                    'form' => $form
                ]),
                'active' => ($language == $type)?true:false
            ];
        }
        echo TabsX::widget([
            'items' => $items,
            'position'=>TabsX::POS_ABOVE,
            'encodeLabels'=>false,
        ]);
        ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('common', 'Create') , ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>

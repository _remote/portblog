<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Callback */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="callback-form">

    <?php 
    $form = ActiveForm::begin(); 
    ?>

    <?= $form->field($model, 'session')->textInput(['maxlength' => true, "disabled" => "disabled"]) ?>

    <?php $form->field($model, 'time')->textInput() ?>

    <?= $form->field($model, 'autor_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'autor_email')->textInput(['maxlength' => true]) ?>

    <?php $form->field($model, 'autor_telephone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

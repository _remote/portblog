<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('language', 'Languages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('language', 'Create Language'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'icon',
                'format' => 'raw',
                'value' => function($model) {
                    return ($model->icon)?Html::img($model->icon):false;
                }
            ],
            'name',
            'type',
            'is_default',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

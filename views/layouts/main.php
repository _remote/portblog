<?php
use yii\helpers\Html;
use yii\bootstrap\NavBar;
use kartik\nav\NavX;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\BootboxAsset;

use kartik\sidenav\SideNav;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
BootboxAsset::overrideSystemConfirm();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'Portblog.ru',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            echo NavX::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => Yii::t("menu", 'Home'), 'url' => ['/admin/index']],
                    Yii::$app->user->isGuest ?
                        ['label' => Yii::t("menu", 'Login'), 'url' => ['/admin/login']] :
                        ['label' => Yii::t("menu", 'Logout').' (' . Yii::$app->user->identity->username . ')',
                            'url' => ['/admin/logout'],
                            'linkOptions' => ['data-method' => 'post']],
                ],
            ]);
            NavBar::end();
        ?>

        <div class="container">
            <?php if (!Yii::$app->user->isGuest) { ?>
            <div class="col-md-2">
                <?php
                $item = "index";
                // ['label' => '<span class="pull-right badge">10</span> New Arrivals', 'url' => Url::to(['/site/new-arrivals', 'type'=>$type]), 'active' => ($item == 'new-arrivals')],
                
                if (Yii::$app->user->can("admin/news")) {
                    echo SideNav::widget([
                        'encodeLabels' => false,
                        'heading' => Yii::t("menu", "News"),
                        'items' => [
                            ['label' => Yii::t("menu", 'News list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/news/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Create new'), 'icon' => 'plus', 'url' => Url::to(['/admin/news/create']), 'active' => false],
                            ['label' => Yii::t("menu", 'Comments list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/news/comments/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Add subscription'), 'icon' => 'plus', 'url' => Url::to(['/admin/news/subscription/index']), 'active' => false],
                            ['label' => Yii::t("menu", 'Section menu'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/news/menu']), 'active' => false],
                            ['label' => Yii::t("menu", 'Tags list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/news/tags/index', 'Tags[material]' => 'news']), 'active' => false],
                        ],
                    ]); 
                }
                
                if (Yii::$app->user->can("admin/articles")) {
                    echo SideNav::widget([
                        'encodeLabels' => false,
                        'heading' => Yii::t("menu", "Articles"),
                        'items' => [
                            ['label' => Yii::t("menu", 'Articles list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/articles/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Create article'), 'icon' => 'plus', 'url' => Url::to(['/admin/articles/create']), 'active' => false],
                            ['label' => Yii::t("menu", 'Comments list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/articles/comments/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Section menu'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/articles/menu']), 'active' => false],
                            ['label' => Yii::t("menu", 'Tags list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/articles/tags/index', 'Tags[material]' => 'articles']), 'active' => false],
                        ],
                    ]); 
                }
                
                if (Yii::$app->user->can("admin/blogs")) {
                    echo SideNav::widget([
                        'encodeLabels' => false,
                        'heading' => Yii::t("menu", "Blogs"),
                        'items' => [
                            ['label' => Yii::t("menu", 'Posts list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/blogs/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Create post'), 'icon' => 'plus', 'url' => Url::to(['/admin/blogs/create']), 'active' => false],
                            ['label' => Yii::t("menu", 'Comments list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/blogs/comments/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Add subscription'), 'icon' => 'plus', 'url' => Url::to(['/admin/blogs/subscription/index']), 'active' => false],
                            ['label' => Yii::t("menu", 'Section menu'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/blogs/menu']), 'active' => false],
                            ['label' => Yii::t("menu", 'Tags list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/blogs/tags/index', 'Tags[material]' => 'blogs']), 'active' => false],
                        ],
                    ]); 
                }
                
                if (Yii::$app->user->can("admin/events")) {
                    echo SideNav::widget([
                        'encodeLabels' => false,
                        'heading' => Yii::t("menu", "Events"),
                        'items' => [
                            ['label' => Yii::t("menu", 'Events list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/events/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Create event'), 'icon' => 'plus', 'url' => Url::to(['/admin/events/create']), 'active' => false],
                            ['label' => Yii::t("menu", 'Comments list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/events/comments/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Section menu'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/events/menu']), 'active' => false],
                            ['label' => Yii::t("menu", 'Tags list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/events/tags/index', 'Tags[material]' => 'events']), 'active' => false],
                        ],
                    ]); 
                }
                
                if (Yii::$app->user->can("admin/places")) {
                    echo SideNav::widget([
                        'encodeLabels' => false,
                        'heading' => Yii::t("menu", "Places"),
                        'items' => [
                            ['label' => Yii::t("menu", 'Places list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/places/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Add place'), 'icon' => 'plus', 'url' => Url::to(['/admin/places/create']), 'active' => false],
                            ['label' => Yii::t("menu", 'Comments list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/places/comments/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Section menu'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/places/menu']), 'active' => false],
                            ['label' => Yii::t("menu", 'Tags list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/places/tags/index', 'Tags[material]' => 'places']), 'active' => false],
                        ],
                    ]); 
                }
                
                if (Yii::$app->user->can("admin/persons")) {
                    echo SideNav::widget([
                        'encodeLabels' => false,
                        'heading' => Yii::t("menu", "Persons"),
                        'items' => [
                            ['label' => Yii::t("menu", 'Persons list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/persons/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Add person'), 'icon' => 'plus', 'url' => Url::to(['/admin/persons/create']), 'active' => false],
                            ['label' => Yii::t("menu", 'Comments list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/persons/comments/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Section menu'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/persons/menu']), 'active' => false],
                            ['label' => Yii::t("menu", 'Tags list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/persons/tags/index', 'Tags[material]' => 'persons']), 'active' => false],
                        ],
                    ]); 
                }
                
                if (Yii::$app->user->can("admin/company")) {
                    echo SideNav::widget([
                        'encodeLabels' => false,
                        'heading' => Yii::t("menu", "Companies"),
                        'items' => [
                            ['label' => Yii::t("menu", 'Companies list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/company/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Add company'), 'icon' => 'plus', 'url' => Url::to(['/admin/company/create']), 'active' => false],
                            ['label' => Yii::t("menu", 'Comments list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/company/comments/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Section menu'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/company/menu']), 'active' => false],
                            ['label' => Yii::t("menu", 'Tags list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/company/tags/index', 'Tags[material]' => 'company']), 'active' => false],
                        ],
                    ]); 
                }
                
                
                
                
                
                if (Yii::$app->user->can("admin/language")) {
                    echo SideNav::widget([
                        'encodeLabels' => false,
                        'heading' => Yii::t("menu", "Localisation"),
                        'items' => [
                            ['label' => Yii::t("menu", 'Languages'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/language/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Add language'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/language/create']), 'active' => false],
                            ['label' => Yii::t("menu", 'Translate'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/translate/index']), 'active' => false],
                            ['label' => Yii::t("menu", 'Countries list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/countries/index']), 'active' => false],
                            ['label' => Yii::t("menu", 'Cities list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/cities/index']), 'active' => false],
                            
                        ],
                    ]); 
                }
                
                
                
                if (Yii::$app->user->can("admin/user/index") | Yii::$app->user->can("permit/access")) {
                    echo SideNav::widget([
                        'encodeLabels' => false,
                        'heading' => Yii::t("menu", "Users"),
                        'items' => [
                            ['label' => Yii::t("menu", 'Users list'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/user/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Permitions control'), 'icon' => 'list-alt', 'url' => Url::to(['/permit/default/index']), 'active' => false],
                            ['label' => Yii::t("menu", 'Roles control'), 'icon' => 'list-alt', 'url' => Url::to(['/permit/access/role']), 'active' => false],
                            ['label' => Yii::t("menu", 'Rules'), 'icon' => 'list-alt', 'url' => Url::to(['/permit/access/permission']), 'active' => false],
                            ['label' => Yii::t("menu", 'Add rule'), 'icon' => 'plus', 'url' => Url::to(['/permit/access/add-permission']), 'active' => false],
                            ['label' => Yii::t("menu", 'Users callback'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/callback/index']), 'active' => false],
                        ],
                    ]); 
                }
                
                if (Yii::$app->user->can("admin/icons")) {
                    echo SideNav::widget([
                        'encodeLabels' => false,
                        'heading' => Yii::t("menu", "Icons"),
                        'items' => [
                            ['label' => Yii::t("menu", 'Materials icons'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/icons/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Add icon'), 'icon' => 'plus', 'url' => Url::to(['/admin/icons/create']), 'active' => false],
                        ],
                    ]); 
                }
                
                if (Yii::$app->user->can("admin/settings")) {
                    echo SideNav::widget([
                        'encodeLabels' => false,
                        'heading' => Yii::t("menu", "Settings"),
                        'items' => [
                            ['label' => Yii::t("menu", 'Site settings'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/settings/index', 'top' => 1]), 'active' => false],
                            ['label' => Yii::t("menu", 'Mails settings'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/settings/mail/index']), 'active' => false],
                            ['label' => Yii::t("menu", 'Mail templates'), 'icon' => 'list-alt', 'url' => Url::to(['/admin/settings/mail-templates/index']), 'active' => false],
                        ],
                    ]); 
                }
                ?>
            </div>
            <div class="col-md-10">
            <?php } else {
                echo '<div>';
            }
            ?>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= $content ?>
            </div>
        </div>
    </div>
    
    <?= Yii::$app->upload->form(); ?>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; Portblog.ru <?= date('Y') ?></p>
            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

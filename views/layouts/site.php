<?php
use \Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\SiteAsset;
use app\assets\BootboxAsset;
use yii\bootstrap\Modal;

use kartik\sidenav\SideNav;
use kartik\nav\NavX;

use app\components\Site;
use app\models\User;

use app\modules\material\models\Material;

/* @var $this \yii\web\View */
/* @var $content string */

SiteAsset::register($this);
BootboxAsset::overrideSystemConfirm();

$meta = Site::getInstance()->getMeta();
$this->title = $meta->title;
$show_banner = true;

$menu = $this->render("@views/site/templ/menu", [
    "selected" => @$this->params['selected'],
    "banner" => $show_banner,
]);

$template = Site::getInstance()->getTemplate();
$javascript = Site::getInstance()->getJavaScript();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta property="og:title" content="<?= $meta->title ?>" />
    <meta property="og:description" content="<?= $meta->description ?>" />
    <meta property="og:type" content="<?= @$this->params['selected'] ?>" />
    <meta property="og:url" content="<?= $meta->site ?>" />
    <meta property="og:image" content="<?= $meta->image ?>" />
    
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body <?= ($template && $template == Material::TEMPLATE_WITHOUT_WIDGETS)?"class=\"personal\"":"" ?>>
<?php $this->beginBody() ?>
<?php if ($template && $template == Material::TEMPLATE_WITHOUT_WIDGETS) {?>    
    <div class="back_for_body"><?= Html::img($meta->image) ?></div>
<?php } ?>
<div id="social_init">
<?= $this->render("@views/site/templ/social_init") ?>
</div>
<div class="wrap pc">
    <div class="content">
        <?= $menu ?>
    </div>
    <div class="content height">
        <div id="lenta" class="body" style="<?= $show_banner?"margin-top: 40px;":"padding-top: 60px;" ?>">   
            <?php 
            echo $this->render("@views/site/modules/meta", [
                "meta" => $meta,
                "selected" => @$this->params['selected'],
            ]) 
            ?>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>
    <script>
        $.ajaxSetup({
            data: <?= \yii\helpers\Json::encode([
                Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
            ]) ?>
        });
    </script>
    <?php
    if (Yii::$app->user->isGuest) {
        Modal::begin([
            'id' => 'form-auth-modal',
            'header' => '',
            'clientEvents' => [
                'show.bs.modal' => new \yii\web\JsExpression("function(event){
                    var button = $(event.relatedTarget);
                    //var id_comment = button.data('id_comment');
                    //var title = button.data('title');
                    var modal = $(this);
                    //modal.find('#modal-header').text('Ответ на \"' + title + '\"');
                    //modal.find('#modal-id_comment').val(id_comment);
                }"),
            ]
        ]);

        echo $this->render("@views/site/templ/_auth", []);
    //    $this->registerJs(
    //       '$("document").ready(function(){ 
    //            $("#_send_comment").on("pjax:end", function() {
    //                $("#commentForm").modal("hide");
    //                $.pjax.reload({container:"#_list_message"});
    //            });
    //        });'
    //    );

        Modal::end();
    }
    ?>
    <?= Yii::$app->upload->form(); ?>

</div>

<div class="footer">
    <div class="prefooter">
        <div class="content">
            <?php 
            if (!in_array(@$this->params['selected'], ["blogs"])) {
                echo $this->render("@views/site/templ/prefooter", [
                    "items" => @$this->params['items'],
                ]);
            }
            ?>
        </div>
    </div>
    <div class="content">
        <?php 
        echo $this->render("@views/site/templ/footer", [
            "selected" => @$this->params['selected'],
        ]);
        ?>
    </div>
</div>
    <script type="text/javascript">
        <?= $javascript ?>
    </script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

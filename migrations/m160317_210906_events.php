<?php

use yii\db\Migration;

class m160317_210906_events extends Migration {

    public function up() {
        $this->execute("
            ALTER TABLE  `events` ADD  `telephone` VARCHAR( 256 ) NULL AFTER  `longitude` ,
                ADD  `work_time` VARCHAR( 256 ) NULL AFTER  `telephone` ,
                ADD  `price` VARCHAR( 256 ) NULL AFTER  `work_time` ;
        ");
    }

    public function down() {
        $this->execute("
            ALTER TABLE `events`
                DROP `telephone`,
                DROP `work_time`,
                DROP `price`;
        ");
    }

}

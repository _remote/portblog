<?php

use yii\db\Migration;

class m160514_180138_settings_footer extends Migration {

    public function up() {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `settings_footer` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `id_language` int(11) DEFAULT NULL,
                `id_main` int(11) DEFAULT NULL,
                `slug` varchar(255) DEFAULT NULL,
                `title` varchar(100) DEFAULT NULL,
                `text` text,
                `url` varchar(255) DEFAULT NULL,
                `icon` varchar(255) DEFAULT NULL,
                `position` int(11) DEFAULT NULL,
                `in_new` int(1) DEFAULT NULL,
                `seo_title` varchar(255) NOT NULL,
                `seo_keywords` varchar(255) NOT NULL,
                `seo_description` varchar(255) NOT NULL,
                `seo_other` varchar(255) NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

            ALTER TABLE  `settings` ADD  `footer` VARCHAR( 512 ) NULL ;
        ");
        
    }

    public function down() {
        $this->execute("
            DROP TABLE settings_footer;
            ALTER TABLE `settings` DROP `footer`;
        ");
    }

}

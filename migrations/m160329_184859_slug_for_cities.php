<?php

use yii\db\Migration;

class m160329_184859_slug_for_cities extends Migration
{
    public function up() {
        $this->execute("
            ALTER TABLE  `world_cities` ADD  `slug` VARCHAR( 255 ) NULL AFTER  `name` ;
            ALTER TABLE  `world_countries` ADD  `slug` VARCHAR( 255 ) NULL AFTER  `name` ;
        ");
    }

    public function down() {
        $this->execute("
            ALTER TABLE `world_cities` DROP `slug`;
            ALTER TABLE `world_countries` DROP `slug`;
        ");
    }
}

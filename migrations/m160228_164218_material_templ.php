<?php

use yii\db\Schema;
use yii\db\Migration;

class m160228_164218_material_templ extends Migration {

    public function up() {
        $this->execute("
            ALTER TABLE  `articles` ADD  `template` INT NULL ; 
            ALTER TABLE  `blogs` ADD  `template` INT NULL ;
            ALTER TABLE  `company` ADD  `template` INT NULL ;
            ALTER TABLE  `events` ADD  `template` INT NULL ;
            ALTER TABLE  `news` ADD  `template` INT NULL ;
            ALTER TABLE  `persons` ADD  `template` INT NULL ;
            ALTER TABLE  `places` ADD  `template` INT NULL ;
        ");
        
    }

    public function down() {
        $this->execute("
            ALTER TABLE `articles` DROP `template`;
            ALTER TABLE `blogs` DROP `template`;
            ALTER TABLE `company` DROP `template`;
            ALTER TABLE `events` DROP `template`;
            ALTER TABLE `news` DROP `template`;
            ALTER TABLE `persons` DROP `template`;
            ALTER TABLE `places` DROP `template`;
        ");
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}

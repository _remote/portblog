<?php

use yii\db\Schema;
use yii\db\Migration;

class m160225_181528_images extends Migration {

    public function up() {
        $this->execute("
            ALTER TABLE  `images` ADD  `id_user` INT( 11 ) NULL AFTER  `id`, 
                ADD  `is_active` INT( 1 ) NULL DEFAULT  '0',
                ADD  `is_delete` INT( 1 ) NULL DEFAULT  '0';
            UPDATE `images` SET `id_user` = 1, `is_active` = 1 WHERE 1;
            UPDATE `images` SET `id_user` = 45 WHERE `id` > 20 LIMIT 7;
            

            ALTER TABLE  `articles_images` ADD  `id_user` INT( 11 ) NULL AFTER  `id`,
                ADD  `is_delete` INT( 1 ) NULL DEFAULT  '0' ;
                
            ALTER TABLE  `blogs_images` ADD  `id_user` INT( 11 ) NULL AFTER  `id`,
                ADD  `is_delete` INT( 1 ) NULL DEFAULT  '0' ;
                
            ALTER TABLE  `company_images` ADD  `id_user` INT( 11 ) NULL AFTER  `id`,
                ADD  `is_delete` INT( 1 ) NULL DEFAULT  '0' ;
                
            ALTER TABLE  `events_images` ADD  `id_user` INT( 11 ) NULL AFTER  `id`,
                ADD  `is_delete` INT( 1 ) NULL DEFAULT  '0' ;
                
            ALTER TABLE  `news_images` ADD  `id_user` INT( 11 ) NULL AFTER  `id`,
                ADD  `is_delete` INT( 1 ) NULL DEFAULT  '0' ;
                
            ALTER TABLE  `persons_images` ADD  `id_user` INT( 11 ) NULL AFTER  `id`,
                ADD  `is_delete` INT( 1 ) NULL DEFAULT  '0' ;
                
            ALTER TABLE  `places_images` ADD  `id_user` INT( 11 ) NULL AFTER  `id`,
                ADD  `is_delete` INT( 1 ) NULL DEFAULT  '0' ;

            UPDATE `news_images` SET `id_user` = 1 WHERE 1;
            UPDATE `news_images` SET `id_user` = 45 WHERE 1 LIMIT 5;
            UPDATE `blogs_images` SET `id_user` = 1 WHERE 1;
        ");
        
    }

    public function down() {
        $this->execute("
            ALTER TABLE `images` DROP `id_user`,
                DROP `is_active`,
                DROP `is_delete`;
            ALTER TABLE `articles_images` DROP `id_user`,
                DROP `is_delete`;
            ALTER TABLE `blogs_images` DROP `id_user`,
                DROP `is_delete`;
            ALTER TABLE `company_images` DROP `id_user`,
                DROP `is_delete`;
            ALTER TABLE `events_images` DROP `id_user`,
                DROP `is_delete`;
            ALTER TABLE `news_images` DROP `id_user`,
                DROP `is_delete`;
            ALTER TABLE `persons_images` DROP `id_user`,
                DROP `is_delete`;
            ALTER TABLE `places_images` DROP `id_user`,
                DROP `is_delete`;
        ");
    }
    
}

<?php

use yii\db\Migration;

class m160326_195410_js extends Migration {

    public function up() {
        $this->execute("
            ALTER TABLE  `articles` ADD  `js` TEXT NULL AFTER  `video` ;
            ALTER TABLE  `blogs` ADD  `js` TEXT NULL AFTER  `video` ;
            ALTER TABLE  `company` ADD  `js` TEXT NULL AFTER  `video` ;
            ALTER TABLE  `events` ADD  `js` TEXT NULL AFTER  `video` ;
            ALTER TABLE  `news` ADD  `js` TEXT NULL AFTER  `video` ;
            ALTER TABLE  `persons` ADD  `js` TEXT NULL AFTER  `video` ;
            ALTER TABLE  `places` ADD  `js` TEXT NULL AFTER  `video` ;
        ");
    }

    public function down() {
        $this->execute("
            ALTER TABLE `articles` DROP `js`;
            ALTER TABLE `blogs` DROP `js`;
            ALTER TABLE `company` DROP `js`;
            ALTER TABLE `events` DROP `js`;
            ALTER TABLE `news` DROP `js`;
            ALTER TABLE `persons` DROP `js`;
            ALTER TABLE `places` DROP `js`;
        ");
    }

}

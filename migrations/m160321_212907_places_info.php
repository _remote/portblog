<?php

use yii\db\Migration;

class m160321_212907_places_info extends Migration
{
    public function up() {
        $this->execute("
            ALTER TABLE  `places` ADD  `telephone` VARCHAR( 256 ) NULL AFTER  `longitude` ,
                ADD  `work_time` VARCHAR( 256 ) NULL AFTER  `telephone` ,
                ADD  `price` VARCHAR( 256 ) NULL AFTER  `work_time` ;
        ");
    }

    public function down() {
        $this->execute("
            ALTER TABLE `places`
                DROP `telephone`,
                DROP `work_time`,
                DROP `price`;
        ");
    }
}

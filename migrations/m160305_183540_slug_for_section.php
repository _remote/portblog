<?php

use yii\db\Migration;

class m160305_183540_slug_for_section extends Migration {

    public function up() {
        $this->execute("
            ALTER TABLE  `articles_section` ADD  `slug` VARCHAR( 255 ) NULL AFTER  `name` ;
            ALTER TABLE  `articles_subsection` ADD  `slug` VARCHAR( 255 ) NULL AFTER  `name` ;
            
            ALTER TABLE  `blogs_section` ADD  `slug` VARCHAR( 255 ) NULL AFTER  `name` ;
            ALTER TABLE  `blogs_subsection` ADD  `slug` VARCHAR( 255 ) NULL AFTER  `name` ;
            
            ALTER TABLE  `company_section` ADD  `slug` VARCHAR( 255 ) NULL AFTER  `name` ;
            ALTER TABLE  `company_subsection` ADD  `slug` VARCHAR( 255 ) NULL AFTER  `name` ;
            
            ALTER TABLE  `events_section` ADD  `slug` VARCHAR( 255 ) NULL AFTER  `name` ;
            ALTER TABLE  `events_subsection` ADD  `slug` VARCHAR( 255 ) NULL AFTER  `name` ;
            
            ALTER TABLE  `news_section` ADD  `slug` VARCHAR( 255 ) NULL AFTER  `name` ;
            ALTER TABLE  `news_subsection` ADD  `slug` VARCHAR( 255 ) NULL AFTER  `name` ;
            
            ALTER TABLE  `persons_section` ADD  `slug` VARCHAR( 255 ) NULL AFTER  `name` ;
            ALTER TABLE  `persons_subsection` ADD  `slug` VARCHAR( 255 ) NULL AFTER  `name` ;
            
            ALTER TABLE  `places_section` ADD  `slug` VARCHAR( 255 ) NULL AFTER  `name` ;
            ALTER TABLE  `places_subsection` ADD  `slug` VARCHAR( 255 ) NULL AFTER  `name` ;
        ");
        
    }

    public function down() {
        $this->execute("
            ALTER TABLE  `articles_section` DROP `slug`;
            ALTER TABLE  `articles_subsection` DROP `slug`;
            
            ALTER TABLE  `blogs_section` DROP `slug`;
            ALTER TABLE  `blogs_subsection` DROP `slug`;
            
            ALTER TABLE  `company_section` DROP `slug`;
            ALTER TABLE  `company_subsection` DROP `slug`;
            
            ALTER TABLE  `events_section` DROP `slug`;
            ALTER TABLE  `events_subsection` DROP `slug`;
            
            ALTER TABLE  `news_section` DROP `slug`;
            ALTER TABLE  `news_subsection` DROP `slug`;
            
            ALTER TABLE  `persons_section` DROP `slug`;
            ALTER TABLE  `persons_subsection` DROP `slug`;
            
            ALTER TABLE  `places_section` DROP `slug`;
            ALTER TABLE  `places_subsection` DROP `slug`;
        ");
    }
}

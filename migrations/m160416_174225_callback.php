<?php

use yii\db\Migration;

class m160416_174225_callback extends Migration {

    public function up() {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `callback` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `session` varchar(32) NOT NULL,
                `time` int(11) NOT NULL DEFAULT 0,
                `autor_name` varchar(50) DEFAULT NULL,
                `autor_email` varchar(100) DEFAULT NULL,
                `autor_telephone` varchar(50) DEFAULT NULL,
                `text` varchar(1000) DEFAULT NULL,
                `status` int(1) NOT NULL,
                PRIMARY KEY (`id`),
                KEY `session` (`session`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
    }

    public function down() {
        $this->execute("
            DROP TABLE `callback`;
        ");
    }
}

--
-- Структура таблицы `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_main` int(11) DEFAULT NULL COMMENT 'Принадлежность к переводу новости',
  `id_area` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `id_language` int(11) DEFAULT NULL,
  `title` varchar(1000) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `id_subsection` int(11) DEFAULT NULL,
  `short_text` text,
  `text` text NOT NULL,
  `id_icon` int(11) DEFAULT NULL,
  `image_file` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_public` date DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL,
  `seo_title` varchar(512) DEFAULT NULL,
  `seo_description` varchar(512) DEFAULT NULL,
  `seo_keywords` varchar(512) DEFAULT NULL,
  `view_count` int(11) NOT NULL,
  `comments_count` int(11) DEFAULT '0',
  `rating` float(3,1) DEFAULT '0.0',
  `rating_voting_count` int(11) DEFAULT '0',
  `is_show` int(1) NOT NULL,
  `is_show_on_main` int(1) NOT NULL COMMENT 'Отображать на главной',
  `is_show_in_top` int(1) NOT NULL,
  `is_selected` int(1) DEFAULT NULL,
  `gallery` int(1) DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `comment` varchar(512) DEFAULT NULL COMMENT 'для редактора',
  `hash_tag` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `id_type` (`id_section`),
  KEY `id_sub_type` (`id_subsection`),
  KEY `id_language` (`id_language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `articles_comments`
--

CREATE TABLE IF NOT EXISTS `articles_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` int(11) NOT NULL,
  `id_comment` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `text` text NOT NULL,
  `date_created` datetime NOT NULL,
  `is_show` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `articles_images`
--

CREATE TABLE IF NOT EXISTS `articles_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `ext` varchar(10) DEFAULT NULL,
  `position` int(4) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `articles_section`
--

CREATE TABLE IF NOT EXISTS `articles_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_site` varchar(255) DEFAULT NULL,
  `seo_other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_language` (`id_language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `articles_subscriptions`
--

CREATE TABLE IF NOT EXISTS `articles_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_material` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `id_subsection` int(11) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_city` (`id_user`,`id_city`),
  UNIQUE KEY `_section` (`id_user`,`id_section`),
  UNIQUE KEY `_sub_section` (`id_user`,`id_subsection`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `articles_subsection`
--

CREATE TABLE IF NOT EXISTS `articles_subsection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_site` varchar(255) DEFAULT NULL,
  `seo_other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_language` (`id_language`),
  KEY `id_section` (`id_section`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `blogs`
--

CREATE TABLE IF NOT EXISTS `blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_main` int(11) DEFAULT NULL COMMENT 'Принадлежность к переводу материала',
  `id_area` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `id_language` int(11) DEFAULT NULL,
  `title` varchar(1000) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `id_subsection` int(11) DEFAULT NULL,
  `short_text` text,
  `text` text NOT NULL,
  `id_icon` int(11) DEFAULT NULL,
  `image_file` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_public` date DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL,
  `seo_title` varchar(512) DEFAULT NULL,
  `seo_description` varchar(512) DEFAULT NULL,
  `seo_keywords` varchar(512) DEFAULT NULL,
  `view_count` int(11) NOT NULL,
  `comments_count` int(11) DEFAULT '0',
  `rating` float(3,1) DEFAULT '0.0',
  `rating_voting_count` int(11) DEFAULT '0',
  `is_show` int(1) NOT NULL,
  `is_show_on_main` int(1) NOT NULL COMMENT 'Отображать на главной',
  `is_show_in_top` int(1) NOT NULL,
  `is_selected` int(1) DEFAULT NULL,
  `gallery` int(1) DEFAULT NULL,
  `adress` varchar(512) DEFAULT NULL,
  `latitude` decimal(20,17) DEFAULT NULL,
  `longitude` decimal(20,17) DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `comment` varchar(512) DEFAULT NULL COMMENT 'для редактора',
  `hash_tag` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `id_type` (`id_section`),
  KEY `id_sub_type` (`id_subsection`),
  KEY `id_language` (`id_language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `blogs_comments`
--

CREATE TABLE IF NOT EXISTS `blogs_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` int(11) NOT NULL,
  `id_comment` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `text` text NOT NULL,
  `date_created` datetime NOT NULL,
  `is_show` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `blogs_images`
--

CREATE TABLE IF NOT EXISTS `blogs_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `ext` varchar(10) DEFAULT NULL,
  `position` int(4) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `blogs_section`
--

CREATE TABLE IF NOT EXISTS `blogs_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_site` varchar(255) DEFAULT NULL,
  `seo_other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_language` (`id_language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `blogs_subscriptions`
--

CREATE TABLE IF NOT EXISTS `blogs_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_material` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `id_subsection` int(11) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_city` (`id_user`,`id_city`),
  UNIQUE KEY `_section` (`id_user`,`id_section`),
  UNIQUE KEY `_sub_section` (`id_user`,`id_subsection`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `blogs_subsection`
--

CREATE TABLE IF NOT EXISTS `blogs_subsection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_site` varchar(255) DEFAULT NULL,
  `seo_other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_language` (`id_language`),
  KEY `id_section` (`id_section`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pays` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_main` int(11) DEFAULT NULL COMMENT 'Принадлежность к переводу новости',
  `id_area` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `id_language` int(11) DEFAULT NULL,
  `title` varchar(1000) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `id_subsection` int(11) DEFAULT NULL,
  `short_text` text,
  `text` text NOT NULL,
  `id_icon` int(11) DEFAULT NULL,
  `image_file` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_public` date DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL,
  `seo_title` varchar(512) DEFAULT NULL,
  `seo_description` varchar(512) DEFAULT NULL,
  `seo_keywords` varchar(512) DEFAULT NULL,
  `view_count` int(11) NOT NULL,
  `comments_count` int(11) DEFAULT '0',
  `rating` float(3,1) DEFAULT '0.0',
  `rating_voting_count` int(11) DEFAULT '0',
  `is_show` int(1) NOT NULL,
  `is_show_on_main` int(1) NOT NULL COMMENT 'Отображать на главной',
  `is_show_in_top` int(1) NOT NULL,
  `is_selected` int(1) DEFAULT NULL,
  `gallery` int(1) DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `adress` varchar(512) DEFAULT NULL,
  `latitude` decimal(20,17) DEFAULT NULL,
  `longitude` decimal(20,17) DEFAULT NULL,
  `comment` varchar(512) DEFAULT NULL COMMENT 'для редактора',
  `hash_tag` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `id_type` (`id_section`),
  KEY `id_sub_type` (`id_subsection`),
  KEY `id_language` (`id_language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `company_comments`
--

CREATE TABLE IF NOT EXISTS `company_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` int(11) NOT NULL,
  `id_comment` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `text` text NOT NULL,
  `date_created` datetime NOT NULL,
  `is_show` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `company_images`
--

CREATE TABLE IF NOT EXISTS `company_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `ext` varchar(10) DEFAULT NULL,
  `position` int(4) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `company_section`
--

CREATE TABLE IF NOT EXISTS `company_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_site` varchar(255) DEFAULT NULL,
  `seo_other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_language` (`id_language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `company_subscriptions`
--

CREATE TABLE IF NOT EXISTS `company_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_material` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `id_subsection` int(11) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_city` (`id_user`,`id_city`),
  UNIQUE KEY `_section` (`id_user`,`id_section`),
  UNIQUE KEY `_sub_section` (`id_user`,`id_subsection`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `company_subsection`
--

CREATE TABLE IF NOT EXISTS `company_subsection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_site` varchar(255) DEFAULT NULL,
  `seo_other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_language` (`id_language`),
  KEY `id_section` (`id_section`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_main` int(11) DEFAULT NULL COMMENT 'Принадлежность к переводу новости',
  `id_area` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `id_language` int(11) DEFAULT NULL,
  `title` varchar(1000) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `id_subsection` int(11) DEFAULT NULL,
  `short_text` text,
  `text` text NOT NULL,
  `id_icon` int(11) DEFAULT NULL,
  `image_file` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_public` date DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL,
  `seo_title` varchar(512) DEFAULT NULL,
  `seo_description` varchar(512) DEFAULT NULL,
  `seo_keywords` varchar(512) DEFAULT NULL,
  `view_count` int(11) NOT NULL,
  `comments_count` int(11) DEFAULT '0',
  `rating` float(3,1) DEFAULT '0.0',
  `rating_voting_count` int(11) DEFAULT '0',
  `is_show` int(1) NOT NULL,
  `is_show_on_main` int(1) NOT NULL COMMENT 'Отображать на главной',
  `is_show_in_top` int(1) NOT NULL,
  `is_selected` int(1) DEFAULT NULL,
  `gallery` int(1) DEFAULT NULL,
  `adress` varchar(512) DEFAULT NULL,
  `latitude` decimal(20,17) DEFAULT NULL,
  `longitude` decimal(20,17) DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `comment` varchar(512) DEFAULT NULL COMMENT 'для редактора',
  `hash_tag` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `id_type` (`id_section`),
  KEY `id_sub_type` (`id_subsection`),
  KEY `id_language` (`id_language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `events_comments`
--

CREATE TABLE IF NOT EXISTS `events_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` int(11) NOT NULL,
  `id_comment` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `text` text NOT NULL,
  `date_created` datetime NOT NULL,
  `is_show` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `events_images`
--

CREATE TABLE IF NOT EXISTS `events_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `ext` varchar(10) DEFAULT NULL,
  `position` int(4) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `events_section`
--

CREATE TABLE IF NOT EXISTS `events_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_site` varchar(255) DEFAULT NULL,
  `seo_other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_language` (`id_language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `events_subscriptions`
--

CREATE TABLE IF NOT EXISTS `events_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_material` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `id_subsection` int(11) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_city` (`id_user`,`id_city`),
  UNIQUE KEY `_section` (`id_user`,`id_section`),
  UNIQUE KEY `_sub_section` (`id_user`,`id_subsection`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `events_subsection`
--

CREATE TABLE IF NOT EXISTS `events_subsection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_site` varchar(255) DEFAULT NULL,
  `seo_other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_language` (`id_language`),
  KEY `id_section` (`id_section`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(255) DEFAULT NULL,
  `id_object` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `ext` varchar(10) DEFAULT NULL,
  `type` varchar(5) DEFAULT NULL,
  `position` int(4) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `type` varchar(3) DEFAULT NULL,
  `icon` text,
  `is_default` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `material_tags`
--

CREATE TABLE IF NOT EXISTS `material_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `material` varchar(100) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `frequency` int(11) NOT NULL DEFAULT '1',
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_site` varchar(255) DEFAULT NULL,
  `seo_other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`material`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_main` int(11) DEFAULT NULL COMMENT 'Принадлежность к переводу новости',
  `id_area` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_language` int(11) DEFAULT NULL,
  `title` varchar(1000) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `id_subsection` int(11) DEFAULT NULL,
  `short_text` text,
  `text` text NOT NULL,
  `id_icon` int(11) DEFAULT NULL,
  `image_file` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_public` date DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL,
  `seo_title` varchar(512) DEFAULT NULL,
  `seo_description` varchar(512) DEFAULT NULL,
  `seo_keywords` varchar(512) DEFAULT NULL,
  `view_count` int(11) NOT NULL,
  `comments_count` int(11) DEFAULT '0',
  `rating` float(3,1) DEFAULT '0.0',
  `rating_voting_count` int(11) DEFAULT '0',
  `is_show` int(1) NOT NULL,
  `is_show_on_main` int(1) NOT NULL COMMENT 'Отображать на главной',
  `is_show_in_top` int(1) NOT NULL,
  `is_selected` int(1) DEFAULT NULL,
  `gallery` int(1) DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `comment` varchar(512) DEFAULT NULL COMMENT 'для редактора',
  `hash_tag` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `id_type` (`id_section`),
  KEY `id_sub_type` (`id_subsection`) USING BTREE,
  KEY `id_language` (`id_language`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `news_comments`
--

CREATE TABLE IF NOT EXISTS `news_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` int(11) NOT NULL,
  `id_comment` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `text` text NOT NULL,
  `date_created` datetime NOT NULL,
  `is_show` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `news_images`
--

CREATE TABLE IF NOT EXISTS `news_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `ext` varchar(10) DEFAULT NULL,
  `position` int(4) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `news_section`
--

CREATE TABLE IF NOT EXISTS `news_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_site` varchar(255) DEFAULT NULL,
  `seo_other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_language` (`id_language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `news_subscriptions`
--

CREATE TABLE IF NOT EXISTS `news_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_material` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `id_subsection` int(11) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_city` (`id_user`,`id_city`),
  UNIQUE KEY `_section` (`id_user`,`id_section`),
  UNIQUE KEY `_sub_section` (`id_user`,`id_subsection`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `news_subsection`
--

CREATE TABLE IF NOT EXISTS `news_subsection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_site` varchar(255) DEFAULT NULL,
  `seo_other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_language` (`id_language`),
  KEY `id_section` (`id_section`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `person`
--

CREATE TABLE IF NOT EXISTS `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_main` int(11) DEFAULT NULL COMMENT 'Принадлежность к переводу новости',
  `id_area` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `id_language` int(11) DEFAULT NULL,
  `title` varchar(1000) NOT NULL,
  `id_section` int(11) DEFAULT NULL,
  `id_subsection` int(11) DEFAULT NULL,
  `short_text` text,
  `text` text NOT NULL,
  `image_file` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `site` varchar(255) DEFAULT NULL,
  `seo_title` varchar(512) DEFAULT NULL,
  `seo_description` varchar(512) DEFAULT NULL,
  `seo_keywords` varchar(512) DEFAULT NULL,
  `view_count` int(11) NOT NULL,
  `is_show` int(1) NOT NULL,
  `is_show_in_top` int(1) NOT NULL,
  `is_selected` int(1) DEFAULT NULL,
  `gallery` int(1) DEFAULT NULL,
  `adress` varchar(512) DEFAULT NULL,
  `latitude` decimal(20,17) DEFAULT NULL,
  `longitude` decimal(20,17) DEFAULT NULL,
  `comment` varchar(512) DEFAULT NULL COMMENT 'для редактора',
  PRIMARY KEY (`id`),
  KEY `id_type` (`id_section`),
  KEY `id_sub_type` (`id_subsection`),
  KEY `id_language` (`id_language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `persons`
--

CREATE TABLE IF NOT EXISTS `persons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_main` int(11) DEFAULT NULL COMMENT 'Принадлежность к переводу новости',
  `id_area` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `id_language` int(11) DEFAULT NULL,
  `title` varchar(1000) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `id_subsection` int(11) DEFAULT NULL,
  `short_text` text,
  `text` text NOT NULL,
  `id_icon` int(11) DEFAULT NULL,
  `image_file` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_public` date DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL,
  `seo_title` varchar(512) DEFAULT NULL,
  `seo_description` varchar(512) DEFAULT NULL,
  `seo_keywords` varchar(512) DEFAULT NULL,
  `view_count` int(11) NOT NULL,
  `comments_count` int(11) DEFAULT '0',
  `rating` float(3,1) DEFAULT '0.0',
  `rating_voting_count` int(11) DEFAULT '0',
  `is_show` int(1) NOT NULL,
  `is_show_on_main` int(1) NOT NULL COMMENT 'Отображать на главной',
  `is_show_in_top` int(1) NOT NULL,
  `is_selected` int(1) DEFAULT NULL,
  `gallery` int(1) DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `adress` varchar(512) DEFAULT NULL,
  `latitude` decimal(20,17) DEFAULT NULL,
  `longitude` decimal(20,17) DEFAULT NULL,
  `comment` varchar(512) DEFAULT NULL COMMENT 'для редактора',
  `hash_tag` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `id_type` (`id_section`),
  KEY `id_sub_type` (`id_subsection`),
  KEY `id_language` (`id_language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `persons_comments`
--

CREATE TABLE IF NOT EXISTS `persons_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` int(11) NOT NULL,
  `id_comment` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `text` text NOT NULL,
  `date_created` datetime NOT NULL,
  `is_show` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `persons_images`
--

CREATE TABLE IF NOT EXISTS `persons_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `ext` varchar(10) DEFAULT NULL,
  `position` int(4) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `persons_section`
--

CREATE TABLE IF NOT EXISTS `persons_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_site` varchar(255) DEFAULT NULL,
  `seo_other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_language` (`id_language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `persons_subscriptions`
--

CREATE TABLE IF NOT EXISTS `persons_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_material` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `id_subsection` int(11) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_city` (`id_user`,`id_city`),
  UNIQUE KEY `_section` (`id_user`,`id_section`),
  UNIQUE KEY `_sub_section` (`id_user`,`id_subsection`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `persons_subsection`
--

CREATE TABLE IF NOT EXISTS `persons_subsection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_site` varchar(255) DEFAULT NULL,
  `seo_other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_language` (`id_language`),
  KEY `id_section` (`id_section`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `places`
--

CREATE TABLE IF NOT EXISTS `places` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_main` int(11) DEFAULT NULL COMMENT 'Принадлежность к переводу новости',
  `id_area` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `id_language` int(11) DEFAULT NULL,
  `title` varchar(1000) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `id_subsection` int(11) DEFAULT NULL,
  `short_text` text,
  `text` text NOT NULL,
  `id_icon` int(11) DEFAULT NULL,
  `image_file` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_public` date DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL,
  `seo_title` varchar(512) DEFAULT NULL,
  `seo_description` varchar(512) DEFAULT NULL,
  `seo_keywords` varchar(512) DEFAULT NULL,
  `view_count` int(11) NOT NULL,
  `comments_count` int(11) DEFAULT '0',
  `rating` float(3,1) DEFAULT '0.0',
  `rating_voting_count` int(11) DEFAULT '0',
  `is_show` int(1) NOT NULL,
  `is_show_on_main` int(1) NOT NULL COMMENT 'Отображать на главной',
  `is_show_in_top` int(1) NOT NULL,
  `is_selected` int(1) DEFAULT NULL,
  `gallery` int(1) DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `adress` varchar(512) DEFAULT NULL,
  `latitude` decimal(20,17) DEFAULT NULL,
  `longitude` decimal(20,17) DEFAULT NULL,
  `comment` varchar(512) DEFAULT NULL COMMENT 'для редактора',
  `hash_tag` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `id_type` (`id_section`),
  KEY `id_sub_type` (`id_subsection`),
  KEY `id_language` (`id_language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `places_comments`
--

CREATE TABLE IF NOT EXISTS `places_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` int(11) NOT NULL,
  `id_comment` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `text` text NOT NULL,
  `date_created` datetime NOT NULL,
  `is_show` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `places_images`
--

CREATE TABLE IF NOT EXISTS `places_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `ext` varchar(10) DEFAULT NULL,
  `position` int(4) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `places_section`
--

CREATE TABLE IF NOT EXISTS `places_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_site` varchar(255) DEFAULT NULL,
  `seo_other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_language` (`id_language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `places_subscriptions`
--

CREATE TABLE IF NOT EXISTS `places_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_material` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `id_subsection` int(11) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_city` (`id_user`,`id_city`),
  UNIQUE KEY `_section` (`id_user`,`id_section`),
  UNIQUE KEY `_sub_section` (`id_user`,`id_subsection`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `places_subsection`
--

CREATE TABLE IF NOT EXISTS `places_subsection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_site` varchar(255) DEFAULT NULL,
  `seo_other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_language` (`id_language`),
  KEY `id_section` (`id_section`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `section`
--

CREATE TABLE IF NOT EXISTS `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_language` (`id_language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_mail_delivery` int(11) DEFAULT NULL,
  `delivery_time` time DEFAULT NULL,
  `id_mail_for_user` int(11) DEFAULT NULL,
  `id_mail_info` int(11) DEFAULT NULL,
  `id_mail_admin` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_language` (`id_language`),
  KEY `id_mail_delivery` (`id_mail_delivery`),
  KEY `id_mail_for_user` (`id_mail_for_user`),
  KEY `id_mail_info` (`id_mail_info`),
  KEY `id_mail_admin` (`id_mail_admin`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `settings_mail`
--

CREATE TABLE IF NOT EXISTS `settings_mail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(100) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `port` int(4) NOT NULL,
  `encryption` varchar(5) NOT NULL,
  `file_transport` int(1) NOT NULL DEFAULT '0',
  `view_path` varchar(100) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `template` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `settings_mail_templates`
--

CREATE TABLE IF NOT EXISTS `settings_mail_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  `text` text,
  `date_created` datetime DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `settings_menu`
--

CREATE TABLE IF NOT EXISTS `settings_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `in_new` int(1) DEFAULT NULL,
  `seo_title` varchar(255) NOT NULL,
  `seo_keywords` varchar(255) NOT NULL,
  `seo_description` varchar(255) NOT NULL,
  `seo_other` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `subsection`
--

CREATE TABLE IF NOT EXISTS `subsection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_language` (`id_language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `translate_message`
--

CREATE TABLE IF NOT EXISTS `translate_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_main` int(11) DEFAULT NULL,
  `id_language` int(11) DEFAULT NULL,
  `language` varchar(3) DEFAULT NULL,
  `category` varchar(150) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `message` varchar(255) NOT NULL,
  `translation` text NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(64) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `middle_name` varchar(64) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `role` enum('user','client','moderator','administrator','operator') DEFAULT NULL,
  `send_delivery_sms` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Посылать смс о доставке',
  `id_city` int(11) DEFAULT NULL,
  `balance` int(11) NOT NULL DEFAULT '0' COMMENT 'Баланс ( баллов )',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `activate_key` varchar(100) DEFAULT NULL,
  `temp_recover_password` varchar(100) DEFAULT NULL,
  `recover_password_key` varchar(100) DEFAULT NULL,
  `recover_valid_time` int(11) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_from_social` int(1) DEFAULT '0',
  `delivery_material` int(1) DEFAULT '0',
  `delivery_comment` int(1) DEFAULT '0',
  `delivery_events` int(1) DEFAULT '0',
  `publish_twitter` int(1) DEFAULT '0',
  `publish_facebook` int(1) DEFAULT '0',
  `publish_vkontakte` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `recover_key` (`recover_password_key`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user_activity`
--

CREATE TABLE IF NOT EXISTS `user_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `post` int(11) DEFAULT NULL COMMENT 'Активноть в постах',
  `comment` int(11) DEFAULT NULL COMMENT 'Активность в комментариях',
  `like` int(11) DEFAULT NULL COMMENT 'Активность в социалках',
  `total_count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user_events`
--

CREATE TABLE IF NOT EXISTS `user_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `material` varchar(255) DEFAULT NULL,
  `id_material` int(11) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `object` varchar(1000) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user_messages`
--

CREATE TABLE IF NOT EXISTS `user_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_comment` int(11) DEFAULT NULL,
  `text` text NOT NULL,
  `date_created` datetime NOT NULL,
  `is_show` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user_socials`
--

CREATE TABLE IF NOT EXISTS `user_socials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `source` varchar(255) NOT NULL,
  `source_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `source` (`source`,`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user_subscriptions`
--

CREATE TABLE IF NOT EXISTS `user_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_autor` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `world_cities`
--

CREATE TABLE IF NOT EXISTS `world_cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `country` smallint(5) unsigned NOT NULL,
  `latitude` decimal(20,17) DEFAULT NULL,
  `longitude` decimal(20,17) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `all_cols_idx` (`latitude`,`longitude`,`name`,`country`),
  KEY `name_id_idx` (`name`),
  KEY `country` (`country`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=REDUNDANT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `world_countries`
--

CREATE TABLE IF NOT EXISTS `world_countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_main` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `alias` varchar(255) NOT NULL,
  `lat` decimal(20,17) DEFAULT NULL,
  `lng` decimal(20,17) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `language_code` (`id_language`,`code`),
  KEY `el_name` (`name`),
  KEY `f930` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=REDUNDANT AUTO_INCREMENT=1 ;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `settings`
--
ALTER TABLE `settings`
  ADD CONSTRAINT `settings_ibfk_1` FOREIGN KEY (`id_language`) REFERENCES `language` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `settings_ibfk_2` FOREIGN KEY (`id_mail_delivery`) REFERENCES `settings_mail` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `settings_ibfk_3` FOREIGN KEY (`id_mail_for_user`) REFERENCES `settings_mail` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `settings_ibfk_4` FOREIGN KEY (`id_mail_info`) REFERENCES `settings_mail` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `settings_ibfk_5` FOREIGN KEY (`id_mail_admin`) REFERENCES `settings_mail` (`id`) ON DELETE SET NULL;
<?php

namespace app\controllers\api;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SignupForm;
use app\models\User;
use app\models\Mail;
use yii\helpers\Url;

use app\components\Settings;

use app\modules\material\models\Material;

class UserController extends Controller {

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        if (!Yii::$app->user->isGuest) {
            $result = array_merge(["status" => 1], User::getUserAsArray(Yii::$app->user->id));
        }
        else $result = ["status" => 0];
        return Yii::$app->json->build($result);
    }
    
    public function actionSearch($id = false) {
        $model = new User();
        Yii::$app->json->field_id = "id";
        Yii::$app->json->simple = true;
        return Yii::$app->json->search($model, ["id" => $id], [
            "path" => "user/".$id,
            "select" => [
                "id",
                "first_name",
                "last_name",
                "middle_name",
                "email",
                "image" => "avatar",
                "role",
                "id_city",
                "balance",
                "active",
                "date_created",
                "date_updated"
            ]
        ]);
    }

    public function actionAuth() {
        if (!\Yii::$app->user->isGuest) {
            //return $this->actionIndex();
        }

        $model = new LoginForm();
        if ($model->prepare(Yii::$app->request->get()) && $model->login()) {
            return $this->actionIndex();
        } else {
            return Yii::$app->json->build([], $model->getErrors());
        }
    }

    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->actionIndex();
    }

    public function actionSignup() {
        $model = new SignupForm();
        if ($model->prepare(Yii::$app->request->get())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->actionIndex();
                }
                else return Yii::$app->json->build([], ["unknown" => "Login failed"]);
            }
            else return Yii::$app->json->build([], $model->getErrors());
        }
        else return Yii::$app->json->build([], $model->getErrors());
    }
    
    public function actionChangePassword($id = false, $old_password = false, $password = false) {
        $user = User::findIdentity($id);
        if ($user) {
            if ($user->validatePassword($old_password)) {
                $user->password = $password;
                if ($user->validate()) {
                    $user->setPassword($password);
                    if ($user->save()) {
                        Mail::send(Mail::MAIL_INFO, $user->email, "Изменение пароля", "registration/recover_success", ["login" => $user->email, "first_name" => $user->first_name]);
                        return Yii::$app->json->build(["success" => 1]);
                    }
                    else return Yii::$app->json->build([], $user->getErrors());
                }
                else return Yii::$app->json->build([], $user->getErrors());
            }
            else return Yii::$app->json->build([], ["system" => Yii::t("users", "Current password is not valid")]);
        }
        else return Yii::$app->json->build([], ["system" => Yii::t("users", "User not found")]);
    }
    
    public function actionRecover($mail = null) {
        $user = User::findOne(["email" => $mail]);
        if ($user) {
            if ($user->generateRecoverKey()) {
                $link = Yii::$app->request->hostInfo.Url::toRoute(["reset", "key" => $user->recover_password_key]);
                Mail::send(Mail::MAIL_INFO, $user->email, "Восстановление пароля", "registration/recover", ["link" => $link, "first_name" => $user->first_name]);
                return Yii::$app->json->build(["success" => 1]);
            }
        }
        return Yii::$app->json->build([], ["unknown" => Yii::t("users", "User not found")]);
    }
    
    public function actionSetPassword($key = null, $password = false) {
        $user = User::findUserFromRecoverKey($key);
        if ($user) {
            if ($user->testRecoverKey($key)) {
                if ($user->changePasswordFromKey($password, $key)) {
                    Mail::send(Mail::MAIL_INFO, $user->email, "Восстановление пароля", "registration/recover_success", ["login" => $user->email, "first_name" => $user->first_name]);
                    return Yii::$app->json->build(["success" => 1]);
                }
                else return Yii::$app->json->build([], $user->getErrors());
            }
            else return Yii::$app->json->build([], ["system" => Yii::t("users", "Key is not valid")]);
        }
        else return Yii::$app->json->build([], ["system" => Yii::t("users", "User not found")]);
    }
    
    public function actionReset($key = null) {
        $model = User::findUserFromRecoverKey($key);
        if (!$model || empty($key))
            throw  new \yii\web\ForbiddenHttpException("Пользователь не найден");
        else if (!$model->testRecoverKey($key))
            throw  new \yii\web\ForbiddenHttpException("Ключ не является действительным");
        
        $success = false;
        if ($model->load(Yii::$app->request->post())) {
            $model->setPassword($model->password);
            if ($model->save()) {
                Mail::send(Mail::MAIL_INFO, $model->email, "Доступ восстановлен", "registration/recover_success", ["login" => $model->email, "first_name" => $model->first_name]);
                $success = true;
            }
        } 
        
        return $this->render("/admin/panel/reset", [
            "model" => $model,
            "success" => $success,
        ]);
    }
    
    public function actionTestemail($email = false, $type = false) {
        if (empty($email) || !isset($email))
            return "Пожалуйста, укажите email";
        if (!$type)
            $type = Mail::MAIL_INFO;
        return "Статус отправки ({$email}): ".Mail::send($type, $email, "Тестирование почты");
    }
}

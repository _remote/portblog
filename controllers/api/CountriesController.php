<?php

namespace app\controllers\api;

use Yii;
use app\models\WorldCountries;

class CountriesController extends \yii\web\Controller {
    public function actionIndex($language = "ru") {
        $countries = WorldCountries::getList($language);
        return Yii::$app->json->build($countries, false, true);
    }

}

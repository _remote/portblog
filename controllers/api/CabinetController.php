<?php

namespace app\controllers\api;

use Yii;
use yii\data\ActiveDataProvider;

use app\modules\blog\models\Blog;
use app\modules\blog\models\BlogSubscriptions;

use app\models\User;
use app\models\UserActivity;

use app\components\Settings;
use app\modules\news\models\Subscription;

class CabinetController extends \yii\web\Controller {

    public function actionSearch($id = false, $language = "ru") {
        Settings::getInstance()->setModuleId("news");
       
        if ($json = Yii::$app->json->getFile("cabinet", $id))
            return $json;
        else {
            $result = User::getUserAsArray($id);
            $activity = UserActivity::getUserActivity($id);
            $items = [];
            $items['posts'] = $activity['post'];
            $items['comments'] = $activity['comment'];
            $items['raiting'] = 0;
            $items['likes'] = $activity['like'];
            
            $blogs = Blog::find()->where(['id_user' => $id])->all();
            $items['blogs'] = [];
            foreach ($blogs as $blog) 
                $items['blogs'][] = $blog->fieldsValue($language, Blog::$select);
            $blog_subscription = BlogSubscriptions::find()->where(['id_user' => $id])->all();
            $items['subscription']['blogs'] = [];
            foreach ($blog_subscription as $subscription) 
                $items['subscription']['blogs'][] = $subscription->fieldsValue();
            $news_subscription = Subscription::find()->where(['id_user' => $id])->all();
            $items['subscription']['news'] = [];
            foreach ($news_subscription as $subscription) 
                $items['subscription']['news'][] = $subscription->fieldsValue();
            //Yii::$app->json->saveToFile([$result], "cabinet", $id);
            $result['items'] = $items;
            return Yii::$app->json->build($result, false, true);
        }
    }

}

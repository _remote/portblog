<?php

namespace app\controllers\api;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SignupForm;
use app\models\Language;

class LocalisationController extends Controller {
    
    public $select = [
        "id",
        "name",
        "type",
        "image" => "icon",
        "is_default"
    ];

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    public function actionIndex($language = false) {
        $model = new Language();
        Yii::$app->json->field_id = "type";
        Yii::$app->json->simple = true;
        
        $condition = [];
        if ($language) {
            if ($language == "all") 
                Yii::$app->json->setLanguage("all");
            else if (strlen($language) == 2) {
                $condition = ["type" => $language];
                Yii::$app->json->setLanguage($language);
            }
        }
        return Yii::$app->json->search($model, $condition, [
            "path" => "language",
            "select" => $this->select
        ]);
    }

}

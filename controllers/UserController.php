<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SignupForm;
use app\models\User;
use app\models\Mail;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\bootstrap\Html;
use yii\helpers\Json;

use app\components\Settings;
use app\models\Language;
use app\models\UserEvents;
use app\models\UserSubscriptions;

use app\modules\material\models\Material;

class UserController extends Controller {
    
    public function init() {
        parent::init();
        $this->layout = "site";
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex($id = false, $language = "ru", $tab = false, $id_post = false) {
        if (Yii::$app->user->isGuest && !$id)
            $this->redirect("/site/index");
        
        $socials = false; $posts = false;
        if ($user = User::findIdentity(($id)?$id:Yii::$app->user->id)) {
            if (!$id)
                $socials = $user->getSocials();
            $posts = $user->getPosts();
            
        }
        
        $class = Settings::getInstance()->getClassName("blogs", Settings::MATERIAL);
        if ($id_post) {
            $model = $class::find()->where(["id" => $id_post])->one();
        }
        else {
            $model = new $class;
            $model->id_language = Language::getLanguageId($language);
        }
        
        //Yii::$app->debug->show($user);
        
        return $this->render("index", [
            "view" => ($id)?true:false,
            "user" => $user,
            "socials" => $socials,
            "posts" => $posts,
            "model" => $model,
            "tab" => $tab,
        ]);
    }
    
    public function actionView($id = false) {
        return $this->actionIndex(intval($id));
    }
    
    public function actionCreatePost($id = false) {
        if (Yii::$app->user->isGuest)
            $this->redirect("/site/index");
        
        $class = Settings::getInstance()->getClassName("blogs", Settings::MATERIAL);
        $class_image = Settings::getInstance()->getClassName("blogs", Settings::IMAGE);

        $redirect_tab = "posts";
        
        if ($id) {
            $model = $class::find()->where(["id" => $id])->one();
        }
        else {
            $model = new $class();
            $model->id_language = Language::getLanguageId();
            $model->scenario = "insert";
            $model->date_public = date("Y-m-d G:i:s", time());
            $model->id_user = Yii::$app->user->id;
        }
        
        $post = Yii::$app->request->post();
        
        if (@$post['action'] == "preview") {
            $model->is_show = $class::STATUS_DRAFT;
            $redirect_tab = "create_post";
        } else {
            $model->is_show = $class::STATUS_PUBLISHED;
        }
        
        if ($model->load(Yii::$app->request->post())) {
            $gallery = false;
            $model->date_created = date("Y-m-d G:i:s", time());
            $model->fieldsClear();
            $model->_temp_gallery = UploadedFile::getInstances($model, '_temp_gallery');
            if (empty($model->short_text)) {
                $model->short_text = strip_tags(htmlspecialchars_decode(substr($model->text, 0, 1000)));
            }
            
            if ($model->save()) {
                $id_main = $model->id;
                $class::updateAll(["id_main" => $id_main], ["id" => $model->id]);
                //Загружаем галерею
                if ($model->_temp_gallery) {
                    foreach ($model->_temp_gallery as $image) {
                        $class_image::addImage($id_main, $image);
                    }
                    $class::updateGalleryStatus($id_main, true);
                    $gallery = true;
                }
                return $this->redirect(Url::toRoute(["/user/index", "tab" => $redirect_tab, "id_post" => $model->id]));
                return json_encode(["result" => true]);
            }
            else Yii::$app->debug->show($model->getErrors());
        }
    }
    
    public function actionSublist() {
        $class = Settings::getInstance()->getClassName("blogs", Settings::SUB_SECTION);
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $section_id = $parents[0];
                $out = $class::find()->select(['name' => 'name', 'id'])->where(["id_section" => $section_id])->orderBy("`name` ASC")->asArray()->all();

                //$out = ArrayHelper::map($items, 'id', 'name');
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }
    
    public function actionUpdatePost($id = false) {
        if (Yii::$app->user->isGuest)
            $this->redirect("/site/index");

        $class = Settings::getInstance()->getClassName("blogs", Settings::MATERIAL);
        $class_image = Settings::getInstance()->getClassName("blogs", Settings::IMAGE);

        if ($id)
            $model = $class::find()->where(["id" => intval($id)])->one();
        else $model = new $class;
        //Yii::$app->debug->show($model);
        //Если пользователь на авторизированный или пытается редактировать не свой пост
        if (Yii::$app->user->isGuest || (!$model->isNewRecord && $model->id_user != Yii::$app->user->id))
            return $this->redirect("index");
        $model->loadScenario("update");

        if ($model->load(Yii::$app->request->post())) {
            $gallery = false;
            $model->fieldsClear();
            $model->_temp_gallery = UploadedFile::getInstances($model, '_temp_gallery');
            if (empty($model->short_text)) {
                $model->short_text = strip_tags(htmlspecialchars_decode(substr($model->text, 0, 1000)));
            }
            if ($model->save()) {
                //Загружаем галерею
                if ($model->_temp_gallery) {
                    foreach ($model->_temp_gallery as $image) {
                        $class_image::addImage($model->id, $image);
                    }
                    $class::updateGalleryStatus($model->id, true);
                    $gallery = true;
                }
                return $this->redirect("index");
            }
            //else Yii::$app->debug->show($model->getErrors());
        }
        
        return $this->render("post", [
            "model" => $model,
        ]);
    }
    
    public function actionDeletePost($id = false) {
        $class = Settings::getInstance()->getClassName("blogs", Settings::MATERIAL);
        return $class::updateAll(["is_show" => $class::STATUS_DELETED], ["id" => intval($id)]);
    } 
    
    public function actionAuth() {

    }

    public function actionLogout() {
        
    }

    public function actionSignup() {
        
    }
    
    public function actionChangePassword() {
        $model = new User;
        $model->load(Yii::$app->request->post());
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->user->isGuest) 
            return ['error' => Yii::t('client', "User not found")];
                
        $user = User::findIdentity(Yii::$app->user->id);
        
        if ($user) {
            if (empty($model->password2)) 
                    return ['password2' => Yii::t('client', "Password2 can`t be empty")];
            if ($user->validatePassword($model->password)) {
                $user->password = $model->password2;
                //echo $model->password2;
                if ($user->validate()) {
                    $user->setPassword($model->password2);
                    if ($user->save()) {
                        Mail::send(Mail::MAIL_INFO, $user->email, "Изменение пароля", "registration/recover_success", ["login" => $user->email, "first_name" => $user->first_name]);
                        return ["success" => Yii::t('client', "Password changed successful.")];
                    }
                    //else echo 4;
                }
                //else echo 3;
            }
            else return ['password' => Yii::t('client', "Invalid current password")];
        }
        else return ['error' => Yii::t('client', "User not found")];
        return $user->getErrors();
    }
    
    public function actionRecover($email = false) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new User;
        $model->load(Yii::$app->request->get());
        //Yii::$app->debug->show($model);
        if (empty($model->email))
            return ["email" => Yii::t('client', "Please write correct e-Mail")];
        $user = User::findOne(["email" => $model->email]);
        if ($user) {
            if ($user->generateRecoverKey()) {
                $link = Yii::$app->request->hostInfo.Url::toRoute(["reset", "key" => $user->recover_password_key]);
                Mail::send(Mail::MAIL_INFO, $user->email, "Восстановление пароля", "registration/recover", ["link" => $link, "first_name" => $user->first_name]);
                return ["success" => Yii::t('client', "We've sent you an email with further instructions")];
            }
            else return ["email" => Yii::t("users", "Password can`t be changed")];
        }
        return ["email" => Yii::t("users", "User not found")];
    }
    
    public function actionSetPassword($key = null, $password = false) {
        $user = User::findUserFromRecoverKey($key);
        if ($user) {
            if ($user->testRecoverKey($key)) {
                if ($user->changePasswordFromKey($password, $key)) {
                    Mail::send(Mail::MAIL_INFO, $user->email, "Восстановление пароля", "registration/recover_success", ["login" => $user->email, "first_name" => $user->first_name]);
                    return Yii::$app->json->build(["success" => Yii::t('client', "You password chenged successful")]);
                }
                else return Yii::$app->json->build([], $user->getErrors());
            }
            else return Yii::$app->json->build([], ["system" => Yii::t("users", "Key is not valid")]);
        }
        else return Yii::$app->json->build([], ["system" => Yii::t("users", "User not found")]);
    }
    
    public function actionReset($key = null) {
        $model = User::findUserFromRecoverKey($key);
        if (!$model || empty($key))
            throw  new \yii\web\ForbiddenHttpException("Пользователь не найден");
        else if (!$model->testRecoverKey($key))
            throw  new \yii\web\ForbiddenHttpException("Ключ не является действительным");
        
        $success = false;
        if ($model->load(Yii::$app->request->post())) {
            $model->setPassword($model->password);
            if ($model->save()) {
                Mail::send(Mail::MAIL_INFO, $model->email, "Доступ восстановлен", "registration/recover_success", ["login" => $model->email, "first_name" => $model->first_name]);
                $success = true;
            }
        } 
        
        return $this->render("reset", [
            "model" => $model,
            "success" => $success,
        ]);
    }
    
    public function actionTestemail($email = false, $type = false) {
        if (empty($email) || !isset($email))
            return "Пожалуйста, укажите email";
        if (!$type)
            $type = Mail::MAIL_INFO;
        return "Статус отправки ({$email}): ".Mail::send($type, $email, "Тестирование почты");
    }
    
    public function actionChangeOption($option = false, $value = false) {
        if (User::isChecked($option) && !Yii::$app->user->isGuest) {
            $user = User::findIdentity(Yii::$app->user->id);
            $user->$option = intval($value);
            if ($user->save()) {
                return 1;
            }
        }
    }
    
    public function actionAvatar() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->user->isGuest)
            return ["error" => Yii::t('client', "Session is old. Please login again.")];
        $id = Yii::$app->user->id;
        if ($model = User::findIdentity($id)) {
            $model->loadScenario("update");
            //Yii::$app->debug->show($_FILES);
            if ($model->load(Yii::$app->request->post())) {
                //Yii::$app->debug->show(Yii::$app->request->post());
                if ($model->save()) {
                    //Yii::$app->debug->show($model);
                    UserEvents::message("You change avatar");
                    Yii::$app->json->clearCache("user/".$id);
                    return ["avatar" => $model->getThumbUploadUrl('avatar', 'preview')];
                }
            }
        }
    }
    
    public function actionSubscription($type = false, $id = false) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($type == "user") {
            if (UserSubscriptions::sign(intval($id))) {
                $user = User::findIdentity($id);
                return ["success" => Html::button("Отписаться", [
                    "class" => "subscription user_unsubscribe",
                    "onclick" => "bootbox.confirm('".Yii::t('client', 'Are you sure you want to unsubscribe from {user}?', ["user" => $user->getFio()]) ."', function(status) { if (status) {unsubscription($('button.subscription.user_unsubscribe'), 'user', {$id});}});",
                ])];
            }
        } else {
            
        }
    }
    
    public function actionUnsubscription($type = false, $id = false) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($type == "user") {
            if (UserSubscriptions::unsign(intval($id))) {
                return ["success" => Html::button("Подписаться", [
                    "class" => "subscription",
                    "onclick" => "subscription(this, 'user', {$id})",
                ])];
            }
        } else {
            
        }
    }
    
    public function actionSetActiveSlide($type = false, $id_material = false, $id_image = false) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($type) {
            $class = Settings::getInstance()->getClassName("blogs", Settings::IMAGE);
            $class::updateAll(['is_active' => 0], ["id_material" => $id_material]);
            $class::updateAll(['is_active' => 1], ["id" => $id_image,"id_material" => $id_material]);
            return ["result" => true, "active" => $id_image];
        }
    }
    
    public function actionDeleteSlide($id = false) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $class = Settings::getInstance()->getClassName("blogs", Settings::IMAGE);
        $slide = $class::find()->where(["id" => $id])->one();
        $slide->is_delete = 1;
        if ($slide->delete())
            return ["result" => true];
    }
}

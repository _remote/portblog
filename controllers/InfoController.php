<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

use app\components\Site;
use app\components\Settings;
use app\models\Language;
use app\modules\settings\models\SettingsFooter;


class InfoController extends Controller {
    
    public function init() {
        parent::init();
        $this->layout = "site";
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    private function findModel($path = false, $language = "ru") {
        $id_language = Language::getLanguageId($language);
        return SettingsFooter::findOne(["slug" => $path, "id_language" => $id_language]);
    }


    public function actionIndex($path = false, $language = "ru") {
        $model = $this->findModel($path, $language);
        if (!$model) 
            return new \yii\web\HttpException(404, Yii::t("client", "Page not found"));
        
        return $this->render("index", [
            "model" => $model,
        ]);
    }
    
        
}


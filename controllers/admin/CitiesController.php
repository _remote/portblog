<?php

namespace app\controllers\admin;

use Yii;
use app\models\WorldCities;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\WorldCountries;
use yii\helpers\Json;

use app\models\Language;

/**
 * CitiesController implements the CRUD actions for WorldCities model.
 */
class CitiesController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all WorldCities models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => WorldCities::find()->where('`id_main` = `id`'),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WorldCities model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $language = "ru") {
        return $this->render('view', [
            'models' => $this->findModels($id),
            'main' => $this->findModel($id),
            'language' => $language
        ]);
    }

    /**
     * Creates a new WorldCities model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($language = "ru") {
        $languages = Language::getList();
        $models = [];
        foreach ($languages as $id => $type) {
            $model = new WorldCities();
            $model->id_language = $id;
            $models[$id] = $model;
        }
        
        if (WorldCities::loadMultiple($models, Yii::$app->request->post())) {
            $id_main = 0;
            foreach ($models as $id_language => $model) {
                $model->id_main = $id_main;
                if ($model->save()) {
                    if (!$id_main) {
                        $id_main = $model->id;
                        WorldCities::updateAll(["id_main" => $id_main], ["id" => $model->id]);
                    }
                    //Yii::$app->json->clearCache("news/".Yii::$app->formatter->asDate($model->date_created, 'php:Y-m-d'));
                }
            }
            if ($id_main)
                return $this->redirect(['view', 'id' => $id_main]);
        } 
        return $this->render('create', [
            'models' => $models,
            'languages' => $languages,
            'language' => $language,
        ]);
        
    }

    /**
     * Updates an existing WorldCities model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $language = "ru") {
        $models = $this->findModels($id);
        $languages = Language::getList();
        foreach ($languages as $id_language => $type) {
            if (!isset($models[$id_language])) { 
                $model = new WorldCities();
                $model->id_language = $id_language;
                $model->id_main = $id;
                $models[$id_language] = $model;
            }
        }
        if (WorldCities::loadMultiple($models, Yii::$app->request->post())) {
            $success = false;
            foreach ($models as $id_language => $model) {
                if ($model->validate()) {
                    if ($model->save()) {
                        $success = true;
                    }
                }
            }
            if ($success)
                return $this->redirect(['view', 'id' => $id]);
                
        } 
        return $this->render('update', [
            'models' => $models,
            'languages' => $languages,
            'main' => $this->findModel($id),
            'language' => $language,
        ]);
        
    }

    /**
     * Deletes an existing WorldCities model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $models = $this->findModels($id);
        foreach ($models as $model) {
            if ($model->delete()) {
                //Можно сделать что-то
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the WorldCities model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WorldCities the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = WorldCities::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModels($id) {
        $id_main = ($object = WorldCities::findOne(["id" => $id]))?$object->id_main:false;
        if ($items = WorldCities::find()->where(["id_main" => $id_main])->orderBy("id_language ASC")->all()) {
            $models = [];
            foreach ($items as $item) 
                $models[$item->id_language] = $item;
            return $models;
        } else {
            throw new NotFoundHttpException('The requested page does not existed.');
        }
    }
    
    
    public function actionCountrylist() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $id_language = $parents[0];
                $out = WorldCountries::find()->select(['name' => 'name', 'id'])->where(["id_language" => $id_language])->orderBy("`name` ASC")->asArray()->all();
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

}

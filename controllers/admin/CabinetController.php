<?php

namespace app\controllers\admin;

use Yii;
use app\modules\blog\models\Blog;
use app\models\User;
use yii\data\ActiveDataProvider;

class CabinetController extends \yii\web\Controller {
    
    public $id_user = false;
    
    public function init() {
        parent::init();
        if (!Yii::$app->user->isGuest)
            $this->id_user = Yii::$app->user->id;
    }

    public function actionIndex() {
        $blogs = Blog::find()->where(['id_user' => $this->id_user])->all();
        $dataProvider = new ActiveDataProvider([
            'query' => Blog::find()->where(['id_user' => $this->id_user]),
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

}

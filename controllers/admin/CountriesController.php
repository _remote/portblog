<?php

namespace app\controllers\admin;

use Yii;
use app\models\WorldCountries;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Language;

/**
 * CountriesController implements the CRUD actions for WorldCountries model.
 */
class CountriesController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all WorldCountries models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => WorldCountries::find()->where('`id_main` = `id`'),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WorldCountries model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $language = "ru") {
        return $this->render('view', [
            'models' => $this->findModels($id),
            'main' => $this->findModel($id),
            'language' => $language
        ]);
    }

    /**
     * Creates a new WorldCountries model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($language = "ru") {
        
        $languages = Language::getList();
        $models = [];
        foreach ($languages as $id => $type) {
            $model = new WorldCountries();
            $model->id_language = $id;
            $models[$id] = $model;
        }
        
        if (WorldCountries::loadMultiple($models, Yii::$app->request->post())) {
            $id_main = 0;
            foreach ($models as $id_language => $model) {
                $model->id_main = $id_main;
                $model->code = strtoupper($model->code);
                if ($model->save()) {
                    if (!$id_main) {
                        $id_main = $model->id;
                        WorldCountries::updateAll(["id_main" => $id_main], ["id" => $model->id]);
                    }
                    //Yii::$app->json->clearCache("news/".Yii::$app->formatter->asDate($model->date_created, 'php:Y-m-d'));
                }
            }
            if ($id_main)
                return $this->redirect(['view', 'id' => $id_main]);
        } 
        return $this->render('create', [
            'models' => $models,
            'languages' => $languages,
            'language' => $language,
        ]);
        
    }

    /**
     * Updates an existing WorldCountries model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $language = "ru") {
        $models = $this->findModels($id);
        $languages = Language::getList();
        foreach ($languages as $id_language => $type) {
            if (!isset($models[$id_language])) { 
                $model = new WorldCountries();
                $model->id_language = $id_language;
                $model->id_main = $id;
                $models[$id_language] = $model;
            }
        }
        if (WorldCountries::loadMultiple($models, Yii::$app->request->post())) {
            $success = false;
            foreach ($models as $id_language => $model) {
                if ($model->validate()) {
                    if ($model->save()) {
                        $success = true;
                    }
                }
            }
            if ($success)
                return $this->redirect(['view', 'id' => $id]);
                
        } 
        return $this->render('update', [
            'models' => $models,
            'languages' => $languages,
            'main' => $this->findModel($id),
            'language' => $language,
        ]);
        
    }

    /**
     * Deletes an existing WorldCountries model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $models = $this->findModels($id);
        foreach ($models as $model) {
            if ($model->delete()) {
                //Можно сделать что-то
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the WorldCountries model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WorldCountries the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = WorldCountries::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModels($id) {
        $id_main = ($object = WorldCountries::findOne(["id" => $id]))?$object->id_main:false;
        if ($items = WorldCountries::find()->where(["id_main" => $id_main])->orderBy("id_language ASC")->all()) {
            $models = [];
            foreach ($items as $item) 
                $models[$item->id_language] = $item;
            return $models;
        } else {
            throw new NotFoundHttpException('The requested page does not existed.');
        }
    }

}

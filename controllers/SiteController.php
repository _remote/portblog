<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\UserSocials;

use app\components\Site;
use app\components\Settings;
use app\modules\material\models\Material;
use app\modules\material\components\Loader;

use app\models\LoginForm;
use app\models\SignupForm;
use app\components\AuthAction;
use yii\authclient\clients\Twitter;

class SiteController extends Controller {
    
    public function init() {
        parent::init();
        $this->layout = "site";
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'auth-social' => [
              'class' => 'app\components\AuthAction',
              'successCallback' => [$this, 'oAuthSuccess'],
              //'_successUrl' => "/material/blogs"
            ],
        ];
    }
    
    public function oAuthSuccess($client) {
        // get user data from client
        $userAttributes = $client->getUserAttributes();
        
        //Yii::$app->debug->show($_GET);
        
        if ($social = UserSocials::find()->where(["source" => $client->getId(), "source_id" => $userAttributes['id']])->one()) {
            //Если не гость, и другой пользователь, то привязать социалку к текущему авторизированному
            if (!Yii::$app->user->isGuest && Yii::$app->user->id != $social->user_id) {
                $social->user_id = Yii::$app->user->id;
                if ($social->save()) {
                    $user->active = User::STATUS_BLOCK_FROM_SOCIAL;
                    //$user->is_from_social = 1;
                    if ($user->save()) {

                    }
                }
            }
            else if ($user = User::findIdentity($social->user_id)) 
                Yii::$app->user->login($user, 3600*24*30);
        }
        else {
            if (Yii::$app->user->isGuest) {
                $model = new User;
                //$model->scenario = "social";
                $model->setPassword(md5(time()));
                $model->date_created = date("Y-m-d G:i:s");
                $model->is_from_social = 1;
                switch ($client->getId()) {
                    case "facebook" : 
                        $name = preg_split("/\s/", $userAttributes['name']);
                        $model->first_name = @$name[0];
                        $model->last_name = @$name[1];
                        break;
                    case "vkontakte" : 
                        $model->first_name = $userAttributes['first_name'];
                        $model->last_name = $userAttributes['last_name'];
                        break;
                    case "twitter" : $model->first_name = $userAttributes['name'];
                        break;
                }
            } 
            else $model = User::findIdentity(Yii::$app->user->id);
            
            if (!$model->isNewRecord || $model->save()) {
                if (!$social)
                    $social = new UserSocials;
                $social->user_id = $model->id;
                $social->source = (string)$client->getId();
                $social->source_id = (string)$userAttributes['id'];
                if ($social->save()) {
                    Yii::$app->user->login($model, 3600*24*30);
                }
                //else Yii::$app->debug->show($social->getErrors());
            }
            //else Yii::$app->debug->show($model->getErrors());
        }
        // do some thing with user data. for example with $userAttributes['email']
        
//        if ($page = @Yii::$app->request->get('redirect')) {
//            $js = new \yii\web\JsExpression("
//                alert(1)
//                ");
//            //return $this->action->redirect($page);
//            
//            //Yii::$app->getResponse()->refresh();//content($js);//redirect("/material/".$page);
//            //return 1;
//        }
    }
    
    private function findModel($type = "news", $path = false, $id = false) {
        Settings::getInstance()->setModuleId($type);
        $class = Settings::getInstance()->getModulePath().'\models\Material';
        if ($id) 
            return $class::findOne(["id" => $id]);
        else if ($path) 
            return $class::findOne(["slug" => $path]);
        else return $class;
    }


    public function actionIndex($language = "ru", $city = false, $type = false, $section = false, $subsection = false, $path = false) {
        if ($path) 
            return $this->view($language, $type, $city, $section, $subsection, $path);
        
        $counts = [];
        Site::$material = $type;
        $items = []; $sections = false;
        $materials = ["news", "articles", "events", "companies" => "company", "persons", "places", "blogs"];
        //Yii::$app->debug->showAll($type);
        foreach ($materials as $path => $material) {
            if ($type && ($type != $material && $type != (string)$path))
                continue;
            
            $loader = new Loader($material);
            $loader->setLanguage($language);
            $loader->setCity($city);
            $loader->setSection($section);
            $loader->setSubSection($subsection);
            
            if ($type) {
                $loader->fullList();
                $class = Settings::getInstance()->getClassName($material, Settings::SECTION);
                $sections = $class::getItemsList($language);
            } else if ($material == "blogs") {
                $loader->fullList(false, 10);
            } else $loader->topList();
            //$loader->showSQL();
            $items[$material] = $loader->get();
            $counts[$material] = $loader->getTotal();
        }

        //Yii::$app->debug->show($counts);
        
        return $this->render("index", [
            "items" => $items,
            "selected" => $type,
            "sections" => $sections,
            "id_section" => $section,
            "id_sub_section" => $subsection,
            "counts" => $counts,
        ]);
    }
    
    public function actionLoadMore($material = false, $id_section = false, $page = 1, $full = false) {
        $loader = new Loader($material);
        $loader->setSection($id_section);
        $loader->setLanguage(Site::getInstance()->getLanguage());
        //$loader->showSQL();
        if (!$full) //Если не главная страница
            $loader->topList(7, 6); //первая страница 7 элементов, а дальше + 6
        else $loader->fullList(31, 15); //первая страница 31 элементов, а дальше + 15
        if ($items = $loader->get($page)) {
            return $this->renderAjax("_ajax_list", [
                "items" => $items,
                "material" => $material,
                "page" => $page,
            ]);
        }
        
    }
    
    private function view($language = "ru", $type = "news", $city = false, $section = false, $subsection = false, $path = false) {
        $id = false;
        
        if ($model = $this->findModel($type, $path)) {
            $id = $model->id;
        }
        
        Site::$material = $type;
        $class_comment = Settings::getInstance()->getModulePath() . "\models\Comment";
        $comment = new $class_comment();
        
        if ($comment->load(Yii::$app->request->post()) && !Yii::$app->user->isGuest) {
            //Yii::$app->debug->show($comment);
            $comment->id_user = @Yii::$app->user->id;
            $comment->id_material = $id;
            $comment->date_created = date("Y-m-d G:i:s", time());
            if ($comment->validate()) {
                if ($comment->save()) {
                    $model->updateCommentsCount();
                    Yii::$app->json->clearCache(Settings::getInstance()->getModuleId() . "/" . $id . ".json");
                    $comment->text = "";
                    //return true;
                }
            }
        }
                
        $sorted_comments = $class_comment::getSortedCommentsList($id);
        $commentProvider = new \yii\data\ArrayDataProvider([
            'key' => 'id', //or whatever you id actually is of these models.
            'allModels' => $class_comment::getCommentsList($sorted_comments),
            //'pagination' => array('pageSize' => 3),
        ]); 
        $levels = $class_comment::getCommentsLevel($class_comment::getSortedCommentsList($id));
        
       
        
        return $this->render("view", [
            "model" => $model,
            'commentProvider' => $commentProvider,
            'commentModel' => $comment,
            'levels' => $levels,
            "material" => $type,
        ]);
    }
    
    public function actionViewsIncrement($type = false, $id = false) {
        //Yii::$app->debug->show($type);
        $model = $this->findModel($type, false, $id);
        return $model->incrementViews();
    }
    
    public function actionChangeRating($type = false, $id = false, $value = false) {
        $model = $this->findModel($type, false, $id);
        Yii::$app->debug->show($model);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ($result = $model->changeRating($value))?["result" => true, "rating" => $result]:["result" => false];
    }
    
    public function actionRating() {
        ini_set("memory_limit","512M");
        $materials = ["articles", "blogs", "events", "company", "news", "persons", "places"];
        foreach ($materials as $material) {
            Settings::getInstance()->setModuleId($material);
            $class = Settings::getInstance()->getModulePath($material)."\models\Material";
            $class_comment = Settings::getInstance()->getModulePath($material)."\models\Comment";
            if ($items = $class::find()->where("id IS NOT NULL")->all()) {
                foreach ($items as $index => $item) {
                    $count = $class_comment::find()->where(["id_material" => $item->id])->count();
                    $item->scenario = "update";
                    $item->comments_count = $count;
                    $item->rating = (rand(1, 50) / 10.0);
                    $item->rating_voting_count = rand(1, 100);
                    //echo "{$material} {$item->id}";
                    if ($item->validate()) {
                        $item->save();
                    }
                    else Yii::$app->debug->show($item->getErrors());
                }
            }
            
        }
    }
      
    public function actionSearch($text = false, $tag = false, $material = "all") {
        if ($material == "all")
            $materials = ["news" => [], "articles" => [], "events" => [], "blogs" => []];
        else $materials[$material] = [];
        $search = false;
        if ($text || $tag) {
            $search = $text?$text:($tag?$tag:false);
            foreach ($materials as $type => &$items) {
                $search_tag = ($text)?false:true;
                $items = Material::fullSearch($type, $search, $search_tag);
            }
        }
        
        echo $this->render("search", [
            "items" => $materials,
            "material" => $material,
            "search" => $search,
        ]);
    }
    
//    public function actionBlogs($path = false, $language = "ru") {
//        $type = "blogs";
//        if ($path) 
//            return $this->view($path, $type, $language);
//        
//        Site::$material = $type;
//        $class = Settings::getInstance()->getClassName($type, Settings::MATERIAL);
//        
//        $items = false;
//        $date = date("Y-m-d", time());
//        // Создать зависимость от времени модификации материала
//        $dependency = new \yii\caching\DbDependency();
//        $dependency->sql = 'SELECT MAX(date_update) FROM '.$type;
//        //Yii::$app->debug->show($dependency);
//        $items = $class::getDb()->cache(function ($db) use ($class, $date){
//            $it = $class::find()->where(["and", ["<=", "date_public", $date], ["is_show" => $class::STATUS_PUBLISHED]]);
//            return $it->orderBy(["`date_public` DESC" => "", "`order` ASC" => ""])
//                ->limit(35)
//                ->all();
//        }, 10, $dependency);
//        
//        return $this->render("blog", [
//            "items" => $items,
//            "selected" => $type,
//        ]);
//    }
    
    public function actionAuth() {
        if (!Yii::$app->user->isGuest) 
            return $this->redirect("/site/index");
        
        //Yii::$app->controller->enableCsrfValidation = false;
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $selector = @Yii::$app->request->post('selector');
            return json_encode(["result" => true, "selector" => $selector]);
        }
        $errors = $model->getErrors();
        return json_encode(["result" => false, "error" => array_shift($errors)]);
    }
    
    public function actionLogout() {
        Yii::$app->user->logout();
        $page = @Yii::$app->request->get('redirect');
        return $this->redirect($page?$page:"/site/index");
    }
    
    public function actionSignup() {
        $model = new SignupForm();
        $model->scenario = "signup";
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return json_encode(["result" => true]);
                }
            }
        }
        $errors = $model->getErrors();
        return json_encode(["result" => false, "error" => array_shift($errors)]);
    }
    
    public function actionChangeLanguage($language = "ru") {
        Site::getInstance()->setLanguage($language, true);
        return true;
    }
    
    public function actionCallback() {
        $model = new \app\models\Callback;
        $model->time = time();
        $model->session = Yii::$app->session->id;
        $model->status = 0;
        if ($model->load(Yii::$app->request->post())) {
            $period = $model->testPeriod();
            if (!$period) {
                if ($model->save()) {
                    return json_encode(["result" => true, "success" => Yii::t("client", "Message sent successful")]);
                }
            } else {
                return json_encode(["result" => false, "error" => Yii::t("client", "It remains to wait {period} seconds", ["period" => $period])]);
            }
        }
        $errors = $model->getErrors();
        $error = array_shift($errors);
        return json_encode(["result" => false, "error" => @$error[0]]);
    }
    
}

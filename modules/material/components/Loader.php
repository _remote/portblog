<?php
/**
 * Используется для загрузки материала
 */

namespace app\modules\material\components;

use Yii;

use app\components\Settings;
use app\modules\material\models\Material;
use app\models\Language;
use app\models\WorldCities;
use app\models\WorldCountries;

class Loader {
       
    public $page            = 1;
    public $limit           = 31;
    public $is_main         = false;
    public $is_view         = false;
    public $language        = "ru";
    public $city            = false;
    public $section         = false;
    public $sub_section     = false;
    
    
    
    public $start           = 0;
    public $success         = true;
    
    private $class              = false;
    private $class_section      = false;
    private $class_subsection   = false;
    private $material           = false;
    private $refresh_timeout    = 300;
    private $sql                = false;
    private $total              = 0;
    
    //Список допустимых материалов в запросах
    public $white_list = ["articles", "blogs", "company", "events", "news", "persons", "places"];
    
    /**
     * 
     * @param Material $material
     */
    public function __construct($material) {
        if (in_array($material, $this->white_list)) {
            $this->material = $material;
            $this->class = Settings::getInstance()->getClassName($material, Settings::MATERIAL);
            $this->class_section = Settings::getInstance()->getClassName($material, Settings::SECTION);
            $this->class_subsection = Settings::getInstance()->getClassName($material, Settings::SUB_SECTION);
        }
    }
    
    public function getMaterial() {
        return $this->material;
    }
    
    public function getClass() {
        return ($this->class)?$this->class:(new Material());
    }
    
    public function getTotal() {
        return $this->total;
    }
    
    public function getTimeout() {
        return $this->refresh_timeout;
    }
    
    public function showSQL() {
        $this->sql = true;
        $this->success = false;
    }
    
    public function topList($start = false, $limit = false) {
        $this->is_main = true;
        $this->start = $start;
        $this->limit = ($limit)?$limit:7;
    }
    
    public function fullList($start = false, $limit = false) {
        $this->is_view = true;
        $this->start = $start;
        $this->limit = ($limit)?$limit:31;
    }
    
    public function setLanguage($language = false) {
        if (intval($language) > 0)
            $this->language = intval($language);
        else if ($language && $id_language = Language::getLanguageId($language)) {
            $this->language = $id_language;
        }
    }
    
    public function setCity(&$city = false) {
        if (intval($city) > 0)
            $this->city = intval($city);
        else if ($city && $item = WorldCities::find()->where(["slug" => $city])->one()) {
            $this->city = $city = $item->id;
        }
    }
    
    public function setSection(&$section = false) {
        $class = $this->class_section;
        if (intval($section) > 0)
            $this->section = intval($section);
        else if ($section && $item = $class::find()->where(["slug" => $section])->one()) {
            $this->section = $section = $item->id;
        }
        //Yii::$app->debug->show($this->section);       
    }
    
    public function setSubSection(&$section = false) {
        $class = $this->class_subsection;
        if (intval($section) > 0)
            $this->sub_section = intval($section);
        else if ($section && $item = $class::find()->where(["slug" => $section])->one()) {
            $this->sub_section = $section = $item->id;
        }
        //Yii::$app->debug->show($this->sub_section);
    }
    
    public function setTotal($count = 0) {
        if ($count > 0)
            $this->total = $count;
    }
    
    public function get($page = false, $limit = false) {
        if (intval($page))
            $this->page = $page;
        if (intval($limit))
            $this->limit = $limit;
        
        $class = $this->class;
        if ($class && $this->material) {
            //\Yii::$app->debug->show($this);
            return $class::getItems($this);
        }
        else return false;
    }
};


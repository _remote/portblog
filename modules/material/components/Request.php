<?php
/**
 * Для работы с api. 
 */

namespace app\modules\material\components;

use Yii;

use app\models\User;
use app\models\Language;
use app\models\Mail;

class Request {
    
    public $type_resource;
    public $path_to_module;

    public $select = [
        "id",
        "id_area",
        "id_city",
        //"id_language",
        "id_user",
        "id_section",
        "id_subsection",
        "title",
        "path" => "slug",
        "text",
        "short_text",
        "image_preview" => "image_file",
        "image",
        "video",
        "date_created",
        //"date_update",
        "date_public",
        "site",
        "seo_title",
        "seo_description",
        "seo_keywords",
        "is_show",
        "is_show_in_top",
        "is_selected",
        "comment",
        "view_count"
    ];
    
    public function __construct($module_id = false) {
        $this->type_resource = $module_id;
        $this->path_to_module = 'app\modules\\'.$this->type_resource.'\\models\\';
    }

    public function search($id = false, $date = false, $language = "ru", $temp = false) {
        $class = $this->path_to_module."Material";
        $model = new $class();
        $status = ($temp) ? $class::STATUS_DRAFT : $class::STATUS_PUBLISHED;
        //Поиск по id
        if ($id) {
            if ($json = Yii::$app->json->getFile($this->type_resource, $id))
                return $json;
            else if ($material = $class::getMaterialWithComments($id, $language, $this->select)) {
                Yii::$app->json->saveToFile([$material], $this->type_resource, $id);
                return Yii::$app->json->build([$material], false, true);
            }
        }
        //Поиск по дате
        else if ($date && strlen($date) == 8) {
            $_date = preg_replace("/^(\d{4})(\d{2})(\d{2})$/i", "$1-$2-$3", $date);
            $_date_next = date("Y-m-d", (strtotime($_date) + 24 * 3600));
            $model = new $class();
            Yii::$app->json->field_id = "date_public";
            return Yii::$app->json->search($model, ["and", [">=", "date_public", $_date], ["<", "date_public", $_date_next], ["is_show" => $status, "id_language" => Language::getLanguageId($language)]], [
                        "path" => $this->type_resource."/" . $_date,
                        "type" => $this->type_resource,
                        "language" => $language,
                        "conformity" => [ //соответсвие полей в запросе к базе
                            "agglomeration" => "id_city",
                            "section" => "id_section",
                            "subsection" => "id_subsection"
                        ],
                        "select" => $this->select
            ]);
        } else if (!empty($_GET)) {
            $model = new $class();
            //Yii::$app->json->field_id = "date_created";
            return Yii::$app->json->search($model, ["is_show" => $status, "id_language" => Language::getLanguageId($language)], [
                        "path" => $this->type_resource,
                        "type" => $this->type_resource,
                        "language" => $language,
                        "conformity" => [ //соответсвие полей в запросе к базе
                            "agglomeration" => "id_city",
                            "section" => "id_section",
                            "subsection" => "id_subsection"
                        ],
                        "select" => $this->select
            ]);
        } else
            return $this->today($language, $temp);
    }

    //portblog/news/today?agglomeration=3
    public function today($language = "ru", $temp = false) {
        $class = $this->path_to_module."Material";
        $date = date("Y-m-d", time());
        $model = new $class();
        $status = ($temp) ? $class::STATUS_DRAFT : $class::STATUS_PUBLISHED;
        Yii::$app->json->field_id = "date_public";
        return Yii::$app->json->search($model, ["and", [">=", "date_public", $date], ["is_show" => $status, "id_language" => Language::getLanguageId($language)]], [
                    "path" => $this->type_resource."/" . $date,
                    "type" => $this->type_resource,
                    "language" => $language,
                    "conformity" => [ //соответсвие полей в запросе к базе
                        "agglomeration" => "id_city",
                        "section" => "id_section",
                        "subsection" => "id_subsection"
                    ],
                    "select" => $this->select
        ]);
    }

    public function rss($language = "ru", $temp = false) {
        $json = $this->today($language, $temp);
        $data = json_decode($json);
        Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
        return $data;
    }

    // /news/view?id=1
    public function view($id = false, $language = "ru") {
        $class = $this->path_to_module."Material";
        $material = $class::getMaterialWithComments($id);
        return $material ? Yii::$app->json->build([$material], false, true) : false;
    } 
    
    public function menu($language = "ru") {
        $class = $this->path_to_module."Section";
        if ($json = Yii::$app->json->getFile($this->type_resource."/menu", $language))
            return $json;
        else if ($menu = $class::getItemsList($language)) {
            Yii::$app->json->saveToFile($menu, $this->type_resource."/menu", $language);
            return Yii::$app->json->build($menu, false, true);
        }
    }
    
    public function comment_set($data = []) {
        $class = $this->path_to_module."Comment";
        $class_material = $this->path_to_module."Material";
        $model = new $class;
        if ($model->prepare($data)) {
            if ($model->id_comment === "null")
                $model->id_comment = NULL;
            $model->date_created = date("Y-m-d G:i:s", time());
            if ($model->validate()) {
                if ($model->save()) {
                    Yii::$app->json->clearCache($this->type_resource."/".$model->id_material.".json");
                    return Yii::$app->json->build(["id" => $model->id]);
                }
            }
        }
        //Yii::$app->debug->show($model->getErrors());
        return Yii::$app->json->build([], $model->getErrors());
    }
    
}


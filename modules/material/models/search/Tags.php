<?php

namespace app\modules\material\models\search;

use Yii;

/**
 * This is the model class for table "material_tags".
 *
 * @property integer $id
 * @property string $material
 * @property string $name
 * @property integer $frequency
 */
class Tags extends \app\modules\material\models\Tags {

    public function rules() {
        return [
            [['name', 'frequency', 'material'], 'safe'],
        ];
    }

}

<?php

namespace app\modules\material\models;

use Yii;
use app\models\Language;
use app\components\Settings;
use app\models\ActiveRecord;

/**
 * This is the model class for table "section".
 *
 * @property integer $id
 * @property integer $id_language
 * @property integer $id_main
 * @property string $name
 * @property string $comment
 */
class Section extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_language', 'name'], 'required'],
            [['id_language', 'id_main'], 'integer'],
            [['name',], 'string', 'max' => 100],
            [['slug'], 'unique'],
            [['seo_title', 'seo_description', 'seo_keywords', 'seo_site', 'seo_other', 'comment'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_language' => Yii::t('material', 'Language'),
            'name' => Yii::t('material', 'Name'),
            'comment' => Yii::t('material', 'Comment'),
            'seo_title' => Yii::t("material", 'Seo Title'),
            'seo_description' => Yii::t("material", 'Seo Description'),
            'seo_keywords' => Yii::t("material", 'Seo Keywords'),
            'seo_site' => Yii::t("material", 'Seo Site'),
            'seo_other' => Yii::t("material", 'Seo Other'),
            'slug' => Yii::t("material", 'Path'),
        ];
    }
    
    function behaviors() {
        return [
            'slug' => [
                    'class' => 'app\modules\material\components\behaviors\Slug',
                    'in_attribute' => 'name',
                    'out_attribute' => 'slug',
                    'translit' => true
            ]
        ];
    }
    
    public function getPath($material) {
        return @$this->language->type."/".$material."/".$this->slug;
    }
    
    public function getMaterials() {
        $class = Settings::getInstance()->getModulePath()."\models\Material";
        return $this->hasMany($class::className(), ['id_section' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'id_language']);
    }

    public static function getItemsList($language = "ru") {
        $class = Settings::getInstance()->getModulePath()."\models\Section";
        $class_subsection = Settings::getInstance()->getModulePath()."\models\SubSection";
        $select = [
            "id", 
            "name", 
            "slug",
            'seo_title',
            'seo_description',
            'seo_keywords',
            'seo_site',
        ];
        $list[] = ["id" => 0, "slug" => "all", "name" => Yii::t('common', "Все"), 'seo_title' => false, 'seo_description' => false, 'seo_keywords' => false, 'seo_site' => false,];
        $items = $class::find()->where(['id_language' => Language::getLanguageId($language)])->all();
        foreach ($items as $item) {
            $_item = $item->fieldsValue($language, $select);
            $sublist = [];
            $subitems = $class_subsection::find()->where(["id_section" => $item->id])->all();
            foreach ($subitems as $sub)
                $sublist[] = $sub->fieldsValue($language, $select);
            $_item['items'] = $sublist;
            $list[] = $_item;
        }
        //Yii::$app->debug->show($list);
        return $list;
    }

    public static function getLocalList($id = false) {
        $class = Settings::getInstance()->getModulePath()."\models\Section";
        return $class::find()->where(["id_main" => $id])->orderBy("id_language ASC")->asArray()->all();
    }

}

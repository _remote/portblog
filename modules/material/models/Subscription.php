<?php

namespace app\modules\material\models;

use Yii;
use app\components\Settings;
use app\models\ActiveRecord;

/**
 * This is the model class for table "news_subscriptions".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_material
 * @property integer $id_city
 * @property integer $id_section
 * @property integer $id_subsection
 * @property string $date_create
 */
class Subscription extends ActiveRecord {
    
    public $type;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_user'], 'required'],
            [['id_user', 'id_material', 'id_city', 'id_section', 'id_subsection'], 'integer'],
            [['date_create'], 'safe'],
            [['id_material'], 'uniqueValue'],
            [['id_city'], 'uniqueValue'],
            [['id_section'], 'uniqueValue'],
            [['id_subsection'], 'uniqueValue'],
            [['id_user'], 'isNotNull'],
            [['type'], 'string'],
//            [['id_user', 'id_material'], 'unique', 'targetAttribute' => ['id_material'], 'message' => Yii::t('material', 'The combination of Id User and Id material has already been taken.')],
//            [['id_user', 'id_city'], 'uniqueCity', 'message' => Yii::t('material', 'The combination of Id User and Id City has already been taken.')],
//            [['id_user', 'id_section'], 'unique', 'targetAttribute' => ['id_user', 'id_section'], 'message' => Yii::t('material', 'The combination of Id User and Id Section has already been taken.')],
//            [['id_user', 'id_subsection'], 'unique', 'targetAttribute' => ['id_user', 'id_subsection'], 'message' => Yii::t('material', 'The combination of Id User and Id Sub Section has already been taken.')]
        ];
    }
    
    public function uniqueValue ($attribute, $params) {
        $labels = $this->attributeLabels();
        if (self::find()->where(["id_user" => $this->id_user, "{$attribute}" => $this->$attribute])->count())
            $this->addError($attribute, Yii::t('material', 'The combination of User and {field} has already been taken.', ["field" => @$labels[$attribute]]));
    }
    
    public function isNotNull($attribute, $params) {
        //Yii::$app->debug->show($this);
        if (empty($this->id_material) && empty($this->id_city) && empty($this->id_section) && empty($this->id_subsection))
            $this->addError ($attribute, Yii::t("material", "Please enter at least one criterion for subscription"));
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_user' => Yii::t('material', 'User'),
            'id_material' => Yii::t('material', 'Material'),
            'id_city' => Yii::t('material', 'City'),
            'id_section' => Yii::t('material', 'Section'),
            'id_subsection' => Yii::t('material', 'Sub Section'),
            'date_create' => Yii::t('material', 'Date Create'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterial() {
        $class = Settings::getInstance()->getModulePath()."\models\Material";
        return $this->hasOne($class::className(), ['id' => 'id_material']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection() {
        $class = Settings::getInstance()->getModulePath()."\models\Section";
        return $this->hasOne($class::className(), ['id' => 'id_section']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubsection() {
        $class = Settings::getInstance()->getModulePath()."\models\SubSection";
        return $this->hasOne($class::className(), ['id' => 'id_subsection']);
    }

}

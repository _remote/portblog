<?php

namespace app\modules\material\models;

use Yii;
use app\models\Language;
use app\components\Settings;
use app\models\ActiveRecord;

/**
 * This is the model class for table "subsection".
 *
 * @property integer $id
 * @property integer $id_section
 * @property integer $id_language
 * @property integer $id_main
 * @property string $name
 * @property string $comment
 */
class SubSection extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_section', 'id_language', 'name'], 'required'],
            [['id_section', 'id_language', 'id_main'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['slug'], 'unique'],
            [['seo_title', 'seo_description', 'seo_keywords', 'seo_site', 'seo_other', 'comment'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_section' => Yii::t('material', 'Owner Type'),
            'id_language' => Yii::t('material', 'Language'),
            'name' => Yii::t('material', 'Name'),
            'comment' => Yii::t('material', 'Comment'),
            'seo_title' => Yii::t("material", 'Seo Title'),
            'seo_description' => Yii::t("material", 'Seo Description'),
            'seo_keywords' => Yii::t("material", 'Seo Keywords'),
            'seo_site' => Yii::t("material", 'Seo Site'),
            'seo_other' => Yii::t("material", 'Seo Other'),
            'slug' => Yii::t("material", 'Path'),
        ];
    }
    
    function behaviors() {
        return [
            'slug' => [
                    'class' => 'app\modules\material\components\behaviors\Slug',
                    'in_attribute' => 'name',
                    'out_attribute' => 'slug',
                    'translit' => true
            ]
        ];
    }
    
    public function getPath($material) {
        return @$this->language->type."/".$material."/".@$this->ownertype->slug."/".$this->slug;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwnertype() {
        $class = Settings::getInstance()->getModulePath()."\models\Section";
        return $this->hasOne($class::className(), ['id' => 'id_section']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'id_language']);
    }

    public static function getLocalList($id = false) {
        $class = Settings::getInstance()->getModulePath()."\models\SubSection";
        return $class::find()->where(["id_main" => $id])->orderBy("id_language ASC")->asArray()->all();
    }

}

<?php
use yii\helpers\Url;
use yii\helpers\Html;

use app\components\Settings;

//use app\modules\news\models\Material;
//use app\modules\news\models\Section;
//use app\modules\news\models\SubSection;

$class_material = Settings::getInstance()->getModulePath()."\models\Material";
$class_section = Settings::getInstance()->getModulePath()."\models\Section";
$class_subsectioin = Settings::getInstance()->getModulePath()."\models\SubSection";


if (empty($item['id']))
    return;

$link = false;
if (isset($level) && $level > 0) {
    $p = 1;
    $sub = "alert-info sub";
    $count = $class_material::find()->where(["id_subsection" => $item['id']])->count();
    if ($main = $class_subsectioin::findOne(["id" => $item['id']]))
        $link = Url::to(['/admin/'.Settings::getInstance()->getModuleId().'/subsection/update', "id" => $main->id_main, "language" => $language]);
}
else {
    $p = 0;
    $sub = "alert-success";
    $count = $class_material::find()->where(["id_section" => $item['id']])->count();
    if ($main = $class_section::findOne(["id" => $item['id']]))
        $link = Url::to(['/admin/'.Settings::getInstance()->getModuleId().'/section/update', "id" => $main->id_main, "language" => $language]);
}
?>
<div class="alert <?= $sub ?>" id="<?= $item['id'] ?>">
    <?=
    Html::a('x', ['delete-menu-item', 'id' => $item['id'], "sub" => $p, "language" => $language], [
        'data-confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
        'data-pjax' => '0', 
        'class' => 'close grid-action'
    ]);
    ?>
    <?= ($link)?Html::a($item['name']." ({$count})", $link, ["target" => "_blank"]):$item['name'] ?>
</div>


<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

use app\components\Settings;


$this->title = strip_tags(trim($model->title));
$this->params['breadcrumbs'][] = ['label' => Yii::t(Settings::getInstance()->getModuleId(), 'Materials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'label' => Yii::t('material', 'Autor'),
                'value' => !empty($model->user->first_name)?$model->user->first_name." (".$model->user->email.")":false,
            ],
            [
                'label' => Yii::t('material', 'Language'),
                'format' => 'raw',
                'value' => !empty($model->language->name)?Html::a($model->language->name, ["/admin/language/view", "id" => $model->language->id]):false,
            ],
            //'title',
            'short_text' => [
                'attribute' => 'short_text',
                'format' => 'raw',
                'value' => strip_tags(trim($model->short_text)),
            ],
            //'text:ntext',
            'date_created',
        ],
    ]) ?>

    <h1>Комментарии</h1>
    <?= 
    $this->render("_list", [
        'dataProvider' => $commentProvider,
        "model" => $commentModel,
        "levels" => $levels
    ]) 
    ?>
</div>

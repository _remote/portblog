<?php
use yii\widgets\Pjax;

if (!defined("show_menu_function") and define('show_menu_function', true)) {
    function showMenu($view, $menu, $language = "ru", $level = 0) {
        $result = "";
        foreach ($menu as $item) {
            $result .= $view->render("_menu_item", [
                "item" => $item,
                "level" => $level,
                "language" => $language,
            ]);

            if (isset($item['items']) && is_array($item['items'])) 
                $result .= showMenu($view, $item['items'], $language, ($level + 1));
        }
        return $result;
    }
} 

Pjax::begin(['id' => '_menu_items_'.$language, 'options' => ['class' => 'pjax-wraper']]);
    echo showMenu($this, $menu, $language);
Pjax::end();

<?php

use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\widgets\ActiveForm;

use app\models\Language;
use app\components\Settings;

$this->title = Yii::t(Settings::getInstance()->getModuleId(), 'Update material: ') . ' ' . strip_tags(trim(html_entity_decode($main->title)));
$this->params['breadcrumbs'][] = ['label' => Yii::t(Settings::getInstance()->getModuleId(), 'Materials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => strip_tags(trim($main->title)), 'url' => ['view', 'id' => $main->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="material-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="material-form">
   
        <?php 
        $form = ActiveForm::begin([
            'enableClientValidation' => false,
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]);
        ?>

        <?php
        $items = [];
        foreach ($languages as $id => $type) {
            $items[] = [
                'options' => ['id' => 'lng-'.$id],
                'label' => Language::getTitleOfLanguage($id),
                'content' => $this->render('_form', [
                    'model' => $models[$id],
                    'id_language' => $id,
                    'form' => $form
                ]),
                //'active' => true
            ];
        }
        echo TabsX::widget([
            'items' => $items,
            'position'=>TabsX::POS_ABOVE,
            'encodeLabels'=>false,
        ]);
        ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('common', 'Update') , ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>

<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\widgets\Pjax;

use app\models\Language;
use app\components\Settings;

$this->title = Yii::t(Settings::getInstance()->getModuleId(), 'Section menu');
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
    .alert {
        padding: 5px 10px;
        margin-bottom: 3px;
    }
    
    .alert.sub {
        padding: 5px 10px 5px 40px;
    }
</style>
<div class="menu-index">
    <div class="alert alert-warning" role="alert">
        <?= Yii::t("menu", "Menu is based on elements of sections and subsections. Removing menu items may result in not displaying the correct material.") ?>
    </div>
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a(Yii::t('material', 'View sections'), ['/admin/'.Settings::getInstance()->getModuleId().'/section/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('material', 'View subsections'), ['/admin/'.Settings::getInstance()->getModuleId().'/subsection/index'], ['class' => 'btn btn-primary']) ?>
    </p>
    
    <?php
    $items = [];
    foreach ($menus as $id_language => $menu) {
        $language = Language::getTypeOfLanguage($id_language);
        $items[] = [
            'options' => ['id' => 'lng-'.$id_language],
            'label' => Language::getTitleOfLanguage($id_language),
            'content' => $this->render("_menu_language", [
                "menu" => $menu,
                "language" => $language,
            ]),
        ];
    }
    
    echo TabsX::widget([
        'items' => $items,
        'position'=>TabsX::POS_ABOVE,
        'encodeLabels'=>false,
    ]);
    ?>
</div>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\tabs\TabsX;
use yii2mod\bxslider\BxSlider;
use yii\web\JsExpression;

use app\models\Language;
use app\components\Settings;
use app\models\WorldCountries;
use app\models\WorldCities;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = strip_tags(trim(html_entity_decode($main->title)));
$this->params['breadcrumbs'][] = ['label' => Yii::t(Settings::getInstance()->getModuleId(), 'Materials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$gallery = $main->buildSlider();
?>
<div class="material-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $main->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $main->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?php
        if (!$gallery)
            echo Html::a(Yii::t("material", "Add gallery"), ['/admin/'.Settings::getInstance()->getModuleId().'/images/add', 'id' => $main->id], ['class' => 'btn btn-success']);
        ?>
    </p>
    <?= $gallery ?>
    <p>
        <?php 
        if ($gallery) {
            echo Html::a(Yii::t("material", "Update gallery"), ['/admin/'.Settings::getInstance()->getModuleId().'/images/update', 'id' => $main->id], ['class' => 'btn btn-primary']);
            echo " ".Html::a(Yii::t("material", "Delete gallery"), ['/admin/'.Settings::getInstance()->getModuleId().'/images/delete-gallery', 'id' => $main->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
            ]);
        }
        
        ?>
    </p>
    <?php
    
    
    $items = [];
    foreach ($models as $model) {
        $items[] = [
            'options' => ['id' => 'lng-'.$model->id_language],
            'label' => Language::getTitleOfLanguage($model->id_language),
            'content' => "<h1 style=\"margin-top: 0px;\">{$model->title}</h1>".DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'id',
                    'date_public',
                    'image' => [
                        'label' => Yii::t("material", 'Image'),
                        'format' => 'raw',
                        'value' => Html::img($model->image, ['class' => 'img-thumbnail', "width" => 150])
                    ],
                    'id_area' => [
                        'label' => Yii::t("material", 'Country'),
                        'format' => 'raw',
                        'value' => Html::a(WorldCountries::getName($model->id_area), ["/admin/countries/view", "id" => $model->id_area]),
                    ],
                    'id_city' => [
                        'label' => Yii::t("material", 'City'),
                        'format' => 'raw',
                        'value' => Html::a(WorldCities::getName($model->id_city), ["/admin/cities/view", "id" => $model->id_city]),
                    ],
                    
                    [
                        'label' => Yii::t("material", 'Language'),
                        'format' => 'raw',
                        'value' => !empty($model->language->name)?Html::a($model->language->name, ["/admin/language/view", "id" => $model->language->id]):false,
                    ],
                    [
                        'label' => Yii::t("material", 'Autor'),
                        'value' => !empty($model->user->first_name)?$model->user->first_name." (".$model->user->email.")":false,
                    ],
                    'title' => [
                        'attribute' => 'title',
                        'format' => 'raw',
                        'value' => strip_tags(trim($model->title)),
                    ],
                    'telephone' => [
                        //'label' => Yii::t("material", 'Telephone'),
                        'attribute' => 'telephone',
                        'format' => 'raw',
                        //'value' => isset($model->telephone)?$model->telephone:false,
                        'visible' => Settings::getInstance()->visible(["events", "places"]),
                    ],
                    'work_time' => [
                        //'label' => Yii::t("material", 'Work time'),
                        'attribute' => 'work_time',
                        'format' => 'raw',
                        //'value' => isset($model->work_time)?$model->work_time:false,
                        'visible' => Settings::getInstance()->visible(["events", "places"]),
                    ],
                    'price' => [
                        //'label' => Yii::t("material", 'Price'),
                        'attribute' => 'price',
                        'format' => 'raw',
                        //'value' => isset($model->price)?$model->price:false,
                        'visible' => Settings::getInstance()->visible(["events", "places"]),
                    ],
                    [
                        'label' => Yii::t("material", 'Section'),
                        'value' => !empty($model->section->name)?$model->section->name:false,
                    ],
                    [
                        'label' => Yii::t("material", 'Sub-Section'),
                        'value' => !empty($model->subsection->name)?$model->subsection->name:false,
                    ],
                    'short_text' => [
                        'attribute' => 'short_text',
                        'format' => 'raw',
                        'value' => strip_tags(trim($model->short_text)),
                    ],
                    //'text:ntext',
                    'image',
                    'video',
                    'date_created',
                    'date_update',
                    'site',
                    'seo_title',
                    'seo_description',
                    'seo_keywords',
                    'view_count',
                    'is_show' => [
                        'attribute' => 'is_show',
                        'value' => $model::getStatusName($model->is_show),
                    ],
                    'is_show_in_top' => [
                        'attribute' => 'is_show_in_top',
                        'value' => ($model->is_show_in_top)?Yii::t('common', "yes"):Yii::t('common', "no"),
                    ],
                    'is_selected' => [
                        'attribute' => 'is_selected',
                        'value' => ($model->is_selected)?Yii::t('common', "yes"):Yii::t('common', "no"),
                    ],
                    'gallery' => [
                        'attribute' => 'gallery',
                        'format' => 'raw',
                        //'value' => $gallery,
                    ],
                    'rating',
                    'comment',
                ],
            ]),
        ];
    }
    echo TabsX::widget([
        'items' => $items,
        'position'=>TabsX::POS_LEFT,
        'encodeLabels'=>false,
    ]);
    ?>

</div>

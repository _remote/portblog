<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\components\Settings;

/* @var $this yii\web\View */
/* @var $model app\models\NewsSubscriptions */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t(Settings::getInstance()->getModuleId(), 'Materials'), 'url' => ['/admin/'.Settings::getInstance()->getModuleId().'/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('material', 'Subscriptions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-subscriptions-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_user',
            'id_material',
            'id_city',
            'id_section',
            'id_subsection',
            'date_create',
        ],
    ]) ?>

</div>

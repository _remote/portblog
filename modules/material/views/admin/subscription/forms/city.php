<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;

use app\models\User;
use app\components\Settings;
?>


<?php $form = ActiveForm::begin(); ?>

<?php
echo $form->field($model, 'type')->hiddenInput(['value' => 'city'])->label(false);
echo $form->field($model, 'id_user', [
        //'template' =>  "{label} ".Html::a('', ['admin/section/create'], ['class' => 'glyphicon glyphicon-plus action-button'])." {input}" 
    ])->widget(Select2::classname(), [
    'options' => [
        'id'=>'user-id-city',
        'placeholder' => Yii::t('material', 'Select user..'),
    ],
    'data' => ArrayHelper::map(User::find()->select(["CONCAT(first_name,' (',email,')') as first_name", 'id'])->orderBy("`first_name` ASC")->all(), 'id', 'first_name')
])->label(Yii::t('material', 'User'));
?>

<?= $form->field($model, 'id_city')->textInput(["disabled" => "disabled"])->label("City (пока не поддерживается)") ?>

<div class="form-group">
    <?= Html::submitButton(Yii::t('common', 'Create'), ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>


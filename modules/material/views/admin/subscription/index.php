<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

use app\models\User;
use app\components\Settings;

//use app\modules\news\models\Section;
//use app\modules\news\models\SubSection;
$class_section = Settings::getInstance()->getModulePath()."\models\Section";
$class_subsectioin = Settings::getInstance()->getModulePath()."\models\SubSection";

$this->title = Yii::t('material', 'Subscriptions');
$this->params['breadcrumbs'][] = ['label' => Yii::t(Settings::getInstance()->getModuleId(), 'Materials'), 'url' => ['/admin/'.Settings::getInstance()->getModuleId().'/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-subscriptions-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('material', 'Create Subscription'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_user' => [
                'attribute' => 'id_user',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a(User::getNameOfUser($model->id_user), ["/admin/user/view", "id" => $model->id_user]);
                },
                'filter' => ArrayHelper::map(
                    User::find()
                        ->orderBy('id')
                        ->asArray()->all(),
                    'id', 'first_name'),
                'options' => ['width' => '120'],
                'label' => Yii::t('common', "User")
            ],
            'id_material' => [
                'attribute' => 'id_material',
                'format' => 'raw',
                'value' => function($model) {
                    return !empty($model->material)?Html::a($model->material->title, ['admin/'.Settings::getInstance()->getModuleId().'/view', "id" => $model->id_material]):false;
                },
                //'visible' => Settings::getInstance()->hidden("blogs")
            ],
//            'id_city' => [
//                'attribute' => 'id_city',
//                'visible' => Settings::getInstance()->hidden("blogs")
//            ],
            [
                'attribute' => 'id_section',
                'format' => 'raw',
                'value' => function($model) {
                    return !empty($model->section)?Html::a($model->section->name, ['admin/'.Settings::getInstance()->getModuleId().'/section/view', "id" => $model->id_section]):false;
                },
                'filter' => ArrayHelper::map(
                    $class_section::find()
                        ->orderBy('id')
                        ->asArray()->all(),
                    'name','name'),
                'options' => ['width' => '120'],
                'visible' => Settings::getInstance()->hidden("blogs")
            ],
            [
                'attribute' => 'id_subsection',
                'format' => 'raw',
                'value' => function($model) {
                    return !empty($model->subsection)?Html::a($model->subsection->name, ['admin/'.Settings::getInstance()->getModuleId().'/subsection/view', "id" => $model->id_subsection]):false;
                },
                'filter' => ArrayHelper::map(
                    $class_subsectioin::find()
                        ->orderBy('id')
                        ->asArray()->all(),
                    'name','name'),
                'options' => ['width' => '120'],
                'visible' => Settings::getInstance()->hidden("blogs")
            ],
            'date_create',

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'V',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{view}'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'U',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{update}'
            ],
                [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'D',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{delete}'
            ],
        ],
    ]); ?>

</div>

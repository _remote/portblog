<?php

use yii\helpers\Html;

use app\components\Settings;


/* @var $this yii\web\View */
/* @var $model app\models\NewsSubscriptions */

$this->title = Yii::t('material', 'Create Subscription');
$this->params['breadcrumbs'][] = ['label' => Yii::t(Settings::getInstance()->getModuleId(), 'Materials'), 'url' => ['/admin/'.Settings::getInstance()->getModuleId().'/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('material', 'Subscriptions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-subscriptions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_create', [
        'model' => $model,
    ]) ?>

</div>

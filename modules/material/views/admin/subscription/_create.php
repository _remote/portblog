<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use kartik\tabs\TabsX;

use app\models\User;
use app\components\Settings;

//use app\modules\news\models\Section;
//use app\modules\news\models\SubSection;
$class = Settings::getInstance()->getModulePath()."\models\Material";
$class_section = Settings::getInstance()->getModulePath()."\models\Section";
$class_subsectioin = Settings::getInstance()->getModulePath()."\models\SubSection";
?>

<div class="material-subscriptions-forms">
    <?php
    $items = [
        [
            'label' => Yii::t("material", "Material"),
            'content' => $this->render("forms/material", ["model" => $model]),
            'active' => ($model->type == 'material')?true:false,
        ],
        [
            'label' => Yii::t("material", "City"),
            'content' => $this->render("forms/city", ["model" => $model]),
            'active' => ($model->type == 'city')?true:false,
        ],
        [
            'label' => Yii::t("material", "Section"),
            'content' => $this->render("forms/section", ["model" => $model]),
            'active' => ($model->type == 'section')?true:false,
        ],
    ];
    
    echo TabsX::widget([
        'items' => $items,
        'position'=>TabsX::POS_ABOVE,
        'encodeLabels'=>false,
    ]);
    ?>
</div>

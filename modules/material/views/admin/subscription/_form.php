<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;

use app\models\User;
use app\components\Settings;

//use app\modules\news\models\Section;
//use app\modules\news\models\SubSection;
$class = Settings::getInstance()->getModulePath()."\models\Material";
$class_section = Settings::getInstance()->getModulePath()."\models\Section";
$class_subsectioin = Settings::getInstance()->getModulePath()."\models\SubSection";
?>

<div class="material-subscriptions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    echo $form->field($model, 'id_user', [
            //'template' =>  "{label} ".Html::a('', ['admin/section/create'], ['class' => 'glyphicon glyphicon-plus action-button'])." {input}" 
        ])->widget(Select2::classname(), [
        'options' => [
            'id'=>'user-id',
            'placeholder' => Yii::t('material', 'Select user..'),
        ],
        'data' => ArrayHelper::map(User::find()->select(["CONCAT(first_name,' (',email,')') as first_name", 'id'])->orderBy("`first_name` ASC")->all(), 'id', 'first_name')
    ])->label(Yii::t('material', 'User'));
    ?>
    
    <?php
    echo $form->field($model, 'id_material', [
            //'template' =>  "{label} ".Html::a('', ['admin/section/create'], ['class' => 'glyphicon glyphicon-plus action-button'])." {input}" 
        ])->widget(Select2::classname(), [
        'options' => [
            'id'=>'material-id',
            'placeholder' => Yii::t('material', 'Select a material..')
        ],
        'data' => ArrayHelper::map($class::find()->select(['title', 'id'])->orderBy("`title` ASC")->all(), 'id', 'title')
    ]);
    ?>

    <?= $form->field($model, 'id_city')->textInput(["disabled" => "disabled"])->label("City (пока не поддерживается)") ?>
    
    <?php
    echo $form->field($model, 'id_section', [
            //'template' =>  "{label} ".Html::a('', ['admin/section/create'], ['class' => 'glyphicon glyphicon-plus action-button'])." {input}" 
        ])->widget(Select2::classname(), [
        'options' => [
            'id'=>'section-id',
            'placeholder' => Yii::t('material', 'Select a section..')
        ],
        'data' => ArrayHelper::map($class_section::find()->select(['name', 'id'])->orderBy("`name` ASC")->all(), 'id', 'name')
    ]);
    
    $subsection = $class_subsectioin::find()->where(["id_section" => $model->id_section])->asArray()->all();
    echo $form->field($model, 'id_subsection', [
            //'template' =>  "{label} ".Html::a('', ['admin/subsection/create'], ['class' => 'glyphicon glyphicon-plus action-button'])." {input}" 
        ])->widget(DepDrop::classname(), [
        'data' => @ArrayHelper::map($subsection, 'id', 'name'),
        'options'=>['id'=>'subtype-id'],
        'type' => DepDrop::TYPE_SELECT2,
        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
        'pluginOptions'=>[
            'depends'=>['section-id'],
            'placeholder'=>Yii::t('material', 'Select a subsection..'),
            'url'=>Url::to(['/admin/'.Settings::getInstance()->getModuleId().'/sublist'])
        ]
    ])->label("Sub section");
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

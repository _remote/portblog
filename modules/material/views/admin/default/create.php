<?php

use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\widgets\ActiveForm;

use app\models\Language;
use app\components\Settings;

$this->title = Yii::t(Settings::getInstance()->getModuleId(), 'Create material');
$this->params['breadcrumbs'][] = ['label' => Yii::t(Settings::getInstance()->getModuleId(), 'Materials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-create">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="material-form">
   
        <?php 
        $form = ActiveForm::begin([
            'enableClientValidation' => false,
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]);
        ?>

        <?php
        $items = [];
        foreach ($languages as $id => $type) {
            $items[] = [
                'options' => ['id' => 'lng-'.$id],
                'label' => Language::getTitleOfLanguage($id),
                'content' => $this->render('_form', [
                    'model' => $models[$id],
                    'id_language' => $id,
                    'form' => $form
                ]),
                //'active' => true
            ];
        }
        echo TabsX::widget([
            'items' => $items,
            'position'=>TabsX::POS_ABOVE,
            'encodeLabels'=>false,
        ]);
        ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('common', 'Create') , ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use yii\web\JsExpression;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\file\FileInput;
use kartik\touchspin\TouchSpin;

use app\components\Settings;

use app\models\Language;
use app\models\WorldCountries;
use app\models\WorldCities;
use app\models\User;

use app\modules\files\models\Icons;

//use app\modules\news\models\Section;
//use app\modules\news\models\SubSection;

$class_section = Settings::getInstance()->getModulePath()."\models\Section";
$class_subsectioin = Settings::getInstance()->getModulePath()."\models\SubSection";


$id_language = isset($id_language)?$id_language:false;
?>

    <?php
    if ($model->hidden("blogs")) {      
        echo $form->field($model, "[{$id_language}]id_area", [
                'template' =>  "{label} ".Html::a('', ['/admin/countries/create', "language" => Language::getTypeOfLanguage($model->id_language)], ['class' => 'glyphicon glyphicon-plus action-button', 'target' => "_blank"])." {input}" 
            ])->widget(Select2::classname(), [
            'options' => [
                'id'=>"area-id{$id_language}",
                'placeholder' => Yii::t('material', 'Select a country ..')
            ],
            'data' => ArrayHelper::map(WorldCountries::find()->select(['name', 'id'])->where(["id_language" => $model->id_language])->orderBy("`name` DESC")->all(), 'id', 'name')

        ]);
            
        $cities = WorldCities::find()->where(["country" => $model->id_area])->asArray()->all();
        echo $form->field($model, "[{$id_language}]id_city", [
                'template' =>  "{label} ".Html::a('', ['/admin/cities/create'], ['class' => 'glyphicon glyphicon-plus action-button', 'target' => "_blank"])." {input}" 
            ])->widget(DepDrop::classname(), [
            'data' => @ArrayHelper::map($cities, 'id', 'name'),
            'options'=>['id'=>"cities-id{$id_language}"],
            'type' => DepDrop::TYPE_SELECT2,
            'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
            'pluginOptions'=>[
                'depends'=>["area-id{$id_language}"],
                'placeholder'=>Yii::t('material', 'Select a subsection..'),
                'url'=>Url::to(['citieslist', "id_language" => $model->id_language])
            ]
        ]);
    }      
    ?>

    <?= $form->field($model, "[{$id_language}]id_language")->hiddenInput()->label(false) ?>

    <?php 
    if ($model->hidden("blogs")) { 
        echo $form->field($model, "[{$id_language}]id_user")->hiddenInput(["value" => Yii::$app->user->id])->label(false);
    }
    else {
        echo $form->field($model, "[{$id_language}]id_user")->widget(Select2::classname(), [
            'options' => [
                'placeholder' => Yii::t('material', 'Select a user ..')
            ],
            'data' => ArrayHelper::map(User::find()->select(["CONCAT(first_name,' (',email,')') as first_name", 'id'])->orderBy("`first_name` ASC")->all(), 'id', 'first_name')
        ])->label(Yii::t('material', 'User'));
    }
    ?>
    
    <?php
    if ($model->hidden("blogs")) {       
        echo $form->field($model, "[{$id_language}]id_section", [
                'template' =>  "{label} ".Html::a('', ['/admin/'.Settings::getInstance()->getModuleId().'/section/create', "id_language" => $id_language], ['class' => 'glyphicon glyphicon-plus action-button', 'target' => "_blank"])." {input}" 
            ])->widget(Select2::classname(), [
            'options' => [
                'id'=>"section-id{$id_language}",
                'placeholder' => Yii::t('material', 'Select a section ..')
            ],
            'data' => ArrayHelper::map($class_section::find()->select(['name', 'id'])->where(["id_language" => $model->id_language])->orderBy("`name` DESC")->all(), 'id', 'name')

        ]);

        $subsection = $class_subsectioin::find()->where(["id_section" => $model->id_section])->asArray()->all();
        echo $form->field($model, "[{$id_language}]id_subsection", [
                'template' =>  "{label} ".Html::a('', ['/admin/'.Settings::getInstance()->getModuleId().'/subsection/create', "id_language" => $id_language], ['class' => 'glyphicon glyphicon-plus action-button', 'target' => "_blank"])." {input}" 
            ])->widget(DepDrop::classname(), [
            'data' => @ArrayHelper::map($subsection, 'id', 'name'),
            'options'=>['id'=>"subtype-id{$id_language}"],
            'type' => DepDrop::TYPE_SELECT2,
            'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
            'pluginOptions'=>[
                'depends'=>["section-id{$id_language}"],
                'placeholder'=>Yii::t('material', 'Select a subsection..'),
                'url'=>Url::to(['sublist'])
            ]
        ]);
    }
    ?>
   
    <?= $form->field($model, "[{$id_language}]title")->textInput(); ?> 

    <?= $form->field($model, "[{$id_language}]short_text")->textArea(["rows" => 3]); ?>

    <?= 
    $form->field($model, "[{$id_language}]text")->widget(TinyMce::className(), [
        'options' => ['rows' => 10],
        'language' => 'ru',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern imagetools"
            ],
            'toolbar1' => "insertfile undo redo | styleselect | bullist numlist outdent indent | link image media | print code preview fullscreen",
            'toolbar2' => "fontselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | forecolor backcolor",
            'image_advtab' => true,
            "relative_urls" => false,
            "remove_script_host"=> false,
            'file_browser_callback'=> new yii\web\JsExpression("function(field_name, url, type, win) {
                if(type=='image') {
                    $('#upload_form_model input').val('{$model->getShortName()}');
                    $('#upload_form_object input').val({$model->id});
                    $('#upload_form input[type=file]').click();
                }
            }"),
        ],
        
    ]); 
    //$form->field($model, "[{$id_language}]text")->textarea(['rows' => 10]);
    ?>

    <?php 
    echo $form->field($model, "[{$id_language}]scenario")->hiddenInput()->label(false);
    echo $form->field($model, "[{$id_language}]image_file")->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
            //'multiple'=>true
        ],
        'pluginOptions' => [
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  Yii::t('material', 'Select Photo'),
            'initialPreview'=> ($model->image_file)?Html::img($model->getThumbUploadUrl('image_file', 'preview'), ['class' => 'img-thumbnail']):($model->image?Html::img($model->image, ['class' => 'img-thumbnail']):false), 
            'overwriteInitial'=>true,
        ],
        'pluginEvents' => [
            "fileclear" => "function() { jQuery('#".$model->getShortName(true)."-{$id_language}-scenario').val('default');}",
            "fileloaded" => "function() { jQuery('#".$model->getShortName(true)."-{$id_language}-scenario').val('update')}",
        ],
    ]);
    echo $form->field($model, "[{$id_language}]image")->textInput(['maxlength' => true, "placeholder" => "Укажите ссылку на изображение.."])->label(false);
    //echo $model->image_file;
    ?>

    <?= $form->field($model, "[{$id_language}]video")->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, "[{$id_language}]site")->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, "[{$id_language}]seo_title")->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, "[{$id_language}]seo_description")->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, "[{$id_language}]seo_keywords")->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, "[{$id_language}]view_count")->textInput(["value" => 0]) ?>

    <?= $form->field($model, "[{$id_language}]is_show")->dropDownList(["0" => "Черновик", "1" => "Опубликована"]) ?>

    <?php if ($model->hidden("blogs")) { ?>
    <?= $form->field($model, "[{$id_language}]is_show_in_top")->checkbox(); ?>

    <?= $form->field($model, "[{$id_language}]is_selected")->checkbox(); ?>

    <?= $form->field($model, "[{$id_language}]is_show_on_main")->checkbox(); ?>
    <?php } ?>

    <?= $form->field($model, "[{$id_language}]template")->dropDownList($model::getTemplatesItems()) ?>

    <?php $form->field($model, "[{$id_language}]gallery")->checkbox(); ?>

    <?php    
    echo $form->field($model, "[{$id_language}]_temp_gallery[]", [
            'template' =>  "{label} ".((!$model->isNewRecord && $model->gallery)?$model->buildSlider():false)."{input}" 
        ])->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
            'multiple'=>true
        ],
        'pluginOptions' => [
            'showRemove' => false,
            'showUpload' => false,
            'overwriteInitial'=>true,
            //'uploadUrl' => Url::to(['/site/file-upload']),
        ],
    ]);
    ?>

    <?php
    if ($model->isShowDelivery()) {
        echo $form->field($model, "[{$id_language}]delivery_date", ['template' =>  "{label} {input}".Html::a(null, null, [
            'class' => 'glyphicon glyphicon-trash',
            'style' => 'margin-left: 5px;',
            'onclick' => "$('#material-{$id_language}-delivery_date').val(''); return false;",
        ])])->widget(\yii\jui\DatePicker::classname(), [
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
        ]);
    }
    ?>

    <?= $form->field($model, "[{$id_language}]hash_tag")->textInput() ?>

    <?= $form->field($model, "[{$id_language}]tags")->textInput() ?>

    <?php
    // Templating example of formatting each list element
    $icons_list = [];
    $icons = Icons::getList();
    foreach ($icons as $icon)
        $icons_list[$icon->id] = Html::img($icon->getThumbUploadUrl('file', 'preview_small'), ['class' => 'icon']) . " " . $icon->title;

    $escape = new JsExpression("function(m) { return m; }");
    echo $form->field($model, "[{$id_language}]id_icon", [
        'template' =>  "{label} ".Html::a('', ['/admin/icons/create'], ['class' => 'glyphicon glyphicon-plus action-button', 'target' => "_blank"])." {input}" 
    ])->widget(Select2::classname(), [
        'data' => $icons_list,
        'options' => ['placeholder' => Yii::t('material', 'Select a icon ...'), "class" => "select-icon"],
        'pluginOptions' => [
            'escapeMarkup' => $escape,
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, "[{$id_language}]rating")->widget(TouchSpin::classname(), [
        'pluginOptions' => [
            'initval' => 0.0,
            'buttonup_class' => 'btn btn-primary', 
            'buttondown_class' => 'btn btn-info', 
            'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>', 
            'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>',
            'min' => 0,
            'max' => 5,
            'step' => 0.1,
            'decimals' => 1,
            'boostat' => 5,
            'maxboostedstep' => 10,
        ],
    ]); ?>

    <?= $form->field($model, "[{$id_language}]js")->textArea() ?>

    <?= 
    $form->field($model, "[{$id_language}]date_public")->widget(\yii\jui\DatePicker::classname(), [
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]);
    ?>

    <?= $form->field($model, "[{$id_language}]comment")->textInput(['maxlength' => true]) ?>


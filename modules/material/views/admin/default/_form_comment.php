<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

use app\models\User;
use app\components\Settings;

//use app\modules\news\models\Comment;
$class_comment = Settings::getInstance()->getModulePath()."\models\Comment";
?>

<?php $form = ActiveForm::begin([
    'options' => ['data-pjax' => 1]
]); ?>
    <?php 
    echo $form->field($model, 'id_user')->widget(Select2::classname(), [
        'options' => [
            'id'=> $class_comment::getIdForInput(),
            'placeholder' => Yii::t('material', 'Select a user ..')
        ],
        'data' => ArrayHelper::map(User::find()->select(['first_name', 'id'])->orderBy("`first_name` ASC")->all(), 'id', 'first_name')
    ])->label(Yii::t('material', 'From user'));
    ?>
    <?= $form->field($model, 'id_comment')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'text')->textarea()->label(Yii::t('material', "Message")) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', "Create"), ['class' => 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>


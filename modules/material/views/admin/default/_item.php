<?php
use yii\helpers\Html;
use app\models\User;
use yii\widgets\Pjax;

use app\components\Settings;

//use app\modules\news\models\Comment;
$class_comment = Settings::getInstance()->getModulePath()."\models\Comment";

$_model = $model;
$model = new $class_comment();
$model->prepare($_model);
$username = User::getNameOfUser($model->id_user);
$user = User::findIdentity($model->id_user);
?>
<div class="comment row">
    <div class="user-box" style="margin-left: <?= isset($levels[$_model['id']])?(25 * ($levels[$_model['id']] - 1)):0 ?>px !important;">
        <?= Html::img($user->getThumbUploadUrl('avatar', 'preview'), ["width" => 47, "height" => 47, "class" => "avatar"]); ?>
    </div>
    <div class="col-md-8 _body">
        <div class="head">
            <div class="autor"><?= $username ?></div>
            <div class="date" title="<?= $model->date_created ?>"><?= $model->formatDate() ?></div>
        </div>
        <div class="text"><?= $model->text ?></div>
    </div>
    <ul class="buttons">
        <?php
        echo Html::a('<span class="glyphicon glyphicon-envelope"></span>', '#', [
            'data-toggle' => 'modal',
            'data-target' => "#commentForm",
            'data-id_comment' => $_model['id'],
            'data-title' => (strlen($model->text) > 20)?substr($model->text, 0, 19)."..":$model->text,
            //'data-pjax' => '0',
            //'class' => 'grid-action '
        ]);
        
        //$url = "?id={$model->id_material}&id_comment={$_model['id']}";
        echo Html::a('<span class="glyphicon glyphicon-trash"></span>', ["comment", "id" => $model->id_material, "id_comment" => $_model['id']], [
            'title' => Yii::t('common', "Delete"),
            'data-confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
            'data-pjax' => '0', 
            'class' => 'grid-action'
        ]);
         
        ?>
        <?php

        ?>
    </ul>
</div>


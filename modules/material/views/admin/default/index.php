<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use yii\widgets\Pjax;

use app\models\User;
use app\models\Language;
use app\components\Settings;
use app\models\WorldCountries;
use app\models\WorldCities;



//use app\modules\news\models\Material;
//use app\modules\news\models\Section;
//use app\modules\news\models\SubSection;
$class_material = Settings::getInstance()->getModulePath()."\models\Material";
$class_section = Settings::getInstance()->getModulePath()."\models\Section";
$class_subsection = Settings::getInstance()->getModulePath()."\models\SubSection";


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t(Settings::getInstance()->getModuleId(), 'Materials');
$this->params['breadcrumbs'][] = $this->title;
?>

<script type="text/javascript">
function addToDeliveryList(container, id) {
    var date = $(container).attr('rel');
    var input = $(container).after("<input type='text' class='delivery-date' id='datepicker-"+id+"'>");
    $("#datepicker-"+id).val(date);
    $("#datepicker-"+id).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd',
        yearRange: '1920:2020'
    });
    var button = $("#datepicker-"+id).after("<input type='button' id='set-delivery-button-"+id+"' value='+' onclick='setDeliveryDate("+id+")'>");
    $(container).hide();
}

function setDeliveryDate(id) {
    var date = $("#datepicker-"+id).val();
    //if (date) {
        $.post("delivery", {id:id, date:date}, function(data) {
            if (data.result) {
                $("#set-delivery-button-"+id).after(data.content);
//                $("##set-delivery-button-"+id).append("<input type='button' id='remove-delivery-"+id+"' value='-' onclick='removeDeliveryDate("+id+")'>");
                $("#datepicker-"+id).remove();
                $("#set-delivery-button-"+id).remove();
            }
            else bootbox.alert("Ошибка добавления в рассылку");
        }, "json");
    //}
}
</script>

<style type="text/css">
    .delivery-date {
        width: 90px;
    }
</style>

<div class="material-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t(Settings::getInstance()->getModuleId(), 'Create material'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t(Settings::getInstance()->getModuleId(), 'Material subscription'), ['/admin/'.Settings::getInstance()->getModuleId().'/subscription/index'], ['class' => 'btn btn-primary']) ?>
    </p>
    <?php
    Pjax::begin(['id' => 'grid-list', 'options' => ['class' => 'pjax-wraper']]);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => \kotchuprik\sortable\grid\Column::className(),
            ],
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            ['class' => 'yii\grid\CheckboxColumn'],
//            'id_area' => [
//                'attribute' => 'id_area',
//                'format' => 'raw',
//                'value' => function($model) {
//                    return WorldCountries::getName($model->id_area);
//                },
//                'filter' => ArrayHelper::map(
//                        WorldCountries::find()
//                        ->orderBy('id')
//                        ->asArray()->all(),
//                    'id', 'name'),
//                'options' => ['width' => '120'],
//            ],
//            'id_city' => [
//                'attribute' => 'id_city',
//                'format' => 'raw',
//                'value' => function($model) {
//                    return WorldCities::getName($model->id_city);
//                },
//                'filter' => ArrayHelper::map(
//                        WorldCities::find()
//                        ->orderBy('id')
//                        ->asArray()->all(),
//                    'id', 'name'),
//                'options' => ['width' => '120'],
//            ],
            'id_user' => [
                'attribute' => 'id_user',
                'format' => 'raw',
                'value' => function($model) {
                    return User::getNameOfUser($model->id_user);
                },
                'filter' => ArrayHelper::map(
                    User::find()
                        ->orderBy('id')
                        ->asArray()->all(),
                    'id', 'first_name'),
                'options' => ['width' => '120'],
            ],
            'title' => [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function($model) {
                    return strip_tags(trim($model->title));
                }
            ],
            [
                'attribute' => '_section',
                'format' => 'raw',
                'value' => function($model) {
                    return !empty($model->section)?Html::a($model->section->name, ["admin/section/view", "id" => $model->id_section]):false;
                },
                'filter' => ArrayHelper::map(
                    $class_section::find()
                        ->orderBy('id')
                        ->asArray()->all(),
                    'name','name'),
                'options' => ['width' => '120'],
                //'visible' => Settings::getInstance()->hidden("blogs")
            ],
            [
                'attribute' => '_sub_section',
                'format' => 'raw',
                'value' => function($model) {
                    return !empty($model->subsection)?Html::a($model->subsection->name, ["admin/subsection/view", "id" => $model->id_subsection]):false;
                },
                'filter' => ArrayHelper::map(
                    $class_subsection::find()//->where(['id_section'])
                        ->orderBy('id')
                        ->asArray()->all(),
                    'name','name'),
                'options' => ['width' => '120'],
                //'visible' => Settings::getInstance()->hidden("blogs")
            ],
             //'short_text',
             //'text:ntext',
            // 'image',
            // 'video',
             'date_public' => [
                'attribute' => 'date_public',
                'filter' => DatePicker::widget([
                    'name' => $searchModel->getShortName()."[date_public]",
                    'language' => 'ru', 
                    'dateFormat' => 'yyyy-MM-dd',
                    'value' => $searchModel->date_public,
                    'options' => ['style' => 'width:110px;'],
                ]),
                'format' => 'html',
                'options' => ['width' => '120'], 
             ],
            // 'date_update',
            // 'site',
            // 'seo_description',
            // 'seo_keywords',
            
//            'view_count' => [
//                'attribute' => 'view_count',
//                'label' => 'Views',
//                'filter' => false
//            ],
            'is_show' => [
                'attribute' => 'is_show',
                'filter' => $class_material::getStatusItems(),
                'format' => 'raw',
                'value' => function($model) {
                    return $model::getStatusName($model->is_show);
                }
            ],
            'delivery_date' => [
                'attribute' => 'delivery_date',
                'filter' => DatePicker::widget([
                    'name' => $searchModel->getShortName()."[delivery_date]",
                    'language' => 'ru', 
                    'dateFormat' => 'yyyy-MM-dd',
                    'value' => $searchModel->delivery_date,
                    'options' => ['style' => 'width:100px;'],
                ]),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::tag('span', !empty($model->delivery_date)?$model->delivery_date:Yii::t('material', 'Add to delivery') /*,['class' => 'glyphicon glyphicon-play']*/ ), ['delivery'], [
                        'id' => 'delivery-button-'.$model->id,
                        'rel' => $model->delivery_date,
                        'onclick' => "addToDeliveryList(this, {$model->id}); return false;",
                    ]);
                },
                'visible' => Settings::getInstance()->visible(["news", "events", "articles", "company"]),
            ],
//            [
//                'header' => 'Comments',
//                'format' => 'raw',
//                'value' => function($model) {
//                    return app\models\Comment::find()->where(["id_new" => $model->id])->count();
//                }
//            ],
            // 'is_show_in_top',
            // 'comment',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '<i class="glyphicon glyphicon-comment"><i>',
                'contentOptions' => ['style' => 'width:75px;'],
                'buttons'=>[
                    'view' => function ($url, $model) {
                        $class = Settings::getInstance()->getModulePath().'\models\Material';
                        $news = $class::getMaterialsList($model->id);
                        $icons = [];
                        foreach ($news as $new) {
                            $icons[] = Html::a(Html::img(Language::getIcon($new['id_language']), ["width" => 15, "title" => Language::getNameOfLanguage($new['id_language'])]), ["comment", "id" => $new['id']]);
                        }
                        return implode("  ", $icons);
                    },
                ],
                'template' => '{view}'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'V',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{view}'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'U',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{update}'
            ],
                [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'D',
                'contentOptions' => ['style' => 'width:30px;'],
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a(Html::tag("span", '', ['class' => 'glyphicon glyphicon-trash']), $url, [
                            'data' => [
                                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                                'pjax' => 0,
                                'class' => 'grid-action'
                            ],
                        ]);
                    },
                ],
                'template' => '{delete}'
            ],
        ],
        'options' => [
            'data' => [
                'sortable-widget' => 1,
                'sortable-url' => \yii\helpers\Url::toRoute(['sorting']),
            ]
        ],
    ]); 
    Pjax::end();
    ?>
    <p>
        <?php //if (!Settings::getInstance()->visible("blogs")) { ?>
        <?= Html::a(Yii::t('material', 'View sections'), ['/admin/'.Settings::getInstance()->getModuleId().'/section/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('material', 'View subsections'), ['/admin/'.Settings::getInstance()->getModuleId().'/subsection/index'], ['class' => 'btn btn-primary']) ?>
        <?php //} ?>
        <?= Html::button(Yii::t('common', 'Delete selected'), [
                'class' => 'btn btn-danger', 
                'onclick' => "deleteSelectedInGrid('.grid-view', '#grid-list', {confirm:'".Yii::t('common', 'Are you sure you want to delete this items?')."', cancel:'".Yii::t('common', 'Please select at least one element!')."'})",
            ]) 
        ?>
    </p>
    
</div>

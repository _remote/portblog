<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\models\Language;
use app\components\Settings;

//use app\modules\news\models\SubSection;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('material', 'Subsections');
$this->params['breadcrumbs'][] = ['label' => Yii::t(Settings::getInstance()->getModuleId(), 'Materials'), 'url' => ['/admin/'.Settings::getInstance()->getModuleId().'/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-material-sub-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('material', 'Create subsection'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'id_section',
                'label' => Yii::T('material', 'Owner'),
                'format' => 'raw',
                'value' => function($model) {
                    return !empty($model->ownertype->name)?Html::a($model->ownertype->name, ['admin/'.Settings::getInstance()->getModuleId().'section/view', "id" => $model->id_section]):false;
                }
            ],
//            [
//                'attribute' => 'id_language',
//                'label' => 'Language',
//                'format' => 'raw',
//                'value' => function($model) {
//                    return !empty($model->language)?Html::a($model->language->name, ["/admin/language/view", "id" => $model->language->id]):false;
//                }
//            ],
            'name',
            'comment',
                    
            [
                'attribute' => 'id_language',
                'label' => Yii::t('material', 'Language'),
                'format' => 'raw',
                'value' => function($model) {
                    $class = Settings::getInstance()->getModulePath()."\models\SubSection";
                    $countries = $class::getLocalList($model->id);
                    $icons = [];
                    foreach ($countries as $country) {
                        $icons[] = Html::a(Html::img(Language::getIcon($country['id_language']), ["width" => 15, "title" => Language::getNameOfLanguage($country['id_language'])]), ["/admin/language/view", "id" => $country['id_language']]);
                    }
                    return implode("  ", $icons);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'V',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{view}'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'U',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{update}'
            ],
                [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'D',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{delete}'
            ],
        ],
    ]); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

use app\models\Language;
use app\components\Settings;

//use app\modules\news\models\Section;
$class_section = Settings::getInstance()->getModulePath()."\models\Section";


$id_language = isset($id_language)?$id_language:false;
?>

<?= $form->field($model, "[{$id_language}]id_language")->hiddenInput()->label(false) ?> 

<?php 
echo $form->field($model, "[{$id_language}]id_section", [
        'template' =>  "{label} ".Html::a('', ['/admin/'.Settings::getInstance()->getModuleId().'/section/create'], ['class' => 'glyphicon glyphicon-plus action-button'])." {input}" 
    ])->widget(Select2::classname(), [
    'options' => [
        'id'=>"section-{$id_language}-id",
        'placeholder' => Yii::t('material', 'Select a section ..')
    ],
    'data' => ArrayHelper::map($class_section::find()->where(["id_language" => $model->id_language])->asArray()->all(), 'id', 'name')
]);
?>

<?= $form->field($model, "[{$id_language}]name")->textInput(['maxlength' => true]) ?>

<?= $form->field($model, "[{$id_language}]seo_title")->textInput(['maxlength' => true]) ?>
    
<?= $form->field($model, "[{$id_language}]seo_description")->textInput(['maxlength' => true]) ?>

<?= $form->field($model, "[{$id_language}]seo_keywords")->textInput(['maxlength' => true]) ?>

<?= $form->field($model, "[{$id_language}]seo_site")->textInput(['maxlength' => true]) ?>

<?= $form->field($model, "[{$id_language}]comment")->textInput(['maxlength' => true]) ?>
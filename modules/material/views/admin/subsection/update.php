<?php

use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\widgets\ActiveForm;

use app\models\Language;
use app\components\Settings;

$this->title = Yii::t('material', 'Update subsection'). ' ' . $main->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t(Settings::getInstance()->getModuleId(), 'Materials'), 'url' => ['/admin/'.Settings::getInstance()->getModuleId().'/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('material', 'Subsections'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $main->id, 'url' => ['view', 'id' => $main->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="type-material-sub-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="subsection-form">
   
        <?php 
        $form = ActiveForm::begin([
            'enableClientValidation' => false,
        ]);
        ?>

        <?php
        $items = [];
        foreach ($languages as $id => $type) {
            $items[] = [
                'options' => ['id' => 'lng-'.$id],
                'label' => Language::getTitleOfLanguage($id),
                'content' => $this->render('_form', [
                    'model' => $models[$id],
                    'id_language' => $id,
                    'form' => $form
                ]),
                'active' => ($language == $type)?true:false
            ];
        }
        echo TabsX::widget([
            'items' => $items,
            'position'=>TabsX::POS_ABOVE,
            'encodeLabels'=>false,
        ]);
        ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('common', 'Update') , ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\tabs\TabsX;

use app\models\Language;
use app\components\Settings;

$this->title = $main->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t(Settings::getInstance()->getModuleId(), 'Materials'), 'url' => ['/admin/'.Settings::getInstance()->getModuleId().'/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('material', 'Sections'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-material-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $main->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $main->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    $items = [];
    foreach ($models as $model) {
        $items[] = [
            'options' => ['id' => 'lng-'.$model->id_language],
            'label' => Language::getTitleOfLanguage($model->id_language),
            'content' => "<h1 style=\"margin-top: 0px;\">{$model->name}</h1>".DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'id',
                    [
                        'label' => Yii::t('material', 'Language'),
                        'format' => 'raw',
                        'value' => !empty($model->language->name)?Html::a($model->language->name, ["/admin/language/view", "id" => $model->language->id]):false,
                    ],
                    'name',
                    'seo_title',
                    'seo_description',
                    'seo_keywords',
                    'seo_site',
                    'comment',
                ],
            ]),
        ];
    }
    echo TabsX::widget([
        'items' => $items,
        'position'=>TabsX::POS_LEFT,
        'encodeLabels'=>false,
    ]);
    ?>

</div>

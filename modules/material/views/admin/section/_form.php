<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

use app\models\Language;
use app\components\Settings;

$id_language = isset($id_language)?$id_language:false;
?>

<?= $form->field($model, "[{$id_language}]id_language")->hiddenInput()->label(false) ?>  


<?= $form->field($model, "[{$id_language}]name")->textInput(['maxlength' => true]) ?>

<?= $form->field($model, "[{$id_language}]comment")->textInput(['maxlength' => true]) ?>

<?= $form->field($model, "[{$id_language}]seo_title")->textInput(['maxlength' => true]) ?>
    
<?= $form->field($model, "[{$id_language}]seo_description")->textInput(['maxlength' => true]) ?>

<?= $form->field($model, "[{$id_language}]seo_keywords")->textInput(['maxlength' => true]) ?>

<?= $form->field($model, "[{$id_language}]seo_site")->textInput(['maxlength' => true]) ?>

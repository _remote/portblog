<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\NewsImages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="material-images-form">

    <?php 
    $form = ActiveForm::begin([
        //'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]);
    ?>
    
    <?php 
//    $prew = [];
//    foreach ($model->medias as $media) {
//        $prew[] = Html::img($media->getThumbUploadUrl('file', 'preview'), ['class' => 'img-thumbnail']); 
//    }
    echo $form->field($model, '_temp_images[]')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
            'multiple'=>true
        ],
        'pluginOptions' => [
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  Yii::t('blogs', 'Select images'),
            //'initialPreview'=> $prew,
            'overwriteInitial'=>true,
        ],
    ])->label(false);
    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t("material", 'Upload'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

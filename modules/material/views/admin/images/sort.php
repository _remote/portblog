<?php
use yii\helpers\Html;

use kartik\sortable\Sortable;
use app\components\Settings;


$this->title = Yii::t('material', "Sorting gallery items");
$this->params['breadcrumbs'][] = ['label' => Yii::t(Settings::getInstance()->getModuleId(), 'Materials'), 'url' => ['/admin/'.Settings::getInstance()->getModuleId().'/index']];
$this->params['breadcrumbs'][] = ['label' => strip_tags(trim($material->title)), 'url' => ['/admin/'.Settings::getInstance()->getModuleId().'/view', 'id' => $material->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<p>
    <?= Html::a(Yii::t('common', 'Back'), ['/admin/'.Settings::getInstance()->getModuleId().'/view', 'id' => $material->id], ['class' => 'btn btn-primary', ]); ?>
    <?= " ".Html::button(Yii::t('common', 'Save'), [
        'id' => 'save-sort-button', 
        'class' => 'btn btn-success', 
        'style' => 'display: none;',
        'onclick' => 'changeSlidePosition(); return false;'
    ]); ?>
</p>

<script type="text/javascript">
function changeSlidePosition() {
    var position = [];
    $("#sortable-slide li").each(function(indx, element){
        position.push($(element).find("img").attr("rel"));
    });
    console.log(position);
    $.post("save-position", {position:position}, function(data) {
        if (data.result)
            $('#save-sort-button').hide();
        else bootbox.alert(data.error);
    }, "json");

}
</script>
<?php
$items = [];
foreach ($images as $image) {
    $items[] = ["content" => Html::img($image->getThumbUploadUrl('file', 'preview'), [
        'class' => ($image->is_active)?'img-thumbnail active':'img-thumbnail',
        'rel' => $image->id,
    ])];
}
echo Sortable::widget([
    'id' => 'sortable-slide',
    'type'=>'grid',
    'items'=> $items,
    'pluginEvents' => [
        'sortupdate' => 'function() { $("#save-sort-button").show(); }',
    ],
]);


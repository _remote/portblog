<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\components\Settings;

/* @var $this yii\web\View */
/* @var $model app\models\NewsImages */
$object = $model->material;
$title = ($object)?$object->title:null;
$id = ($object)?$object->id:false;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t(Settings::getInstance()->getModuleId(), 'Materials'), 'url' => ['/admin/'.Settings::getInstance()->getModuleId().'/index']];
$this->params['breadcrumbs'][] = ['label' => strip_tags(trim($title)), 'url' => ['/admin/'.Settings::getInstance()->getModuleId().'/view', 'id' => $id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-images-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_new',
            'title',
            'file',
            'url:url',
            'description',
            'ext',
            'position',
            'date_create',
            'date_update',
        ],
    ]) ?>

</div>

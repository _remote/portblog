<?php

use yii\helpers\Html;

use app\components\Settings;

/* @var $this yii\web\View */
/* @var $model app\models\NewsImages */

$this->title = Yii::t('material', 'Update Gallery');
$this->params['breadcrumbs'][] = ['label' => Yii::t(Settings::getInstance()->getModuleId(), 'Materials'), 'url' => ['/admin/'.Settings::getInstance()->getModuleId().'/index']];
$this->params['breadcrumbs'][] = ['label' => strip_tags(trim($main->title)), 'url' => ['/admin/'.Settings::getInstance()->getModuleId().'/view', 'id' => $main->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-images-update">

    <h1><?= Html::encode($this->title) ?>:</h1>

    <?= $this->render('_form', [
        'models' => $models,
    ]) ?>

</div>

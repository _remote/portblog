<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\components\Settings;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t(Settings::getInstance()->getModuleId(), 'Material images');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-images-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('material', 'Add image'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_material',
            'title',
            'file',
            'url:url',
            // 'description',
            // 'ext',
            // 'position',
            // 'date_create',
            // 'date_update',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

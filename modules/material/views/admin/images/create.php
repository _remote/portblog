<?php

use yii\helpers\Html;

use app\components\Settings;


/* @var $this yii\web\View */
/* @var $model app\models\NewsImages */

$object = $model->material;
$title = ($object)?$object->title:null;
$id = ($object)?$object->id:false;

$this->title = Yii::t('material', 'Create Gallery');
$this->params['breadcrumbs'][] = ['label' => Yii::t(Settings::getInstance()->getModuleId(), 'Materials'), 'url' => ['/admin/'.Settings::getInstance()->getModuleId().'/index']];
$this->params['breadcrumbs'][] = ['label' => strip_tags(trim($title)), 'url' => ['/admin/'.Settings::getInstance()->getModuleId().'/view', 'id' => $id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-images-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_add', [
        'model' => $model,
    ]) ?>

</div>

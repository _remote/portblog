<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\NewsImages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="material-images-form">

    <?php 
    $form = ActiveForm::begin([
        //'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]);
    ?>
    
    <?php 
    foreach ($models as $model) {
        echo "<div id='block-{$model->id}'>";
        echo $form->field($model, "[{$model->id}]title")->textInput();
        echo '<div class="col-xs-4" style="padding-left: 0px;">';
        echo Html::img($model->getThumbUploadUrl('file', 'thumb'), ['class' => 'img-thumbnail', 'style' => "width: 100%;"]); 
        echo '</div>';
        echo '<div class="col-xs-8" style="padding: 0px;">';
        echo $form->field($model, "[{$model->id}]description")->textarea(['rows' => 7]);
        echo '</div>';
        echo $form->field($model, "[{$model->id}]scenario")->hiddenInput()->label(false);
        echo '</div>';
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t("materail", 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

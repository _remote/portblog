<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

use app\models\User;
use app\components\Settings;

//use app\modules\news\models\Material;
$class_material = Settings::getInstance()->getModulePath()."\models\Material";


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('material', 'Comments');
$this->params['breadcrumbs'][] = ['label' => Yii::t(Settings::getInstance()->getModuleId(), 'Material'), 'url' => ['/admin/'.Settings::getInstance()->getModuleId().'/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('material', 'Create Comment'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 
    Pjax::begin(['id' => 'grid-list', 'options' => ['class' => 'pjax-wraper']]);    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            ['class' => 'yii\grid\CheckboxColumn'],
            'id_user' => [
                'attribute' => 'id_user',
                'format' => 'raw',
                'value' => function($model) {
                    return User::getNameOfUser($model->id_user);
                },
                'filter' => ArrayHelper::map(
                    User::find()
                        ->orderBy('id')
                        ->asArray()->all(),
                    'id', 'first_name'),
                'options' => ['width' => '120'],
            ],
            'id_material' => [
                'attribute' => 'id_material',
                'format' => 'raw',
                'value' => function($model) {
                    return (!empty($model->material))?Html::a(strip_tags($model->material->title), ['/admin/'.Settings::getInstance()->getModuleId().'/comment', "id" => $model->id_material]):false;
                },
                'filter' => ArrayHelper::map(
                    $class_material::find()
                        ->orderBy('id')
                        ->asArray()->all(),
                    'id', 'title'),
            ],
            'id_comment' => [
                'attribute' => 'id_comment',
                'format' => 'raw',
                'value' => function($model) {
                    return ($model->id_comment)?Html::a($model->id_comment, ['/admin/'.Settings::getInstance()->getModuleId().'/comments/view', "id" => $model->id_comment]):false;
                }
            ],
            'text:ntext',
            'date_created' => [
                'attribute' => 'date_created',
                'filter' => DatePicker::widget([
                    'name' => $searchModel->getShortName()."[date_created]",
                    'language' => 'ru', 
                    'dateFormat' => 'yyyy-MM-dd',
                    'value' => $searchModel->date_created
                ]),
                'format' => 'html',
                'options' => ['width' => '120'], 
                'value' => function($model) {
                    $time = strtotime($model->date_created);
                    return date("Y-m-d", $time);
                }
            ],
            // 'is_show',

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'V',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{view}'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'U',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{update}'
            ],
                [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'D',
                'contentOptions' => [
                    'style' => 'width:30px;',
                    'data' => [
                        'pjax' => 1
                    ],
                ],
                'template' => '{delete}'
            ],
        ],
    ]); 
    Pjax::end();
    ?>
    
    <p>
        <?= Html::button(Yii::t('common', 'Delete selected'), [
                'class' => 'btn btn-danger', 
                'onclick' => "deleteSelectedInGrid('.grid-view', '#grid-list', {confirm:'".Yii::t('common', 'Are you sure you want to delete this items?')."', cancel:'".Yii::t('common', 'Please select at least one element!')."'})",
            ]) 
        ?>
    </p>

</div>

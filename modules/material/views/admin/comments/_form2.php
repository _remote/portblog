<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\helpers\Url;
use kartik\depdrop\DepDrop;

use app\models\User;
use app\components\Settings;

//use app\modules\news\models\Material;
//use app\modules\news\models\Comment;
$class_comment = Settings::getInstance()->getModulePath()."\models\Comment";
$class_material = Settings::getInstance()->getModulePath()."\models\Material";
?>

<div class="comment-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php
            
    echo $form->field($model, "id_material", [
            'template' =>  "{label} ".Html::a('', ['admin/create'], ['class' => 'glyphicon glyphicon-plus action-button', 'target' => "_blank"])." {input}" 
        ])->widget(Select2::classname(), [
        'options' => [
            'id'=>"article-id",
            'placeholder' => Yii::t('material', 'Select a article..')
        ],
        'data' => ArrayHelper::map($class_material::find()->select(['title', 'id'])->orderBy("`title` DESC")->all(), 'id', 'title')
                    
    ]);
            
    $comments = $class_comment::find()->where(["id_material" => $model->id_material])->asArray()->all();
    echo $form->field($model, "id_comment", [
            //'template' =>  "{label} ".Html::a('', ['admin/comment/create'], ['class' => 'glyphicon glyphicon-plus action-button', 'target' => "_blank"])." {input}" 
        ])->widget(DepDrop::classname(), [
        'data' => @ArrayHelper::map($comments, 'id', 'text'),
        'options'=>['id'=>"cities-id"],
        'type' => DepDrop::TYPE_SELECT2,
        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
        'pluginOptions'=>[
            'depends'=>["article-id"],
            'placeholder'=>Yii::t('material', 'Select a comment..'),
            'url'=>Url::to(['admin/commentlist'])
        ]
    ]);
            
    ?>
    
    <?php 
    echo $form->field($model, 'id_user')->widget(Select2::classname(), [
        'options' => [
            'placeholder' => Yii::t('material', 'Select a user ..')
        ],
        'data' => ArrayHelper::map(User::find()->select(["CONCAT(first_name,' (',email,')') as first_name", 'id'])->orderBy("`first_name` ASC")->all(), 'id', 'first_name')
    ])->label(Yii::t('material', 'From user'));
    ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?php $form->field($model, 'date_created')->textInput() ?>

    <?php $form->field($model, 'is_show')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

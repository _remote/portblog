<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\models\User;
use app\components\Settings;

/* @var $this yii\web\View */
/* @var $model app\models\Comment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t(Settings::getInstance()->getModuleId(), 'Materials'), 'url' => ['/admin/'.Settings::getInstance()->getModuleId().'/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('material', 'Comments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'id_material',
                'format' => 'raw',
                'value' => Html::a($model->material->title, ["/admin'.Settings::getInstance()->getModuleId().'view", "id" => $model->id_material]),
                
            ],
            'id_comment',
            [
                'attribute' => 'id_user',
                'format' => 'raw',
                'value' => User::getNameOfUser($model->id_user),
            ],
            'text:ntext',
            'date_created',
            //'is_show',
        ],
    ]) ?>

</div>

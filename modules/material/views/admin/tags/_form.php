<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\material\models\Tags */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tags-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'material')->dropDownList([
        "news" => Yii::t('common', 'News'),
        "articles" => Yii::t('common', 'Articles'),
        "events" => Yii::t('common', 'Events'),
        "blog" => Yii::t('common', 'Blogs'),
        "company" => Yii::t('common', 'Company'),
        "persons" => Yii::t('common', 'Persons'),
        "places" => Yii::t('common', 'Places'),
    ], ["disabled" => 'disabled']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'frequency')->textInput() ?>
    
    <?= $form->field($model, "seo_title")->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, "seo_description")->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, "seo_keywords")->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, "seo_site")->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

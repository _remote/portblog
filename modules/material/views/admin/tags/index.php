<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

use app\components\Settings;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Tags');
$this->params['breadcrumbs'][] = ['label' => Yii::t(Settings::getInstance()->getModuleId(), 'Materials'), 'url' => ['/admin/'.Settings::getInstance()->getModuleId().'/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tags-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Add Tag'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 
    Pjax::begin(['id' => 'grid-list', 'options' => ['class' => 'pjax-wraper']]);    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            ['class' => 'yii\grid\CheckboxColumn'],
            'material' => [
                'attribute' => 'material',
                'format' => 'raw',
                'value' => function($model) {
                    return $model->material;
                },
                'filter' => [
                    "news" => Yii::t('common', 'News'),
                    "articles" => Yii::t('common', 'Articles'),
                    "events" => Yii::t('common', 'Events'),
                    "blog" => Yii::t('common', 'Blogs'),
                    "company" => Yii::t('common', 'Company'),
                    "persons" => Yii::t('common', 'Persons'),
                    "places" => Yii::t('common', 'Places'),
                ],
                'options' => ['width' => '120'],
                'visible' => false,
            ],
            'name' => [
                'attribute' => 'name',
                'filter' => true,
            ],
            'frequency' => [
                'attribute' => 'frequency',
                'filter' => true,
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'V',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{view}'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'U',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{update}'
            ],
                [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'D',
                'contentOptions' => [
                    'style' => 'width:30px;',
                    'data' => [
                        'pjax' => 1
                    ],
                ],
                'template' => '{delete}'
            ],
        ],
    ]); 
    Pjax::end();
    ?>

</div>

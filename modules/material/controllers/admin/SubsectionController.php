<?php

namespace app\modules\material\controllers\admin;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\models\Language;
use app\components\Settings;

//use app\modules\news\models\SubSection;
/**
 * SubtypeController implements the CRUD actions for SubSection model.
 */
class SubsectionController extends Controller  implements \yii\base\ViewContextInterface {

    public function getViewPath() {
        return Yii::getAlias('@app/modules/material/views/admin/subsection');
    }

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SubSection models.
     * @return mixed
     */
    public function actionIndex() {
        $class = Settings::getInstance()->getModulePath()."\models\SubSection";
        $dataProvider = new ActiveDataProvider([
            'query' => $class::find()->where('`id_main` = `id`'),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SubSection model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $language = "ru") {
        return $this->render('view', [
            'models' => $this->findModels($id),
            'main' => $this->findModel($id),
            'language' => $language
        ]);
    }

    /**
     * Creates a new SubSection model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_language = false, $language = "ru") {
        $class = Settings::getInstance()->getModulePath()."\models\SubSection";
        $languages = Language::getList();
        $models = [];
        foreach ($languages as $id => $type) {
            $model = new $class();
            $model->id_language = $id;
            $models[$id] = $model;
        }
        
        if ($class::loadMultiple($models, Yii::$app->request->post())) {
            $id_main = 0;
            foreach ($models as $id_language => $model) {
                $model->id_main = $id_main;
                if ($model->save()) {
                    if (!$id_main) {
                        Yii::$app->json->clearCache(Settings::getInstance()->getModuleId()."/menu");
                        $id_main = $model->id;
                        $class::updateAll(["id_main" => $id_main], ["id" => $model->id]);
                    }
                    //Yii::$app->json->clearCache(Settings::getInstance()->getModuleId()."/".Yii::$app->formatter->asDate($model->date_created, 'php:Y-m-d'));
                }
            }
            if ($id_main)
                return $this->redirect(['view', 'id' => $id_main]);
        } else {
            return $this->render('create', [
                'models' => $models,
                'languages' => $languages,
                'language' => $language,
            ]);
        }
    }

    /**
     * Updates an existing SubSection model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $language = "ru") {
        $class = Settings::getInstance()->getModulePath()."\models\SubSection";
        $models = $this->findModels($id);
        $languages = Language::getList();
        foreach ($languages as $id_language => $type) {
            if (!isset($models[$id_language])) { 
                $model = new $class();
                $model->id_language = $id_language;
                $model->id_main = $id;
                $models[$id_language] = $model;
            }
        }
        if ($class::loadMultiple($models, Yii::$app->request->post())) {
            $success = false;
            foreach ($models as $id_language => $model) {
                if ($model->validate()) {
                    if ($model->save()) {
                        Yii::$app->json->clearCache(Settings::getInstance()->getModuleId()."/menu");
                        $success = true;
                    }
                }
            }
            if ($success)
                return $this->redirect(['view', 'id' => $id]);
                
        } else {
            return $this->render('update', [
                'models' => $models,
                'languages' => $languages,
                'main' => $this->findModel($id),
                'language' => $language,
            ]);
        }
    }

    /**
     * Deletes an existing SubSection model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $models = $this->findModels($id);
        foreach ($models as $model) {
            if ($model->delete()) {
                Yii::$app->json->clearCache(Settings::getInstance()->getModuleId()."/menu");
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the SubSection model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SubSection the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        $class = Settings::getInstance()->getModulePath()."\models\SubSection";
        if (($model = $class::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModels($id) {
        $class = Settings::getInstance()->getModulePath()."\models\SubSection";
        if ($items = $class::find()->where(["id_main" => $id])->orderBy("id_language ASC")->all()) {
            $models = [];
            foreach ($items as $item) 
                $models[$item->id_language] = $item;
            return $models;
        } else {
            throw new NotFoundHttpException('The requested page does not existed.');
        }
    }

}

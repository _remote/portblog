<?php

namespace app\modules\material\controllers\admin;

use Yii;

use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

use app\components\Settings;

//use app\modules\news\models\Comment;

/**
 * CommentsController implements the CRUD actions for Comment model.
 */
class CommentsController extends Controller implements \yii\base\ViewContextInterface {

    public function getViewPath() {
        return Yii::getAlias('@app/modules/material/views/admin/comments');
    }

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Comment models.
     * @return mixed
     */
    public function actionIndex() {
        $class = Settings::getInstance()->getModulePath()."\models\search\Comment";
        $searchModel = new $class;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Comment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Comment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $class = Settings::getInstance()->getModulePath()."\models\search\Comment";
        $model = new $class;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->updateCount();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Comment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->updateCount();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Comment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if ($model = $this->findModel($id)) {
            $material = $model->material;
            $model->delete();
            $material->updateCommentsCount();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Comment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Comment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        $class = Settings::getInstance()->getModulePath()."\models\search\Comment";
        if (($model = $class::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    //Удаление множества объектов, по ajax-запросу
    public function actionDeleteselected() {
        $class = Settings::getInstance()->getModulePath()."\models\search\Comment";
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            if (!empty($post['items'])) {
                foreach ($post['items'] as $item) {
                    if ($model = $class::findOne(["id" => $item])) {
                        if ($model->delete()) {
                            Yii::$app->json->clearCache(Settings::getInstance()->getModuleId()."/".$model->id_material.".json");
                        }
                    }
                }
                return true;
            }
        }
    }

}

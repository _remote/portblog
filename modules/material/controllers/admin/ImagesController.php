<?php

namespace app\modules\material\controllers\admin;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

use app\components\Settings;

//use app\modules\news\models\Material;
//use app\modules\news\models\MaterialImages;

/**
 * ImagesController implements the CRUD actions for NewsImages model.
 */
class ImagesController extends Controller  implements \yii\base\ViewContextInterface {

    public function getViewPath() {
        return Yii::getAlias('@app/modules/material/views/admin/images');
    }

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all NewsImages models.
     * @return mixed
     */
    public function actionIndex() {
        $class = Settings::getInstance()->getModulePath()."\models\MaterialImages";
        $dataProvider = new ActiveDataProvider([
            'query' => $class::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NewsImages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NewsImages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAdd($id = false) {
        $class = Settings::getInstance()->getModulePath()."\models\MaterialImages";
        $class_material = Settings::getInstance()->getModulePath()."\models\Material";
        $model = new $class();
        $model->id_material = $id;

        if ($model->load(Yii::$app->request->post())) {
            $model->_temp_images = UploadedFile::getInstances($model, '_temp_images');
            foreach ($model->_temp_images as $image) {
                $class::addImage($id, $image);
            }
            $class_material::updateGalleryStatus($id, true);
            return $this->redirect(['update', 'id' => $id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing NewsImages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id = false) {
        $class = Settings::getInstance()->getModulePath()."\models\MaterialImages";
        $class_material = Settings::getInstance()->getModulePath()."\models\Material";
        
        if (!($material = $class_material::findOne(["id" => $id])))
            throw new NotFoundHttpException('The requested page does not exist.');
        if (!$models = $this->findModels($id))
            $this->redirect (["add", "id" => $id]);
        
        if ($class::loadModels($models, Yii::$app->request->post())) {
            $result = true;
            foreach ($models as $model) {
                //Сценарий нужен для загрузки фотографий
                if ($model->isDefaultScenario())
                    $model->scenario = "update";
                if (!$model->save())
                    $result = false;
            }
            if ($result) 
                return $this->redirect(['/admin/'.Settings::getInstance()->getModuleId().'/view', 'id' => $id]);
        } else {
            return $this->render('update', [
                'id_material' => $id,
                'models' => $models,
                'main' => $material,
            ]);
        }
    }
    
    public function actionDelete($id) {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing NewsImages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteGallery($id) {
        $class = Settings::getInstance()->getModulePath()."\models\MaterialImages";
        $class_material = Settings::getInstance()->getModulePath()."\models\Material";
        $class::deleteImagesFromObject($id);
        $class_material::updateGalleryStatus($id, false);
        return $this->redirect(['/admin/'.Settings::getInstance()->getModuleId().'/view', "id" => $id]);
    }

    /**
     * Finds the NewsImages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NewsImages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        $class = Settings::getInstance()->getModulePath()."\models\MaterialImages";
        if (($model = $class::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModels($id) {
        $class = Settings::getInstance()->getModulePath()."\models\MaterialImages";
        if ($items = $class::find()->where(["id_material" => $id])->orderBy("date_create ASC")->all()) {
            $models = [];
            foreach ($items as $item) 
                $models[] = $item;
            return $models;
        } else {
            return false;
        }
    }
    
    public function actionSort($id = false) {
        $class_material = Settings::getInstance()->getModulePath() . "\models\Material";
        $class = Settings::getInstance()->getModulePath() . "\models\MaterialImages";
        $images = $class::find()->where(["id_material" => $id])->orderBy("position ASC")->all();
        return $this->render('sort', [
            "id" => $id,
            "material" => $class_material::findOne(["id" => $id]),
            "images" => $images,
        ]);
    }
    
    public function actionSavePosition() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $position = Yii::$app->request->post('position');
        if (is_array($position)) {
            $class = Settings::getInstance()->getModulePath() . "\models\MaterialImages";
            foreach ($position as $index => $id) {
                $class::updateAll(['position' => ($index+1)], ['id' => $id]);
            }
            return ["result" => true];
        }
    }

}

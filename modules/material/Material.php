<?php

namespace app\modules\material;

use app\components\Settings;

class Material extends \yii\base\Module {

    public $controllerNamespace = null;

    public function init() {
        parent::init();
        $this->controllerNamespace = "app\modules\\{$this->id}\controllers";
        Settings::getInstance()->setModule($this->id); // Устанавливаем идентификатор текущего модуля в настройках. 
    }

}

<?php

namespace app\modules\places\controllers\admin;

use Yii;
/**
 * NewsController implements the CRUD actions for News model.
 */
class DefaultController extends \app\modules\material\controllers\admin\DefaultController {
    public function getViewPath() {
        return Yii::getAlias('@app/modules/material/views/admin/default_with_map');
    }
}

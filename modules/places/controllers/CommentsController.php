<?php

namespace app\modules\places\controllers;

use Yii;
use app\components\Settings;
use app\modules\material\components\Request;

class CommentsController extends \yii\web\Controller {

    public $request = null;

    public function init() {
        parent::init();
        $this->request = new Request(Settings::getInstance()->getModuleId());
    }
    
    public function actionSet($id_material = false, $id_user = false, $text = "", $id_comment = false) {
        return $this->request->comment_set(Yii::$app->request->get());
    }

}

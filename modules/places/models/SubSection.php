<?php

namespace app\modules\places\models;

use Yii;
use app\models\Language;
use app\components\Settings;

/**
 * This is the model class for table "subsection".
 *
 * @property integer $id
 * @property integer $id_section
 * @property integer $id_language
 * @property integer $id_main
 * @property string $name
 * @property string $comment
 */
class SubSection extends \app\modules\material\models\SubSection {

    public static function tableName() {
        return "{{%".Settings::getInstance()->getModuleId()."_subsection}}";
    }

}

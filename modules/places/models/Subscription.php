<?php

namespace app\modules\places\models;

use Yii;
use app\components\Settings;
use app\models\ActiveRecord;

/**
 * This is the model class for table "news_subscriptions".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_city
 * @property integer $id_section
 * @property integer $id_subsection
 * @property string $date_create
 */
class Subscription extends \app\modules\material\models\Subscription {
    
    public static function tableName() {
        return "{{%".Settings::getInstance()->getModuleId()."_subscriptions}}";
    }

}

<?php

namespace app\modules\company\models\search;

use Yii;
use yii\data\ActiveDataProvider;

class Material extends \app\modules\company\models\Material  {

    public function rules() {
        return [
            [['id_user', 'id_language', 'username', 'date_created', 'date_update', 'title', 'text', 'is_show', 'date_public'], 'safe'],
        ];
    }
    
}
        
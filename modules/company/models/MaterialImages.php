<?php

namespace app\modules\company\models;

use Yii;
use yii\web\UploadedFile;
use app\components\UploadImageBehavior;

use app\modules\company\models\Material;
use app\components\Settings;

class MaterialImages extends \app\modules\material\models\MaterialImages {
    
    public static function tableName() {
        return "{{%".Settings::getInstance()->getModuleId()."_images}}";
    }

}

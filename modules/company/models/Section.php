<?php

namespace app\modules\company\models;

use Yii;
use app\models\Language;
use app\components\Settings;

/**
 * This is the model class for table "section".
 *
 * @property integer $id
 * @property integer $id_language
 * @property integer $id_main
 * @property string $name
 * @property string $comment
 */
class Section extends \app\modules\material\models\Section {
    
    public static function tableName() {
        return "{{%".Settings::getInstance()->getModuleId()."_section}}";
    }

}

<?php

namespace app\modules\company\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

use app\models\Language;
use app\components\Settings;
/*
 * @property string $adress
 * @property string $latitude
 * @property string $longitude
*/

class Material extends \app\modules\material\models\Material { 
    
    public static function tableName() {
        return "{{%".Settings::getInstance()->getModuleId()."}}";
    }
    
    public function rules() {
        return array_merge(parent::rules(), [
            [['latitude', 'longitude'], 'number'],
            [['adress'], 'string', 'max' => 512],
        ]);
    }
    
    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), [
            'adress' => Yii::t('material', 'Adress'),
            'latitude' => Yii::t('material', 'Latitude'),
            'longitude' => Yii::t('material', 'Longitude'),
        ]);
    }
    
    
    
    
}

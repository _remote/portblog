<?php

namespace app\modules\articles\models;

use Yii;
use app\models\ActiveRecord;
use app\components\Settings;

/**
 * This is the model class for table "news_subscriptions".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_city
 * @property integer $id_section
 * @property integer $id_subsection
 * @property string $date_create
 */
class Subscription extends \app\modules\material\models\Subscription {
    
    public static function tableName() {
        return "{{%".Settings::getInstance()->getModuleId()."_subscriptions}}";
    }

}

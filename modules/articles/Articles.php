<?php

namespace app\modules\articles;

use app\components\Settings;

class Articles extends \yii\base\Module {

    public $controllerNamespace = null;

    public function init() {
        parent::init();
        $this->controllerNamespace = "app\modules\\{$this->id}\controllers";
        Settings::getInstance()->setModule($this->id);
    }

}

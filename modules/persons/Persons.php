<?php

namespace app\modules\persons;

use app\components\Settings;

class Persons extends \yii\base\Module {

    public $controllerNamespace = null;

    public function init() {
        parent::init();
        $this->controllerNamespace = "app\modules\\{$this->id}\controllers";
        Settings::getInstance()->setModule($this->id);
    }

}

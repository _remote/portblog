<?php

namespace app\modules\persons\models;

use Yii;
use yii\web\UploadedFile;
use app\components\Settings;
use app\components\UploadImageBehavior;

use app\modules\persons\models\Material;


class MaterialImages extends \app\modules\material\models\MaterialImages {
    
    public static function tableName() {
        return "{{%".Settings::getInstance()->getModuleId()."_images}}";
    }

}

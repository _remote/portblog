<?php

namespace app\modules\blogs;

use app\components\Settings;

class Blogs extends \yii\base\Module {

    public $controllerNamespace = null;

    public function init() {
        parent::init();
        $this->controllerNamespace = "app\modules\\{$this->id}\controllers";
        Settings::getInstance()->setModule($this->id);
    }

}

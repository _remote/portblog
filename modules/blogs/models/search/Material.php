<?php

namespace app\modules\blogs\models\search;

use Yii;
use yii\data\ActiveDataProvider;

class Material extends \app\modules\blogs\models\Material  {

    public function rules() {
        return [
            [['id_user', 'id_language', 'username', 'date_created', 'date_update', 'title', 'text', 'is_show', 'delivery_date', 'date_public'], 'safe'],
        ];
    }
    
}
        
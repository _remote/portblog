<?php

namespace app\modules\blogs\models\search;

use Yii;
use yii\data\ActiveDataProvider;

use app\models\User;

class Comment extends \app\modules\blogs\models\Comment {
    
    public function rules() {
        return [
            [['id_material', 'id_comment', 'id_user', 'text', 'date_created', 'is_show'], 'safe'],
        ];
    }
    
}

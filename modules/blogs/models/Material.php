<?php

namespace app\modules\blogs\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

use app\models\Language;
use app\components\Settings;


class Material extends \app\modules\material\models\Material { 
    
    public static function tableName() {
        return "{{%".Settings::getInstance()->getModuleId()."}}";
    }
    
}

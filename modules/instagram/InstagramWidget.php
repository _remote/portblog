<?php

namespace app\modules\instagram;

use Yii;

class InstagramWidget extends \yii\base\Widget
{
    public $clientId;
    public $callBackUrl = 'http://api.portrussia.ru/instagram/success';
    public $secretId;
    public $userName;
    public $tag;
    public $showBy = 'user';
    public $isCacheEnabled = true;
    public $cacheTime = 3600;

    public $width = 260;
    public $imgWidth = 0;
    public $inline = 4;
    public $isShowToolbar = true;
    public $count = 12;
    public $imgRes = 'thumbnail';

    private $instagram;
    public $token;
            


    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        $i18n = Yii::$app->i18n;
        $i18n->translations['app/modules/instagram/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en',
            'basePath' => '@app/modules/instagram/messages',
            'fileMap' => [
                'app/modules/instagram/messages' => 'widget.php',
            ],
        ];
    }

    public function run()
    {
        $this->width -= 2;

        if ($this->width > 0) {
            $this->imgWidth = round(($this->width - (17 + (9 * $this->inline))) / $this->inline);
        }

        if (!empty($this->tag)) {
            $this->showBy = 'tag';
        }

        if ($this->showBy == 'tag') {
            $this->isShowToolbar = false;
        }

        $this->instagram = new \Instagram\Instagram;
        $token = Yii::$app->session->get("instagram_access_token");
        $this->instagram->setAccessToken($token);
        Yii::$app->debug->show($this->instagram->getTag($this->tag));

        $user = false;
        if ($this->showBy == 'user') {
            $user = $this->findUser($this->userName);
            $media = $this->findMediaByUser($user, $this->count);
        } elseif ($this->showBy == 'tag') {
            $media = $this->findMediaByTag($this->tag, $this->count);
        }
        
        Yii::$app->debug->show($media);

        return $this->render('default',
            [
                'user' => $user,
                'media' => $media,
                'userName' => $this->userName,
                'width' => $this->width,
                'imgWidth' => $this->imgWidth,
                'inline' => $this->inline,
                'isShowToolbar' => $this->isShowToolbar,
                'imgRes' => $this->imgRes,

                'title' => InstagramWidget::t('messages', 'title'),
                'buttonFollow' => InstagramWidget::t('messages', 'buttonFollow'),
                'statPosts' => InstagramWidget::t('messages', 'statPosts'),
                'statFollowers' => InstagramWidget::t('messages', 'statFollowers'),
                'statFollowing' => InstagramWidget::t('messages', 'statFollowing'),
                'imgEmpty' => InstagramWidget::t('messages', 'imgEmpty'),
            ]
        );
    }

    public function findUser($userName)
    {
        if (empty($userName)) {
            throw new \Exception('Empty \'userName\' argument');
            return false;
        }

        $key = 'widget_instagram_find_user_' . $userName;

        if ($this->isCacheEnabled) {
            $user = \Yii::$app->cache->get($key);
        }

        if ($user === false || !$this->isCacheEnabled) {
            $users = $this->instagram->searchUser($userName, 1);

            if (!empty($users->meta->error_message)) {
                throw new \Exception($users->meta->error_message);
            } else {
                $user_id = $users->data[0]->id;

                if (!empty($user_id)) {
                    $user = $this->instagram->getUser($user_id);
                }
            }

            if ($this->isCacheEnabled) {
                \Yii::$app->cache->set($key, $user, $this->cacheTime);
            }
        }

        if (!empty($user->meta->error_message)) {
            throw new \Exception($user->meta->error_message);
        }

        if (empty($user->data)) {
            throw new \Exception('User not found');
        }

        return $user->data;
    }

    public function findMediaByUser($user, $count)
    {
        if (empty($user)) {
            throw new \Exception('Empty \'user\' argument');
            return false;
        }

        $key = 'widget_instagram_find_media_by_user_' . $this->userName . '_' . $count;

        if ($this->isCacheEnabled) {
            $media = \Yii::$app->cache->get($key);
        }

        if ($media === false || !$this->isCacheEnabled) {
            $user = $this->instagram->getUser($user->id);
            $media = $user->getMedia();

            if ($this->isCacheEnabled) {
                \Yii::$app->cache->set($key, $media, $this->cacheTime);
            }
        }

        if (!empty($media->meta->error_message)) {
            throw new \Exception($media->meta->error_message);
        }

        return $media;
    }

    public function findMediaByTag($tag, $count)
    {
        $key = 'widget_instagram_find_media_by_tag_' . $tag . '_' . $count;

        if ($this->isCacheEnabled) {
            $media = \Yii::$app->cache->get($key);
        }

        if ($media === false || !$this->isCacheEnabled) {
            if (!empty($this->tag)) {
                $media = $this->instagram->getTagMedia($tag);
            }

            if ($this->isCacheEnabled) {
                \Yii::$app->cache->set($key, $media, $this->cacheTime);
            }
        }

        if (!empty($media->meta->error_message)) {
            throw new \Exception($media->meta->error_message);
        }

        return $media;
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('app/modules/instagram/' . $category, $message, $params, $language);
    }
}

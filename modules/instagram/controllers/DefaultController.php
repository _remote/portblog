<?php

namespace app\modules\instagram\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;

class DefaultController extends Controller {
    
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    public function actionAuth($code = false) {
        
        
        $config = [
            'client_id' => '5764bc4b02c741c3a8dab930b078a78c',
            'client_secret' => '9d4dd6a409ba429f81ab3d0ff3c00643',
            'redirect_uri' => 'http://api.portrussia.ru/instagram/auth',
            'scope' => ['likes', 'comments', 'relationships', 'public_content', 'basic'],
        ];
        $auth = new \Instagram\Auth($config);
        if ($code) {
            $token = $auth->getAccessToken($code);
            Yii::$app->session->set("instagram_access_token", $token);
            return $this->redirect(Url::to(["search", "tag" => "snow"]));
        }
        else $auth->authorize();
    }
    
    public function actionSuccess($code = false) {
        Yii::$app->session->set("instagram_access_token", $code);
        return $this->redirect("search");
    }
    
    public function actionSearch($tag = false) {
        if (!$token = Yii::$app->session->get("instagram_access_token")) 
            $this->redirect("auth");
        
        return \app\modules\instagram\InstagramWidget::widget([
            'showBy' => 'tag',
            'tag' => 'nighte',
            'token' => $token
        ]);
    }
    
    
    
    public function actionGetToken() {
        return Yii::$app->session->get("instagram_access_token");
    }
}


<?php

namespace app\modules\instagram;

class Instagram extends \yii\base\Module {
    
    public $controllerNamespace = 'app\modules\instagram\controllers';

    public function init() {
        parent::init();
        // custom initialization code goes here
    }
}

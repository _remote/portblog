<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\settings\models\SettingsMailTemplates */

$this->title = Yii::t('settings', 'Add Templates');
$this->params['breadcrumbs'][] = ['label' => Yii::t('settings', 'Settings'), 'url' => ['/admin/settings/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('settings', 'Mail Templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-mail-templates-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

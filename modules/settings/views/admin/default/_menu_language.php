<?php
use yii\helpers\Html;

use kartik\sortable\Sortable;

?>
<p>
<?= Html::a(Yii::t('common', 'Change'), ['/admin/settings/menu/index', 'id_language' => $id_language], ['class' => 'btn btn-primary']); ?>
<?= Html::button(Yii::t('common', 'Save'), ['id' => 'save-lang-'.$id_language, 'class' => 'btn btn-success', 'style' => 'display: none;margin-left: 5px;', 'onclick' => 'changeMenuPosition('.$id_language.');']); ?>
</p>

<?php
$items = [];
foreach ($menu as $item) {
    $items[] = ["content" => Html::a($item->title, ['/admin/settings/menu/update', 'id' => $item->id, 'id_language' => $item->id_language], ['rel' => $item->id])];
}
echo Sortable::widget([
    'id' => 'sortable-menu-'.$id_language,
    'items'=> $items,
    'pluginEvents' => [
        'sortupdate' => 'function() { $("#save-lang-'.$id_language.'").show(); }',
    ],
]);


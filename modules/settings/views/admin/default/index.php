<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\models\Language;
use kartik\tabs\TabsX;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('settings', 'Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id_language',
                'format' => 'raw',
                'value' => Html::a($model->language->name, ['/admin/language/view', "id" => $model->language->id]),
            ],
            [
                'attribute' => 'id_mail_delivery',
                'format' => 'raw',
                'value' => Html::a($model->mailDelivery->email, ['/admin/settings/mail/view', "id" => $model->mailDelivery->id]),
            ],
            'delivery_time',
            [
                'attribute' => 'id_mail_for_user',
                'format' => 'raw',
                'value' => Html::a($model->mailForUser->email, ['/admin/settings/mail/view', "id" => $model->mailForUser->id]),
            ],
            [
                'attribute' => 'id_mail_info',
                'format' => 'raw',
                'value' => Html::a($model->mailInfo->email, ['/admin/settings/mail/view', "id" => $model->mailInfo->id]),
            ],
            [
                'attribute' => 'id_mail_admin',
                'format' => 'raw',
                'value' => Html::a($model->mailAdmin->email, ['/admin/settings/mail/view', "id" => $model->mailAdmin->id]),
            ],
            [
                'attribute' => 'footer',
                'format' => 'raw',
                'value' => $model->footer,
            ],
        ],
    ]); ?>

    <h1><?= Yii::t('settings', 'Main menu') ?></h1>
    
    <script type="text/javascript">
    function changeMenuPosition(id_language) {
        var position = [];
        $("#sortable-menu-"+id_language+" li").each(function(indx, element){
            position.push($(element).find("a").attr("rel"));
        });
        console.log(position);
        $.post("menu/save-position", {position:position, id_language:id_language}, function(data) {
            if (data.result)
                $('#save-lang-'+id_language).hide();
        }, "json");
        
    }
    </script>
    
    <?php
    $items = [];
    foreach ($menus as $id_language => $menu) {
        $language = Language::getTypeOfLanguage($id_language);
        $items[] = [
            'options' => ['id' => 'lng-'.$id_language],
            'label' => Language::getTitleOfLanguage($id_language),
            'content' => $this->render("_menu_language", [
                "menu" => $menu,
                "language" => $language,
                "id_language" => $id_language,
            ]),
        ];
    }
    
    echo TabsX::widget([
        'items' => $items,
        'position'=>TabsX::POS_ABOVE,
        'encodeLabels'=>false,
    ]);
    ?>
    
    <h1><?= Yii::t('settings', 'Footer') ?></h1>
    
    <script type="text/javascript">
    function changeFooterPosition(id_language) {
        var position = [];
        $("#sortable-footer-"+id_language+" li").each(function(indx, element){
            position.push($(element).find("a").attr("rel"));
        });
        console.log(position);
        $.post("footer/save-position", {position:position, id_language:id_language}, function(data) {
            if (data.result)
                $('#save-footer-lang-'+id_language).hide();
        }, "json");
        
    }
    </script>
    
    <?php
    $items = [];
    foreach ($footer as $id_language => $footer_items) {
        $language = Language::getTypeOfLanguage($id_language);
        $items[] = [
            'options' => ['id' => 'lng-'.$id_language],
            'label' => Language::getTitleOfLanguage($id_language),
            'content' => $this->render("_footer_language", [
                "footer_items" => $footer_items,
                "language" => $language,
                "id_language" => $id_language,
            ]),
        ];
    }
    
    echo TabsX::widget([
        'items' => $items,
        'position'=>TabsX::POS_ABOVE,
        'encodeLabels'=>false,
    ]);
    ?>
</div>

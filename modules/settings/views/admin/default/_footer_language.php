<?php
use yii\helpers\Html;

use kartik\sortable\Sortable;

?>
<p>
<?= Html::a(Yii::t('common', 'Change'), ['/admin/settings/footer/index', 'id_language' => $id_language], ['class' => 'btn btn-primary']); ?>
<?= Html::button(Yii::t('common', 'Save'), ['id' => 'save-footer-lang-'.$id_language, 'class' => 'btn btn-success', 'style' => 'display: none;margin-left: 5px;', 'onclick' => 'changeFooterPosition('.$id_language.');']); ?>
</p>

<?php
$items = [];
foreach ($footer_items as $item) {
    $items[] = ["content" => Html::a($item->title, ['/admin/settings/footer/update', 'id' => $item->id, 'id_language' => $item->id_language], ['rel' => $item->id])];
}
echo Sortable::widget([
    'id' => 'sortable-footer-'.$id_language,
    'items'=> $items,
    'pluginEvents' => [
        'sortupdate' => 'function() { $("#save-footer-lang-'.$id_language.'").show(); }',
    ],
]);


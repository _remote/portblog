<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model app\modules\settings\models\SettingsMenu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_language')->hiddenInput()->label(false) ?>

    <?php $form->field($model, 'id_main')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    
    <?= 
    $form->field($model, "text")->widget(TinyMce::className(), [
        'options' => ['rows' => 10],
        'language' => 'ru',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern imagetools"
            ],
            'toolbar1' => "insertfile undo redo | styleselect | bullist numlist outdent indent | link image media | print code preview fullscreen",
            'toolbar2' => "fontselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | forecolor backcolor",
            'image_advtab' => true,
            "relative_urls" => false,
            "remove_script_host"=> false,
//            'file_browser_callback'=> new yii\web\JsExpression("function(field_name, url, type, win) {
//                if(type=='image') {
//                    $('#upload_form_model input').val('{$model->getShortName()}');
//                    $('#upload_form_object input').val({$model->id});
//                    $('#upload_form input[type=file]').click();
//                }
//            }"),
        ],
        
    ]); 
    ?>

    <?php $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?php $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>

    <?php $form->field($model, 'position')->textInput() ?>

    <?= $form->field($model, 'in_new')->dropDownList(["0" => Yii::t('common', "No"), "1" => Yii::t('common', "Yes")]) ?>

    <?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_other')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\settings\models\SettingsMail */

$this->title = $model->host;
$this->params['breadcrumbs'][] = ['label' => Yii::t('settings', 'Settings'), 'url' => ['/admin/settings/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('settings', 'Mails'), 'url' => ['/admin/settings/mail/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-mail-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'email',
            'host',
            'username',
            //'password',
            'port',
            'encryption',
            'file_transport',
            'view_path',
            'description',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\settings\models\SettingsMenu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_language')->hiddenInput()->label(false) ?>

    <?php $form->field($model, 'id_main')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?php $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>

    <?php $form->field($model, 'position')->textInput() ?>

    <?= $form->field($model, 'in_new')->dropDownList(["0" => Yii::t('common', "No"), "1" => Yii::t('common', "Yes")]) ?>

    <?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_other')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

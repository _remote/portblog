<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\models\Language;

/* @var $this yii\web\View */
/* @var $model app\modules\settings\models\SettingsMenu */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('settings', 'Settings'), 'url' => ['/admin/settings/index']];
$this->params['breadcrumbs'][] = ['label' => Language::getNameOfLanguage($id_language), 'url' => ['/admin/settings/index', 'language' => Language::getTypeOfLanguage($id_language)]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('settings', 'Main Menu'), 'url' => ['index', 'id_language' => $id_language]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-menu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id, 'id_language' => $id_language], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id, 'id_language' => $id_language], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_language',
            'id_main',
            'title',
            'url:url',
            'icon',
            'position',
            'in_new',
            'seo_title',
            'seo_keywords',
            'seo_description',
            'seo_other',
        ],
    ]) ?>

</div>

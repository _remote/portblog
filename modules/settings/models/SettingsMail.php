<?php

namespace app\modules\settings\models;

use Yii;

/**
 * This is the model class for table "settings_mail".
 *
 * @property integer $id
 * @property string $host
 * @property string $email
 * @property string $username
 * @property string $password
 * @property integer $port
 * @property string $encryption
 * @property integer $file_transport
 * @property string $view_path
 * @property string $description
 */
class SettingsMail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings_mail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['host', 'email', 'username', 'password', 'port', 'encryption', 'view_path'], 'required'],
            [['port', 'file_transport'], 'integer'],
            ['email', 'email'],
            [['host', 'view_path', 'description'], 'string', 'max' => 100],
            [['username'], 'string', 'max' => 50],
            [['password'], 'string', 'max' => 32],
            [['encryption'], 'string', 'max' => 5],
            ['file_transport', 'default', 'value' => 0],
            ['template', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('settings', "ID"),
            'host' => Yii::t('settings', "Host"),
            'username' => Yii::t('settings', "Username"),
            'password' => Yii::t('settings', "Password"),
            'port' => Yii::t('settings', "Port"),
            'encryption' => Yii::t('settings', "Encryption"),
            'file_transport' => Yii::t('settings', "File Transport"),
            'view_path' => Yii::t('settings', "View Path"),
            'description' => Yii::t('settings', "Description"),
            'template' => Yii::t('settings', "Template"),
        ];
    }
}

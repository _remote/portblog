<?php

namespace app\modules\settings\models;

use Yii;

/**
 * This is the model class for table "settings_mail_templates".
 *
 * @property integer $id
 * @property string $type
 * @property string $text
 * @property string $date_created
 * @property string $date_updated
 * @property string $comment
 */
class SettingsMailTemplates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings_mail_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['date_created', 'date_updated'], 'safe'],
            [['type'], 'string', 'max' => 100],
            [['comment'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'text' => 'Text',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'comment' => 'Comment',
        ];
    }
}

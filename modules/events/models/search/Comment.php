<?php

namespace app\modules\events\models\search;

use Yii;
use yii\data\ActiveDataProvider;

use app\models\User;

class Comment extends \app\modules\events\models\Comment {
    
    public function rules() {
        return [
            [['id_material', 'id_comment', 'id_user', 'text', 'date_created', 'is_show'], 'safe'],
        ];
    }
    
}

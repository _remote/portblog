<?php

namespace app\modules\files\models;

use Yii;
use yii\bootstrap\Html;
use app\components\UploadImageBehavior;

/**
 * This is the model class for table "images".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $model
 * @property integer $id_object
 * @property string $title
 * @property string $file
 * @property string $url
 * @property string $description
 * @property integer $size
 * @property string $ext
 * @property string $type
 * @property integer $position
 * @property string $date_create
 * @property string $date_update
 * @property integer $is_active
 */
class Icons extends Images {
    
    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), [
            'file' => 'Icon',
        ]);
    }
    
    public function rules() {
        return array_merge(parent::rules(), [
            [['title'], 'required'],
        ]);
    }
    
    function behaviors() {
        return [
            [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'file',
                'scenarios' => ['insert', 'update'],
                'placeholder' => '@webroot/upload/default.jpg',
                'path' => '@webroot/upload/icons/{id}',
                'url' => '@web/upload/icons/{id}',
                'thumbPath' => '@webroot/upload/icons/{id}/thumb',
                'thumbUrl' => '@web/upload/icons/{id}/thumb',
                'thumbs' => [
                    'thumb' => ['width' => 100, 'height' => 100, 'quality' => 90],
                    'preview' => ['width' => 50, 'height' => 50],
                    'preview_small' => ['width' => 32, 'height' => 32],
                ],
            ],
        ];
    }
    
    public static function getList() {
        return Icons::find()->where(["type" => "icon"])->orderBy("title ASC")->all();
    }
    
    public static function getImage($id = false, $small = true) {
        if ($icon = Icons::find()->where(["type" => "icon", "id" => $id])->one()) 
            return Html::img($icon->getThumbUploadUrl('file', ($small)?'preview_small':'preview'), ['class' => ($small)?'icon-small':'icon']);
    }
}

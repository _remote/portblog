<?php

namespace app\modules\files\models;

use Yii;
use app\components\UploadImageBehavior;
use app\models\ActiveRecord;

/**
 * This is the model class for table "images".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $model
 * @property integer $id_object
 * @property string $title
 * @property string $file
 * @property string $url
 * @property string $description
 * @property integer $size
 * @property string $ext
 * @property string $type
 * @property integer $position
 * @property string $date_create
 * @property string $date_update
 * @property integer $is_active
 */
class Images extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'images';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_user', 'id_object', 'size', 'position', 'is_active', 'is_delete'], 'integer'],
            [['date_create'], 'required'],
            [['date_create', 'date_update'], 'safe'],
            [['model', 'title', 'url'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000],
            [['ext'], 'string', 'max' => 10],
            [['type'], 'string', 'max' => 5],
            ['file', 'file', 'extensions' => 'jpg, jpeg, gif, png', 'on' => ['insert', 'update']],
            [['is_active', 'is_delete'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'model' => 'Model',
            'id_object' => 'Id Object',
            'title' => 'Title',
            'file' => 'File',
            'url' => 'Url',
            'description' => 'Description',
            'size' => 'Size',
            'ext' => 'Ext',
            'type' => 'Type',
            'position' => 'Position',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
        ];
    }

    function behaviors() {
        return [
            [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'file',
                'scenarios' => ['insert', 'update'],
                'placeholder' => '@webroot/upload/default.jpg',
                'path' => '@webroot/upload/images/{model}/{id_object}',
                'url' => '@web/upload/images/{model}/{id_object}',
                'thumbPath' => '@webroot/upload/images/{model}/{id_object}/thumb',
                'thumbUrl' => '@web/upload/images/{model}/{id_object}/thumb',
                'thumbs' => [
                    'thumb' => ['width' => 940, 'height' => 640, 'quality' => 90],
                    'preview' => ['width' => 94, 'height' => 64],
                ],
            ],
        ];
    }

}

<?php

namespace app\modules\files\controllers\admin;

use Yii;
use app\modules\files\models\Icons;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\UploadedFile;

/**
 * IconController implements the CRUD actions for Icons model.
 */
class IconsController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Icons models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Icons::find()->where(["type" => "icon"]),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Icons model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Icons model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Icons();
        if ($model->load(Yii::$app->request->post())) {
            if (!Yii::$app->user->isGuest)
                $model->id_user = Yii::$app->user->id;
            $model->scenario = "insert";
            $model->date_create = date("Y-m-d G:i:s", time());
            $image = UploadedFile::getInstance($model, 'file');
            $model->size = $image->size;
            $model->file = $image;
            $type= preg_split('/\s*\/\s*/', $image->type, -1, PREG_SPLIT_NO_EMPTY);
            $model->type = "icon";
            $model->ext = isset($type[1])?$type[1]:NULL;
            $model->model = $model->getShortName();
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Icons model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->scenario = "update";
            $image = UploadedFile::getInstance($model, 'file');
            if ($image) {
                $model->size = $image->size;
                $model->file = $image;
                $type= preg_split('/\s*\/\s*/', $image->type, -1, PREG_SPLIT_NO_EMPTY);
                $model->ext = isset($type[1])?$type[1]:NULL;
            }
            if ($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
        } 
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Icons model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Icons model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Icons the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Icons::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

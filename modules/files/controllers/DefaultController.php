<?php

namespace app\modules\files\controllers;

use Yii;
use yii\web\Controller;
use app\modules\files\models\Images;
use yii\web\UploadedFile;

class DefaultController extends Controller {
    
    public function actionForm($_model = false, $id_object = false) {
        $model = new Images();
        $model->model = $_model;
        $model->id_object = $id_object;
        return $this->renderPartial("_form", [
            "model" => $model,
            "_model" => $_model,
            "id_object" => $id_object,
        ]);
    }
    
    public function actionUpload() {
        $model = new Images();
        if ($model->load(Yii::$app->request->post())) {
            if (!Yii::$app->user->isGuest)
                $model->id_user = Yii::$app->user->id;
            $model->scenario = "insert";
            $model->date_create = date("Y-m-d G:i:s", time());
            $image = UploadedFile::getInstance($model, 'file');
            $model->size = $image->size;
            $model->file = $image;
            $type= preg_split('/\s*\/\s*/', $image->type, -1, PREG_SPLIT_NO_EMPTY);
            $model->type = isset($type[0])?$type[0]:NULL;
            $model->ext = isset($type[1])?$type[1]:NULL;
            if ($model->save()) {
                $url = Yii::$app->request->hostInfo.$model->getThumbUploadUrl('file', 'thumb');
                //.closest('.mce-window').find('.mce-primary').click();
                return "<script>top.$('.mce-btn.mce-open').parent().find('.mce-textbox').val('{$url}');top.$('.mce-btn.upload').remove();top.$('.mce-btn.mce-open').show();</script>";
            }
        }
        else return "<script>alert('".json_encode ($model->getErrors())."')</script>";
    }

}

<?php

namespace app\modules\files\components;

use Yii;

class Prepare extends yii\base\Component {
    
    public function form($data = []) {
        list($controller) = Yii::$app->createController('files/default');
        return $controller->actionForm();
    }
    
}


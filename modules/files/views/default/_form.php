<?php
use yii\widgets\ActiveForm;
?>


<iframe id="upload_form_target" name="upload_form_target" style="display:none;"></iframe>
<?php 
$form = ActiveForm::begin([
    'id' => 'upload_form',
    'enableClientValidation' => false,
    'action' => \yii\helpers\Url::home().'files/default/upload',
    'options' => [
        'target' => 'upload_form_target',
        'enctype' => 'multipart/form-data',
        'style' => 'width:0;height:0;overflow:hidden;'
    ],
]);

echo $form->field($model, "model", ["options" => ["id" => "upload_form_model"]])->hiddenInput()->label(false); 

echo $form->field($model, "id_object",  ["options" => ["id" => "upload_form_object"]])->hiddenInput()->label(false); 

echo $form->field($model, "file", [
    "options" => [
        "onchange" => "
            $('#upload_form').submit();
            this.value='';
            button = $('.mce-btn.mce-open');
            $(button).parent().append('<div class=\'mce-btn upload\'><img src=\'../../images/upload.gif\'></div>');
            $(button).hide();
        ",
    ]
])->fileInput()->label(false);

ActiveForm::end();
?>


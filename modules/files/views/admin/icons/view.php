<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\files\models\Icons */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('files', 'Icons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="icons-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'file' => [
                //'label' => Yii::t('files', 'Icon'),
                'attribute' => 'file',
                'format' => 'raw',
                'value' => Html::img($model->getThumbUploadUrl('file', 'thumb'), ['class' => 'img-thumbnail']),
            ],
            //'id_user',
            //'model',
            //'id_object',
            //'title',
            //'file',
            //'url:url',
            'description',
            'size',
            'ext',
            'type',
            //'position',
            'date_create',
            'date_update',
            //'is_active',
            //'is_delete',
        ],
    ]) ?>

</div>

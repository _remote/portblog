<?php
use yii\helpers\Html;
use yii\grid\GridView;

use app\models\User;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('files', 'Icons');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="icons-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('files', 'Create Icons'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            'file' => [
                'attribute' => 'file',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img($model->getThumbUploadUrl('file', 'preview'), ['class' => 'img-thumbnail']);
                }
            ],
            'title',
            'id_user' => [
                'attribute' => 'id_user',
                'format' => 'raw',
                'value' => function($model) {
                    return User::getNameOfUser($model->id_user);
                },
                'filter' => ArrayHelper::map(
                    User::find()
                        ->orderBy('id')
                        ->asArray()->all(),
                    'id', 'first_name'),
                'options' => ['width' => '120'],
            ],
            //'model',
            //'id_object',
            //'title',
            // 'file',
            // 'url:url',
            'description',
            // 'size',
            // 'ext',
            // 'type',
            // 'position',
            'date_create',
            // 'date_update',
            // 'is_active',
            // 'is_delete',

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'V',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{view}'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'U',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{update}'
            ],
                [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'D',
                'contentOptions' => ['style' => 'width:30px;'],
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a(Html::tag("span", '', ['class' => 'glyphicon glyphicon-trash']), $url, [
                            'data' => [
                                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                                'pjax' => 0,
                                'class' => 'grid-action'
                            ],
                        ]);
                    },
                ],
                'template' => '{delete}'
            ],
        ],
    ]); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\files\models\Icons */

$this->title = Yii::t('files', 'Update Icons: ') . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('files', 'Icons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="icons-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\modules\files\models\Icons */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="icons-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>
    
    <?= $form->field($model, "title")->textInput(); ?>

     <?php 
    echo $form->field($model, "file")->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
            //'multiple'=>true
        ],
        'pluginOptions' => [
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  Yii::t('material', 'Select Photo'),
            'initialPreview'=> ($model->file)?Html::img($model->getThumbUploadUrl('file', 'thumb'), ['class' => 'img-thumbnail']):false, 
            'overwriteInitial'=>true,
        ],
        'pluginEvents' => [
            //"fileclear" => "function() { jQuery('#".$model->getShortName(true)."-{$id_language}-scenario').val('default');}",
            //"fileloaded" => "function() { jQuery('#".$model->getShortName(true)."-{$id_language}-scenario').val('update')}",
        ],
    ]);
    ?>
    
    <?= $form->field($model, "description")->textInput(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

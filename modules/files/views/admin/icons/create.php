<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\files\models\Icons */

$this->title = Yii::t('files', 'Create Icons');
$this->params['breadcrumbs'][] = ['label' => Yii::t('files', 'Icons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="icons-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

namespace app\modules\files;

class Files extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\files\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
